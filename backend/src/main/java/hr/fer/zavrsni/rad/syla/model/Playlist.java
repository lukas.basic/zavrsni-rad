package hr.fer.zavrsni.rad.syla.model;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
public class Playlist {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "playlist_ID", updatable = false, nullable = false)
    private UUID playlist_ID;

    @Column(name = "name")
    private String name;

    @Column(name = "modifiable")
    private Boolean modifiable;

    @Column(name = "public_access")
    private Boolean public_access;

    @CreatedDate
    @Column(name = "created", nullable = false, updatable = false)
    private LocalDate created;

    @LastModifiedDate
    @Column(name = "last_modified")
    private LocalDate last_modified;

    @ManyToOne(targetEntity = Account.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "account_ID")
    private Account account;

    @OneToMany(mappedBy = "playlist")
    List<BelongsTo> songs_orders;

    public Playlist() {
        this.songs_orders = new ArrayList<>();
    }

    public Playlist(UUID playlist_ID,
                    String name,
                    Boolean modifiable,
                    Boolean public_access,
                    LocalDate created,
                    LocalDate last_modified,
                    Account account) {
        this();

        this.playlist_ID = playlist_ID;
        this.name = name;
        this.modifiable = modifiable;
        this.public_access = public_access;
        this.created = created;
        this.last_modified = last_modified;
        this.account = account;
    }

    public Playlist(String name,
                    Boolean modifiable,
                    Boolean public_access,
                    LocalDate created,
                    LocalDate last_modified,
                    Account account) {
        this();

        this.name = name;
        this.modifiable = modifiable;
        this.public_access = public_access;
        this.created = created;
        this.last_modified = last_modified;
        this.account = account;
    }

    @PrePersist
    private void onCreate() {
        this.created = LocalDate.now();
    }

    @PreUpdate
    protected void preUpdate() {
        this.last_modified = LocalDate.now();
    }

    public UUID getPlaylist_ID() {
        return playlist_ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getModifiable() {
        return modifiable;
    }

    public void setModifiable(Boolean modifiable) {
        this.modifiable = modifiable;
    }

    public LocalDate getCreated() {
        return created;
    }

    public LocalDate getLast_modified() {
        return last_modified;
    }

    public void setLast_modified(LocalDate last_modified) {
        this.last_modified = last_modified;
    }

    public Account getAccount() {
        return account;
    }

    public List<BelongsTo> getSongs_orders() {
        return songs_orders;
    }

    public void addSongs_order(BelongsTo songs_order) {
        this.songs_orders.add(songs_order);
    }

    public Boolean getPublic_access() {
        return public_access;
    }

    public void setPublic_access(Boolean public_access) {
        this.public_access = public_access;
    }

    public void setSongs_orders(List<BelongsTo> songs_orders) {
        this.songs_orders = songs_orders;
    }
}
