package hr.fer.zavrsni.rad.syla.controller;

import hr.fer.zavrsni.rad.syla.dto.GenreDTO;
import hr.fer.zavrsni.rad.syla.model.Genre;
import hr.fer.zavrsni.rad.syla.service.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping(path = "api/v1/genre")
public class GenreController {

    private final GenreService genreService;

    @Autowired
    public GenreController(GenreService genreService) {
        this.genreService = genreService;
    }

    @GetMapping
    public List<GenreDTO> getGenres() {
        return genreService.getGenres();
    }

    @PostMapping
    public void registerNewGenre(@RequestBody String name) {
        genreService.addGenre(name);
    }

    @GetMapping(path = "/delete")
    public void deleteGenre(@RequestParam("genre_ID") UUID genre_ID) {
        genreService.deleteGenre(genre_ID);
    }

    @PostMapping(path = "/update")
    public void updateGenre(@RequestBody Genre genre) {
        genreService.updateGenre(genre);
    }
}
