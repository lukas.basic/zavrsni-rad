package hr.fer.zavrsni.rad.syla.service;

import hr.fer.zavrsni.rad.syla.dao.BelongsToRepository;
import hr.fer.zavrsni.rad.syla.model.BelongsTo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class BelongsToService {

    private final BelongsToRepository belongsToRepository;

    private final SongService songService;

    @Autowired
    public BelongsToService(BelongsToRepository belongsToRepository,
                            SongService songService) {
        this.belongsToRepository = belongsToRepository;
        this.songService = songService;
    }

    public void removeSong(BelongsTo bt) {
        songService.removeFromPlaylist(bt);

        belongsToRepository.delete(bt);
    }

    public void makeOrder(UUID playlist_id) {
        List<BelongsTo> belongsToList = belongsToRepository.findAll().stream()
                                                                        .filter(bt -> bt.getPlaylist().getPlaylist_ID().equals(playlist_id))
                                                                        .sorted(Comparator.comparing(BelongsTo::getPlaylist_order))
                                                                        .collect(Collectors.toList());

        for (int i = 1; i <= belongsToList.size(); i++) {
            BelongsTo bt = belongsToList.get(i);
            if (bt.getPlaylist_order() != i) {
                bt.setPlaylist_order(i);
                belongsToRepository.save(bt);
            }
        }
    }
}
