package hr.fer.zavrsni.rad.syla.dto;

import hr.fer.zavrsni.rad.syla.model.Account;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class AlbumDTO {

    private UUID album_ID;
    private String name;
    private String picture;
    private byte[] picture_data;
    private LocalDate released;
    private List<UUID> account_ids;
    private List<AccountDTO> accounts;
    private Integer songs_num;
    private Integer likes;

    public AlbumDTO() {}

    public AlbumDTO(UUID album_ID,
                    String name,
                    List<AccountDTO> accounts) {
        this.album_ID = album_ID;
        this.name = name;
        this.accounts = accounts;
    }

    public AlbumDTO(UUID album_ID,
                    String name,
                    String picture,
                    Integer songs_num) {
        this.album_ID = album_ID;
        this.name = name;
        this.picture = picture;
        this.songs_num = songs_num;

        this.accounts = new ArrayList<>();
    }

    public AlbumDTO(UUID album_ID,
                    String name,
                    String picture) {
        this.album_ID = album_ID;
        this.name = name;
        this.picture = picture;

        this.accounts = new ArrayList<>();
    }

    public AlbumDTO(String name,
                    String picture,
                    List<UUID> account_ids) {
        this.name = name;
        this.picture = picture;
        this.account_ids = account_ids;

        this.accounts = new ArrayList<>();
    }

    public AlbumDTO(String name,
                    List<UUID> account_ids) {
        this.name = name;
        this.account_ids = account_ids;
    }

    public AlbumDTO(UUID album_ID,
                    String name,
                    String picture,
                    LocalDate released,
                    List<AccountDTO> accounts) {
        this.album_ID = album_ID;
        this.name = name;
        this.picture = picture;
        this.released = released;
        this.accounts = accounts;
    }

    public AlbumDTO(UUID album_ID,
                    String name,
                    String picture,
                    LocalDate released,
                    Integer likes,
                    List<AccountDTO> accounts) {
        this.album_ID = album_ID;
        this.name = name;
        this.picture = picture;
        this.released = released;
        this.likes = likes;
        this.accounts = accounts;
    }

    public AlbumDTO(UUID album_ID,
                   String name,
                   byte[] picture_data,
                   LocalDate released,
                   List<AccountDTO> accounts) {
        this.album_ID = album_ID;
        this.name = name;
        this.picture_data = picture_data;
        this.released = released;
        this.accounts = accounts;
    }

    public UUID getAlbum_ID() {
        return album_ID;
    }

    public void setAlbum_ID(UUID album_ID) {
        this.album_ID = album_ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public LocalDate getReleased() {
        return released;
    }

    public void setReleased(LocalDate released) {
        this.released = released;
    }

    public List<UUID> getAccount_ids() {
        return account_ids;
    }

    public void setAccount_ids(List<UUID> account_ids) {
        this.account_ids = account_ids;
    }

    public List<AccountDTO> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<AccountDTO> accounts) {
        this.accounts = accounts;
    }

    public void insertAccount(AccountDTO account) {
        this.accounts.add(account);
    }

    public Integer getSongs_num() {
        return songs_num;
    }

    public void setSongs_num(Integer songs_num) {
        this.songs_num = songs_num;
    }

    public byte[] getPicture_data() {
        return picture_data;
    }

    public void setPicture_data(byte[] picture_data) {
        this.picture_data = picture_data;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }
}
