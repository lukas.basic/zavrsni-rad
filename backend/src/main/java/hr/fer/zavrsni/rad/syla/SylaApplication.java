package hr.fer.zavrsni.rad.syla;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SylaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SylaApplication.class, args);
	}

}
