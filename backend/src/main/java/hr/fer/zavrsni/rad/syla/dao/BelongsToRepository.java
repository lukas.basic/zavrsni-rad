package hr.fer.zavrsni.rad.syla.dao;

import hr.fer.zavrsni.rad.syla.model.BelongsTo;
import hr.fer.zavrsni.rad.syla.model.BelongsToKey;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BelongsToRepository extends JpaRepository<BelongsTo, BelongsToKey> {
}
