package hr.fer.zavrsni.rad.syla.service;

import hr.fer.zavrsni.rad.syla.dao.*;
import hr.fer.zavrsni.rad.syla.dto.*;
import hr.fer.zavrsni.rad.syla.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class SongService {

    private final FileStorageService fileStorageService;

    private final SongRepository songRepository;

    private final AlbumRepository albumRepository;

    private final AccountRepository accountRepository;

    private final GenreRepository genreRepository;

    private final BelongsToRepository belongsToRepository;

    private final CommentRepository commentRepository;

    @Autowired
    public SongService(SongRepository songRepository,
                       AlbumRepository albumRepository,
                       GenreRepository genreRepository,
                       AccountRepository accountRepository,
                       BelongsToRepository belongsToRepository,
                       CommentRepository commentRepository,
                       FileStorageService fileStorageService) {
        this.songRepository = songRepository;
        this.albumRepository = albumRepository;
        this.genreRepository = genreRepository;
        this.accountRepository = accountRepository;
        this.belongsToRepository = belongsToRepository;
        this.commentRepository = commentRepository;
        this.fileStorageService = fileStorageService;
    }

    public List<SongDTO> getSongs() {
        return songRepository.findAll().stream()
                                        .map(s -> new SongDTO(s.getSong_ID(),
                                                                s.getName(),
                                                                s.getAudio(),
                                                                Arrays.asList(s.getCollaborations().split(", ")),
                                                                s.getPopularity(),
                                                                s.getReleased(),
                                                                new AlbumDTO(s.getAlbum().getAlbum_ID(),
                                                                            s.getAlbum().getName(),
                                                                            s.getAlbum().getPicture(),
                                                                            s.getAlbum().getSongs().size()),
                                                                s.getAlbum_order(),
                                                                s.getAccounts().stream()
                                                                                .map(a -> new AccountDTO(a.getAccount_ID(),
                                                                                                        a.getPicture(),
                                                                                                        a.getUsername(),
                                                                                                        a.getStage_name(),
                                                                                                        a.getDisplayName(),
                                                                                                        a.getAlbums().stream()
                                                                                                                .map(alb -> new AlbumDTO(alb.getAlbum_ID(),
                                                                                                                                            alb.getName(),
                                                                                                                                            alb.getPicture()))
                                                                                                                .collect(Collectors.toList())))
                                                                                .collect(Collectors.toList()),
                                                                s.getGenres().stream()
                                                                            .map(g -> new GenreDTO(g.getGenre_ID(), g.getName()))
                                                                            .collect(Collectors.toList()),
                                                                s.getComments().stream()
                                                                                .map(c -> new CommentDTO(c.getComment_ID(),
                                                                                                        c.getContent(),
                                                                                                        c.getCreated(),
                                                                                                        c.getLast_modified(),
                                                                                                        new AccountDTO(c.getAccount().getAccount_ID(),
                                                                                                                        c.getAccount().getPicture(),
                                                                                                                        c.getAccount().getDisplayName())))
                                                                                .collect(Collectors.toList())))
                                        .collect(Collectors.toList());

    }

    public void updateSong(SongDTO song) {
        Optional<Song> songOptional = songRepository.findById(song.getSong_ID());

        if (!songOptional.isPresent())
            throw new IllegalStateException("Song with ID: " + song.getSong_ID() + " does not exist!");

        if (song.getGenres().size() == 0)
            throw new IllegalStateException("Song must have at least one genre.");

        Song song1 = songOptional.get();

        song1.setName(song.getName());
        song1.setAudio(song.getAudio());

        if (song.getCollaborations().size() > 0) {
            List<String> collaborationsList = song.getCollaborations();
            StringBuilder collaborations = new StringBuilder(collaborationsList.get(0));

            for (int i = 1; i < collaborationsList.size(); i++) {
                collaborations.append(", " + collaborationsList.get(i));
            }

            song1.setCollaborations(collaborations.toString());
        } else {
            song1.setCollaborations("");
        }

        List<Genre> genres = new ArrayList<>();

        for (GenreDTO genre : song.getGenres()) {
            genres.add(genreRepository.findById(genre.getGenre_ID()).get());
        }

        song1.setGenres(genres);

        List<Account> accounts = new ArrayList<>();

        for (AccountDTO a : song.getAccounts()) {
            Optional<Account> accountOptional = accountRepository.findById(a.getAccount_ID());

            if (!accountOptional.isPresent())
                throw new IllegalStateException("Account with ID '" + a.getAccount_ID() + "' doesn't exist!");

            accounts.add(accountOptional.get());
        }

        /*Album album = null;

        if (song.getAlbum() != null) {
            Optional<Album> albumOptional = albumRepository.findById(song.getAlbum().getAlbum_ID());

            if (!albumOptional.isPresent())
                throw new IllegalStateException("Album with ID: " + song.getAlbum().getAlbum_ID() + " does not exist!");

            album = albumOptional.get();
        }

        Album oldAlbum = song1.getAlbum();

        if (!oldAlbum.getAlbum_ID().equals(album.getAlbum_ID())) {
            song1.setAlbum(album);
            song1.setAlbum_order(album.getSongs().size() + 1);
        }*/

        song1.setAccounts(accounts);

        songRepository.save(song1);

        //fixAlbumOrder(oldAlbum);
    }

    private void fixAlbumOrder(Album album) {
        List<Song> songs = album.getSongs().stream()
                .sorted(Comparator.comparing(Song::getAlbum_order))
                .collect(Collectors.toList());

        for (int i = 1; i <= songs.size(); i++) {
            Song song = songs.get(i-1);

            if (song.getAlbum_order() != i)
                changeAlbumOrder(song, i);
        }
    }

    private void changeAlbumOrder(Song song, Integer album_order) {
        Album album = song.getAlbum();

        List<Song> songs = album.getSongs().stream()
                                            .sorted((s1, s2) -> s1.getAlbum_order().compareTo(s2.getAlbum_order()))
                                            .collect(Collectors.toList());

        Boolean down = song.getAlbum_order() > album_order;

        int i = 1;
        for (Song s : songs) {
            if (s.equals(song)) {
                continue;
            }

            if (i == album_order && down) {
                i++;
            }

            s.setAlbum_order(i++);
        }

        song.setAlbum_order(album_order);

        for (Song s : album.getSongs())
            songRepository.save(s);

        albumRepository.save(album);
    }

    public void uploadNewSong(SongDTO song, MultipartFile audio) {
        Optional<Album> albumOptional = albumRepository.findById(song.getAlbum_ID());

        if (!albumOptional.isPresent())
            throw new IllegalArgumentException("Album with ID: " + song.getAlbum_ID() + " does not exist!");

        List<Account> accounts = new ArrayList<>();

        for (UUID a_id : song.getAccount_ids()) {
            Optional<Account> accountOptional = accountRepository.findById(a_id);

            if (!accountOptional.isPresent())
                throw new IllegalArgumentException("Account with ID: " + a_id + " does not exist!");

            accounts.add(accountOptional.get());
        }

        List<Genre> genres = new ArrayList<>();

        for (UUID g : song.getGenre_ids()) {
            Optional<Genre> genreOptional = genreRepository.findById(g);

            if (!genreOptional.isPresent())
                throw new IllegalArgumentException("Genre with ID: " + g + " does not exist!");

            genres.add(genreOptional.get());
        }

        String collaborationsString = "";

        if (!song.getCollaborations().isEmpty()) {
            StringBuilder collaborations = new StringBuilder(song.getCollaborations().get(0));

            if (song.getCollaborations().size() > 1) {
                for (int i = 1; i < song.getCollaborations().size(); i++) {
                    collaborations.append(", " + song.getCollaborations().get(i));
                }
            }

            collaborationsString = collaborations.toString();
        }


        Album album = albumOptional.get();

        Song newSong = new Song(song.getName(),
                                "",
                                collaborationsString,
                                LocalDate.now(),
                                song.getAlbum_order(),
                                album,
                                accounts,
                                genres);

        songRepository.save(newSong);

        album.getSongs().add(newSong);

        albumRepository.save(album);

        fixAlbumOrder(album);

        uploadAudio(audio, newSong.getSong_ID());
    }

    public List<AccountDTO> getNotAuthors(UUID song_id) {
        Optional<Song> songOptional = songRepository.findById(song_id);

        if (!songOptional.isPresent())
            throw new IllegalStateException("Song with ID '" + song_id + "' does not exist!");

        List<Account> accounts = songOptional.get().getAccounts();

        return accountRepository.findAll().stream()
                                            .filter(a -> !accounts.contains(a))
                                            .map(a -> new AccountDTO(a.getAccount_ID(),
                                                                    a.getUsername(),
                                                                    a.getStage_name()))
                                            .collect(Collectors.toList());
    }

    public void uploadAudio(MultipartFile audio, UUID song_ID) {
        Optional<Song> s = songRepository.findById(song_ID);

        if (!s.isPresent())
            throw new IllegalStateException("Song with ID '" + song_ID + "' does not exist!");

        Song song = s.get();

        if (fileStorageService.storeFile(audio, "audio/" + song_ID.toString() + ".mp3")) {
            song.setAudio("audio/" + song_ID.toString() + ".mp3");
        }

        songRepository.save(song);
    }

    public Map<String, byte[]> downloadAudio(String name) {
        Map<String, byte[]> resp = new HashMap<>();
        resp.put("data", fileStorageService.downloadFile(name));
        return resp;
    }

    public void deleteSong(UUID song_ID) {
        Optional<Song> songOptional = songRepository.findById(song_ID);

        if (!songOptional.isPresent())
            throw new IllegalStateException("Song with ID '" + song_ID + "' does not exist!");

        Song song = songOptional.get();

        song.getAlbum().getSongs().remove(song);

        while (!song.getGenres().isEmpty()) {
            Genre genre = song.getGenres().get(0);

            genre.getSongs().remove(song);
            song.getGenres().remove(genre);
            genreRepository.save(genre);
        }

        while (!song.getAccounts().isEmpty()) {
            Account account = song.getAccounts().get(0);

            account.getSongs().remove(song);
            song.getAccounts().remove(account);
            accountRepository.save(account);
        }

        while (!song.getPlaylists_orders().isEmpty()) {
            BelongsTo bt = song.getPlaylists_orders().remove(0);

            belongsToRepository.delete(bt);
        }

        while (!song.getComments().isEmpty()) {
            Comment c = song.getComments().remove(0);

            commentRepository.delete(c);
        }

        albumRepository.save(song.getAlbum());
        songRepository.delete(song);
    }

    public void removeAccount(Song song, Account account) {
        song.getAccounts().remove(account);
        songRepository.save(song);
    }

    public void removeGenre(Song song, Genre genre) {
        song.getGenres().remove(genre);
        songRepository.save(song);
    }

    public Boolean checkGenre(Song song) {
        return song.getGenres().size() == 1;
    }

    public void removeLike(Song song) {
        if (song.getPopularity() == 0)
            return;

        song.setPopularity(song.getPopularity() - 1);
        songRepository.save(song);
    }

    public void removeFromPlaylist(BelongsTo bt) {
        Song song = bt.getSong();
        song.getPlaylists_orders().remove(bt);

        songRepository.save(song);
    }

    public SongDTO getSong(UUID song_ID) {
        Optional<Song> songOptional = songRepository.findById(song_ID);

        if (!songOptional.isPresent())
            throw new IllegalStateException("Song with ID '" + song_ID + "' does not exist!");

        Song s = songOptional.get();

        return new SongDTO(s.getSong_ID(),
                            s.getName(),
                            s.getAudio(),
                            s.getCollaborations(),
                            s.getPopularity(),
                            s.getReleased(),
                            new AlbumDTO(s.getAlbum().getAlbum_ID(),
                                    s.getAlbum().getName(),
                                    s.getAlbum().getPicture(),
                                    s.getAlbum().getSongs().size()),
                            s.getAlbum_order(),
                            s.getAccounts().stream()
                                    .map(a -> new AccountDTO(a.getAccount_ID(),
                                            a.getPicture(),
                                            a.getUsername(),
                                            a.getStage_name(),
                                            a.getDisplayName(),
                                            a.getAlbums().stream()
                                                    .map(alb -> new AlbumDTO(alb.getAlbum_ID(),
                                                            alb.getName(),
                                                            alb.getPicture()))
                                                    .collect(Collectors.toList())))
                                    .collect(Collectors.toList()),
                            s.getGenres().stream()
                                    .map(g -> new GenreDTO(g.getGenre_ID(), g.getName()))
                                    .collect(Collectors.toList()),
                            s.getComments().stream()
                                    .map(c -> new CommentDTO(c.getComment_ID(),
                                            c.getContent(),
                                            c.getCreated(),
                                            c.getLast_modified(),
                                            new AccountDTO(c.getAccount().getAccount_ID(),
                                                    c.getAccount().getUsername(),
                                                    c.getAccount().getPicture(),
                                                    c.getAccount().getDisplayName())))
                                    .collect(Collectors.toList()));
    }
    
    public List<SongDTO> search(String search) {
    	return songRepository.findAll().stream()
    					.filter(s -> s.getName().toLowerCase().contains(search.toLowerCase()) || s.getCollaborations().toLowerCase().contains(search.toLowerCase()))
                                        .map(s -> new SongDTO(s.getSong_ID(),
								    s.getName(),
								    s.getAudio(),
								    s.getCollaborations(),
								    s.getPopularity(),
								    s.getReleased(),
								    new AlbumDTO(s.getAlbum().getAlbum_ID(),
									    s.getAlbum().getName(),
									    s.getAlbum().getPicture(),
									    s.getAlbum().getSongs().size()),
								    s.getAlbum_order(),
								    s.getAccounts().stream()
									    .map(a -> new AccountDTO(a.getAccount_ID(),
										    a.getPicture(),
										    a.getUsername(),
										    a.getStage_name(),
										    a.getDisplayName(),
										    a.getAlbums().stream()
										            .map(alb -> new AlbumDTO(alb.getAlbum_ID(),
										                    alb.getName(),
										                    alb.getPicture()))
										            .collect(Collectors.toList())))
									    .collect(Collectors.toList()),
								    s.getGenres().stream()
									    .map(g -> new GenreDTO(g.getGenre_ID(), g.getName()))
									    .collect(Collectors.toList()),
								    s.getComments().stream()
									    .map(c -> new CommentDTO(c.getComment_ID(),
										    c.getContent(),
										    c.getCreated(),
										    c.getLast_modified(),
										    new AccountDTO(c.getAccount().getAccount_ID(),
										            c.getAccount().getUsername(),
										            c.getAccount().getPicture(),
										            c.getAccount().getDisplayName())))
									    .collect(Collectors.toList())))
                                        .collect(Collectors.toList());
    }

    public Optional<Song> findSongById(UUID song_id) {
        return songRepository.findById(song_id);
    }

    public void save(Song song) {
        songRepository.save(song);
    }
}
