package hr.fer.zavrsni.rad.syla.model;

import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
public class Album {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "album_ID", updatable = false, nullable = false)
    private UUID album_ID;

    @Column(name = "name")
    private String name;

    @Column(name = "picture")
    private String picture;

    @CreatedDate
    @Column(name = "released", nullable = false, updatable = false)
    private LocalDate released;

    @ManyToMany(mappedBy = "albums")
    private List<Account> accounts;

    @OneToMany(mappedBy = "album")
    private List<Song> songs;

    @ManyToMany(mappedBy = "likesAlbums")
    private List<Account> likes;

    public Album() {
        this.picture = "images/default_album_picture.jpg";
        this.accounts = new ArrayList<>();
        this.songs = new ArrayList<>();
        this.likes = new ArrayList<>();
    }

    public Album(UUID album_ID,
                 String name,
                 String picture,
                 LocalDate released,
                 List<Account> accounts) {
        this();

        this.album_ID = album_ID;
        this.name = name;
        this.picture = picture;
        this.released = released;
        this.accounts = accounts;
    }

    public Album(String name,
                 String picture,
                 LocalDate released,
                 List<Account> accounts) {
        this();

        this.name = name;
        this.picture = picture;
        this.released = released;
        this.accounts = accounts;
    }

    public Album(String name,
                 LocalDate released,
                 List<Account> accounts) {
        this();

        this.name = name;
        this.released = released;
        this.accounts = accounts;
    }

    @PrePersist
    private void onCreate() {
        this.released = LocalDate.now();
    }

    public UUID getAlbum_ID() {
        return album_ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public LocalDate getReleased() {
        return released;
    }

    public void setReleased(LocalDate released) {
        this.released = released;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void addAccount(Account account) {
        this.accounts.add(account);
    }

    public List<Song> getSongs() {
        return songs;
    }

    public void addSong(Song song) {
        this.songs.add(song);
    }

    public List<Account> getLikes() {
        return likes;
    }

    public void addLike(Account like) {
        this.likes.add(like);
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    public void setSongs(List<Song> songs) {
        this.songs = songs;
    }

    public void setLikes(List<Account> likes) {
        this.likes = likes;
    }
}
