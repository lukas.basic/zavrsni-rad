package hr.fer.zavrsni.rad.syla.service;

import hr.fer.zavrsni.rad.syla.dao.GenreRepository;
import hr.fer.zavrsni.rad.syla.dao.SongRepository;
import hr.fer.zavrsni.rad.syla.dto.GenreDTO;
import hr.fer.zavrsni.rad.syla.model.Genre;
import hr.fer.zavrsni.rad.syla.model.Song;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class GenreService {

    private final GenreRepository genreRepository;

    private final SongService songService;

    @Autowired
    public GenreService(GenreRepository genreRepository,
                        SongService songService) {
        this.genreRepository = genreRepository;
        this.songService = songService;
    }

    public List<GenreDTO> getGenres() {
        return genreRepository.findAll().stream().map(g -> new GenreDTO(g.getGenre_ID(), g.getName())).collect(Collectors.toList());
    }

    public void addGenre(String name) {
        Optional<Genre> g = genreRepository.findByName(name);

        if (g.isPresent())
            throw new IllegalStateException("Genre with name '" + name + "' already exists!");

        Genre genre = new Genre(name);

        genreRepository.save(genre);
    }

    public void deleteGenre(UUID genre_id) {
        Optional<Genre> g = genreRepository.findById(genre_id);

        if (!g.isPresent())
            throw new IllegalStateException("Genre with ID '" + genre_id + "' does not exist!");

        Genre genre = g.get();

        List<Song> unremovableSongs = new ArrayList<>();
        for (Song song : genre.getSongs())
           if (songService.checkGenre(song)) {
               unremovableSongs.add(song);
           }

        if (unremovableSongs.size() > 0)
            throw new IllegalStateException("There are " + unremovableSongs.size() + " songs that depend only on this genre, so this genre cannot be removed!");

        for (Song song : genre.getSongs())
            songService.removeGenre(song, genre);

        genreRepository.deleteById(genre_id);
    }

    public void updateGenre(Genre genre) {
        Optional<Genre> g1 = genreRepository.findById(genre.getGenre_ID());

        if (!g1.isPresent())
            throw new IllegalStateException("Genre with ID '" + genre.getGenre_ID() + "' does not exist!");

        Optional<Genre> g = genreRepository.findByName(genre.getName());

        if (g.isPresent() && !genre.getGenre_ID().equals(g.get().getGenre_ID()))
            throw new IllegalStateException("Genre with name '" + genre.getName() + "' already exists!");

        genreRepository.save(genre);
    }
}
