package hr.fer.zavrsni.rad.syla.dao;

import hr.fer.zavrsni.rad.syla.model.Album;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface AlbumRepository extends JpaRepository<Album, UUID> {
}
