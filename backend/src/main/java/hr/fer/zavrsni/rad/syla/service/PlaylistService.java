package hr.fer.zavrsni.rad.syla.service;

import hr.fer.zavrsni.rad.syla.dao.AccountRepository;
import hr.fer.zavrsni.rad.syla.dao.BelongsToRepository;
import hr.fer.zavrsni.rad.syla.dao.PlaylistRepository;
import hr.fer.zavrsni.rad.syla.dto.*;
import hr.fer.zavrsni.rad.syla.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class PlaylistService {

    private final PlaylistRepository playlistRepository;

    private final AccountRepository accountRepository;

    private final SongService songService;

    private final BelongsToService belongsToService;

    private final BelongsToRepository belongsToRepository;

    @Autowired
    public PlaylistService(PlaylistRepository playlistRepository,
                           AccountRepository accountRepository,
                           SongService songService,
                           BelongsToService belongsToService,
                           BelongsToRepository belongsToRepository) {
        this.playlistRepository = playlistRepository;
        this.accountRepository = accountRepository;
        this.songService = songService;
        this.belongsToService = belongsToService;
        this.belongsToRepository = belongsToRepository;
    }

    public List<PlaylistDTO> getPlaylists() {
        return playlistRepository.findAll().stream()
                                            .map(pl -> new PlaylistDTO(pl.getPlaylist_ID(),
                                                                        pl.getName(),
                                                                        pl.getModifiable(),
                                                                        pl.getPublic_access(),
                                                                        pl.getCreated(),
                                                                        pl.getLast_modified(),
                                                                        new AccountDTO(pl.getAccount().getAccount_ID(),
                                                                                        pl.getAccount().getPicture(),
                                                                                        pl.getAccount().getUsername(),
                                                                                        pl.getAccount().getStage_name(),
                                                                                        pl.getAccount().getDisplayName())
                                                                        ))
                                            .collect(Collectors.toList());
    }

    public void addNewPlaylist(PlaylistDTO playlist) {
        Optional<Account> acc = accountRepository.findById(playlist.getAccount_ID());

        if (!acc.isPresent())
            throw new IllegalArgumentException("Account with ID: " + playlist.getAccount_ID() + "does not exist!");

        Account account = acc.get();

        /*Boolean exists = account.getPlaylists().stream()
                                                .map(p -> p.getName())
                                                .anyMatch(n -> n.equals(playlist.getName()));

        if (exists)
            throw new IllegalArgumentException("You already have playlist with name " + playlist.getName() + "!");*/
            
        Playlist pl = new Playlist(playlist.getName(), true, playlist.getPublic_access(), LocalDate.now(), null, account);

        playlistRepository.save(pl);
        
        account.addPlaylist(pl);
        
        accountRepository.save(account);
    }
    
    public void createFavourites(UUID accountID) {
    	Optional<Account> acc = accountRepository.findById(accountID);

        if (!acc.isPresent())
            throw new IllegalArgumentException("Account with ID: " + accountID + "does not exist!");

        Account account = acc.get();
        
        Playlist pl = new Playlist("Favourites", false, true, LocalDate.now(), null, account);
        
        playlistRepository.save(pl);
        
        account.addPlaylist(pl);
        
        accountRepository.save(account);
    }

    public void deletePlaylist(UUID playlist_id) {
        Optional<Playlist> optionalPlaylist = playlistRepository.findById(playlist_id);

        if (!optionalPlaylist.isPresent()) {
            throw new IllegalStateException("Playlist with id " + playlist_id + " does not exist!");
        }

        Playlist playlist = optionalPlaylist.get();

        List<BelongsTo> btList = new ArrayList<>(playlist.getSongs_orders());

        for (BelongsTo bt : btList) {
            if (!playlist.getModifiable())
                songService.removeLike(bt.getSong());

            this.removeSong(bt);
            belongsToService.removeSong(bt);
        }

        playlistRepository.deleteById(playlist_id);
    }

    public void updatePlaylist(PlaylistDTO playlist) {
        Optional<Playlist> pl = playlistRepository.findById(playlist.getPlaylist_ID());

        if (!pl.isPresent())
            throw new IllegalArgumentException("Playlist with ID: " + playlist.getPlaylist_ID() + " does not exist!");

        Playlist playlist1 = pl.get();

        playlist1.setName(playlist.getName());
        playlist1.setPublic_access(playlist.getPublic_access());
        playlist1.setLast_modified(LocalDate.now());

        playlistRepository.save(playlist1);
    }

    public List<SongDTO> getPlaylistSongs(UUID id) {
        Optional<Playlist> playlistOptional = playlistRepository.findById(id);

        if (!playlistOptional.isPresent())
            throw new IllegalStateException("Playlist with ID '" + id + "' does not exist!");

        Playlist playlist = playlistOptional.get();

        return playlist.getSongs_orders().stream()
                .sorted(Comparator.comparing(BelongsTo::getPlaylist_order))
                .map(s -> new SongDTO(s.getSong().getSong_ID(),
                        s.getSong().getName(),
                        s.getSong().getAudio(),
                        s.getSong().getCollaborations(),
                        s.getPlaylist_order(),
                        s.getSong().getAccounts().stream()
                                .map(a -> new AccountDTO(a.getAccount_ID(),
                                        a.getPicture(),
                                        a.getDisplayName()))
                                .collect(Collectors.toList())))
                .collect(Collectors.toList());
    }

    public void removeSong(BelongsTo bt) {
        Playlist playlist = bt.getPlaylist();
        playlist.getSongs_orders().remove(bt);

        playlistRepository.save(playlist);
    }

    public PlaylistDTO getPlaylist(UUID playlist_id) {
        Optional<Playlist> optionalPlaylist = playlistRepository.findById(playlist_id);

        if (!optionalPlaylist.isPresent()) {
            throw new IllegalStateException("Playlist with id " + playlist_id + " does not exist!");
        }

        Playlist pl = optionalPlaylist.get();

        return new PlaylistDTO(
                        pl.getPlaylist_ID(),
                        pl.getName(),
                        pl.getModifiable(),
                        pl.getPublic_access(),
                        pl.getCreated(),
                        pl.getLast_modified(),
                        new AccountDTO(pl.getAccount().getAccount_ID(),
                                pl.getAccount().getPicture(),
                                pl.getAccount().getUsername(),
                                pl.getAccount().getStage_name(),
                                pl.getAccount().getDisplayName()),
                        pl.getSongs_orders().stream()
                                            .sorted(Comparator.comparing(BelongsTo::getPlaylist_order))
                                            .map(bt -> {
                                                Song s = bt.getSong();
                                                return new SongDTO(s.getSong_ID(),
                                                        s.getName(),
                                                        s.getAudio(),
                                                        s.getCollaborations(),
                                                        s.getPopularity(),
                                                        s.getReleased(),
                                                        new AlbumDTO(s.getAlbum().getAlbum_ID(),
                                                                s.getAlbum().getName(),
                                                                s.getAlbum().getPicture(),
                                                                s.getAlbum().getSongs().size()),
                                                        s.getAlbum_order(),
                                                        s.getAccounts().stream()
                                                                .map(a -> new AccountDTO(a.getAccount_ID(),
                                                                        a.getPicture(),
                                                                        a.getUsername(),
                                                                        a.getStage_name(),
                                                                        a.getDisplayName(),
                                                                        a.getAlbums().stream()
                                                                                .map(alb -> new AlbumDTO(alb.getAlbum_ID(),
                                                                                        alb.getName(),
                                                                                        alb.getPicture()))
                                                                                .collect(Collectors.toList())))
                                                                .collect(Collectors.toList()),
                                                        s.getGenres().stream()
                                                                .map(g -> new GenreDTO(g.getGenre_ID(), g.getName()))
                                                                .collect(Collectors.toList()),
                                                        s.getComments().stream()
                                                                .map(c -> new CommentDTO(c.getComment_ID(),
                                                                        c.getContent(),
                                                                        c.getCreated(),
                                                                        c.getLast_modified(),
                                                                        new AccountDTO(c.getAccount().getAccount_ID(),
                                                                                c.getAccount().getUsername(),
                                                                                c.getAccount().getPicture(),
                                                                                c.getAccount().getDisplayName())))
                                                                .collect(Collectors.toList()));
                                            })
                                            .collect(Collectors.toList())
                    );
    }

    public PlaylistDTO getFavourites(UUID account_id) {
        Optional<Account> optionalAccount = accountRepository.findById(account_id);

        if (!optionalAccount.isPresent()) {
            throw new IllegalStateException("Account with ID '" + account_id + "' does not exist!");
        }

        Account account = optionalAccount.get();

        Playlist pl = account.getPlaylists().stream().filter(p -> !p.getModifiable()).findFirst().get();

        if (pl == null)
            throw new IllegalStateException("Error while getting Favourites playlist for account with ID '" + account_id + "'!");

        return new PlaylistDTO(
                pl.getPlaylist_ID(),
                pl.getName(),
                pl.getModifiable(),
                pl.getPublic_access(),
                pl.getCreated(),
                pl.getLast_modified(),
                new AccountDTO(pl.getAccount().getAccount_ID(),
                        pl.getAccount().getPicture(),
                        pl.getAccount().getUsername(),
                        pl.getAccount().getStage_name(),
                        pl.getAccount().getDisplayName()),
                pl.getSongs_orders().stream()
                        .sorted(Comparator.comparing(BelongsTo::getPlaylist_order))
                        .map(bt -> {
                            Song s = bt.getSong();
                            return new SongDTO(s.getSong_ID(),
                                    s.getName(),
                                    s.getAudio(),
                                    s.getCollaborations(),
                                    s.getPopularity(),
                                    s.getReleased(),
                                    new AlbumDTO(s.getAlbum().getAlbum_ID(),
                                            s.getAlbum().getName(),
                                            s.getAlbum().getPicture(),
                                            s.getAlbum().getSongs().size()),
                                    s.getAlbum_order(),
                                    s.getAccounts().stream()
                                            .map(a -> new AccountDTO(a.getAccount_ID(),
                                                    a.getPicture(),
                                                    a.getUsername(),
                                                    a.getStage_name(),
                                                    a.getDisplayName(),
                                                    a.getAlbums().stream()
                                                            .map(alb -> new AlbumDTO(alb.getAlbum_ID(),
                                                                    alb.getName(),
                                                                    alb.getPicture()))
                                                            .collect(Collectors.toList())))
                                            .collect(Collectors.toList()),
                                    s.getGenres().stream()
                                            .map(g -> new GenreDTO(g.getGenre_ID(), g.getName()))
                                            .collect(Collectors.toList()),
                                    s.getComments().stream()
                                            .map(c -> new CommentDTO(c.getComment_ID(),
                                                    c.getContent(),
                                                    c.getCreated(),
                                                    c.getLast_modified(),
                                                    new AccountDTO(c.getAccount().getAccount_ID(),
                                                            c.getAccount().getUsername(),
                                                            c.getAccount().getPicture(),
                                                            c.getAccount().getDisplayName())))
                                            .collect(Collectors.toList()));
                        })
                        .collect(Collectors.toList())
        );
    }

    public void save(Playlist playlist) {
        playlistRepository.save(playlist);
    }

    public void add(UUID song_id, UUID playlist_id) {
        Optional<Song> optionalSong = songService.findSongById(song_id);

        if (!optionalSong.isPresent())
            throw new IllegalStateException("Song with ID '" + song_id + "' does not exist!");

        Optional<Playlist> optionalPlaylist = playlistRepository.findById(playlist_id);

        if (!optionalPlaylist.isPresent()) {
            throw new IllegalStateException("Playlist with id " + playlist_id + " does not exist!");
        }

        Playlist playlist = optionalPlaylist.get();

        Song song = optionalSong.get();

        BelongsToKey btk = new BelongsToKey(song_id, playlist_id);

        if(playlist.getSongs_orders().contains(btk))
            throw new IllegalStateException("Playlist " + playlist.getName() + " already contains song " + song.getName());

        BelongsTo bt = new BelongsTo(btk, song, playlist, playlist.getSongs_orders().size());

        playlist.getSongs_orders().add(bt);
        song.getPlaylists_orders().add(bt);

        belongsToRepository.save(bt);
        playlistRepository.save(playlist);
        songService.save(song);
    }

    public void remove(UUID song_id, UUID playlist_id) {
        Optional<Song> optionalSong = songService.findSongById(song_id);

        if (!optionalSong.isPresent())
            throw new IllegalStateException("Song with ID '" + song_id + "' does not exist!");

        Optional<Playlist> optionalPlaylist = playlistRepository.findById(playlist_id);

        if (!optionalPlaylist.isPresent()) {
            throw new IllegalStateException("Playlist with id " + playlist_id + " does not exist!");
        }

        Playlist playlist = optionalPlaylist.get();

        Song song = optionalSong.get();

        BelongsToKey btk = new BelongsToKey(song_id, playlist.getPlaylist_ID());

        BelongsTo bt = belongsToRepository.findById(btk).get();

        song.getPlaylists_orders().remove(bt);
        playlist.getSongs_orders().remove(bt);

        songService.save(song);
        playlistRepository.save(playlist);
        belongsToRepository.delete(bt);

        belongsToService.makeOrder(playlist.getPlaylist_ID());
    }
    
    public List<PlaylistDTO> search(String search) {
    	return playlistRepository.findAll().stream()
    						.filter(p -> p.getPublic_access() && p.getName().toLowerCase().contains(search.toLowerCase()))
                                            .map(pl -> new PlaylistDTO(pl.getPlaylist_ID(),
                                                                        pl.getName(),
                                                                        pl.getModifiable(),
                                                                        pl.getPublic_access(),
                                                                        pl.getCreated(),
                                                                        pl.getLast_modified(),
                                                                        new AccountDTO(pl.getAccount().getAccount_ID(),
                                                                                        pl.getAccount().getPicture(),
                                                                                        pl.getAccount().getUsername(),
                                                                                        pl.getAccount().getStage_name(),
                                                                                        pl.getAccount().getDisplayName())
                                                                        ))
                                            .collect(Collectors.toList());
    }
}
