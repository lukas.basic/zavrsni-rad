package hr.fer.zavrsni.rad.syla.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Embeddable
public class BelongsToKey implements Serializable {

    @Column(name = "song_ID")
    private UUID song_ID;

    @Column(name = "playlist_ID")
    private UUID playlist_ID;

    public BelongsToKey() {}

    public BelongsToKey(UUID song_ID, UUID playlist_ID) {
        this.song_ID = song_ID;
        this.playlist_ID = playlist_ID;
    }

    public UUID getSong_ID() {
        return song_ID;
    }

    public void setSong_ID(UUID song_ID) {
        this.song_ID = song_ID;
    }

    public UUID getPlaylist_ID() {
        return playlist_ID;
    }

    public void setPlaylist_ID(UUID playlist_ID) {
        this.playlist_ID = playlist_ID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BelongsToKey that = (BelongsToKey) o;
        return getSong_ID().equals(that.getSong_ID()) && getPlaylist_ID().equals(that.getPlaylist_ID());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSong_ID(), getPlaylist_ID());
    }
}
