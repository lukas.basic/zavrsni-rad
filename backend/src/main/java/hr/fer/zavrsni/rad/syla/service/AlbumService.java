package hr.fer.zavrsni.rad.syla.service;

import hr.fer.zavrsni.rad.syla.dao.AccountRepository;
import hr.fer.zavrsni.rad.syla.dao.AlbumRepository;
import hr.fer.zavrsni.rad.syla.dao.SongRepository;
import hr.fer.zavrsni.rad.syla.dto.*;
import hr.fer.zavrsni.rad.syla.model.Account;
import hr.fer.zavrsni.rad.syla.model.Album;
import hr.fer.zavrsni.rad.syla.model.Song;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class AlbumService {

    private final FileStorageService fileStorageService;

    private final AlbumRepository albumRepository;

    private final AccountRepository accountRepository;

    private final SongService songService;

    @Autowired
    public AlbumService(AlbumRepository albumRepository,
                        AccountRepository accountRepository,
                        SongService songService,
                        FileStorageService fileStorageService) {
        this.albumRepository = albumRepository;
        this.accountRepository = accountRepository;
        this.songService = songService;
        this.fileStorageService = fileStorageService;
    }

    public List<AlbumDTO> getAlbums() {
        return albumRepository.findAll().stream()
                            .map(album -> new AlbumDTO(album.getAlbum_ID(),
                                                        album.getName(),
                                                        album.getPicture(),
                                                        album.getReleased(),
                                                        album.getAccounts().stream()
                                                                .map(acc -> new AccountDTO(acc.getAccount_ID(),
                                                                                            acc.getPicture(),
                                                                                            acc.getUsername(),
                                                                                            acc.getStage_name(),
                                                                                            acc.getDisplayName(),
                                                                                            acc.getAlbums().stream()
                                                                                                    .map(a -> new AlbumDTO(a.getAlbum_ID(),
                                                                                                                            a.getName(),
                                                                                                                            a.getPicture()))
                                                                                                    .collect(Collectors.toList())))
                                                                .collect(Collectors.toList())))
                            .collect(Collectors.toList());
    }

    public Map<String, String> addNewAlbum(AlbumDTO album, MultipartFile picture) {
        List<Account> accounts = new ArrayList<>();

        if (album.getAccount_ids().size() == 0)
            throw new IllegalArgumentException("Album must have at least one author!");

        for (UUID a_id : album.getAccount_ids()) {
            Optional<Account> acc = accountRepository.findById(a_id);

            if (!acc.isPresent())
                throw new IllegalArgumentException("Account with ID '" + a_id + "' does not exist!");

            Account account = acc.get();

            accounts.add(account);
        }

        Album album1 = new Album(album.getName(),
                                LocalDate.now(),
                                accounts);

        accounts.stream().forEach(a -> a.addAlbum(album1));

        albumRepository.save(album1);
        accounts.stream().forEach(a -> accountRepository.save(a));

        if (picture != null)
            uploadImage(picture, album1.getAlbum_ID());

        Map<String, String> res = new TreeMap<>();
        res.put("id", album1.getAlbum_ID().toString());
        return res;
    }

    public void deleteAlbum(UUID album_id) {
        Optional<Album> optionalAlbum = albumRepository.findById(album_id);

        if (!optionalAlbum.isPresent()) {
            throw new IllegalStateException("Album with ID '" + album_id + "' does not exist!");
        }

        Album album = optionalAlbum.get();

        while (!album.getSongs().isEmpty())
            songService.deleteSong(album.getSongs().get(0).getSong_ID());

        for (Account account : album.getAccounts()) {
            account.getAlbums().remove(album);
            accountRepository.save(account);
        }

        for (Account account : album.getLikes()) {
            account.getLikesAlbums().remove(album);
            accountRepository.save(account);
        }

        albumRepository.deleteById(album_id);
    }

    public void updateAlbum(AlbumDTO album) {
        Optional<Album> a = albumRepository.findById(album.getAlbum_ID());

        if (!a.isPresent())
            throw new IllegalArgumentException("Album with ID: " + album.getAlbum_ID() + " does not exist!");

        Album album1 = a.get();

        List<Account> accounts = new ArrayList<>();

        for (AccountDTO acc : album.getAccounts()) {
            Optional<Account> accountOptional = accountRepository.findById(acc.getAccount_ID());

            if (!accountOptional.isPresent())
                throw new IllegalStateException("Account with ID '" + acc.getAccount_ID() + "' doesn't exist!");

            if (!accountOptional.get().getAlbums().contains(album1))
                accountOptional.get().addAlbum(album1);

            accounts.add(accountOptional.get());
        }

        album1.setName(album.getName());
        album1.setAccounts(accounts);

        albumRepository.save(album1);

        for (Account acc : accounts)
            accountRepository.save(acc);
    }

    public Set<AlbumDTO> getAuthorsAlbums(List<UUID> ids) {
        Set<UUID> albums_ids = new HashSet<>();

        for (UUID id : ids) {
            Optional<Account> accountOptional = accountRepository.findById(id);

            if (!accountOptional.isPresent())
                throw new IllegalStateException("Account with ID: " + id + " does not exist!");

            List<Album> accountAlbums = accountOptional.get().getAlbums();

            for (Album album : accountAlbums) {
                albums_ids.add(album.getAlbum_ID());
            }
        }

        Set<AlbumDTO> albums = new HashSet<>();

        for (UUID id : albums_ids) {
            Album album = albumRepository.findById(id).get();
            albums.add(new AlbumDTO(album.getAlbum_ID(), album.getName(), album.getPicture(), album.getSongs().size()));
        }

        return albums;
    }

    public List<AccountDTO> getNotAuthors(UUID album_id) {
        Optional<Album> albumOptional = albumRepository.findById(album_id);

        if (!albumOptional.isPresent())
            throw new IllegalStateException("Album with ID '" + album_id + "' does not exist!");

        List<Account> accounts = albumOptional.get().getAccounts();

        return accountRepository.findAll().stream()
                .filter(a -> !accounts.contains(a))
                .map(acc -> new AccountDTO(acc.getAccount_ID(),
                                        acc.getPicture(),
                                        acc.getUsername(),
                                        acc.getStage_name(),
                                        acc.getDisplayName(),
                                        acc.getAlbums().stream()
                                                .map(a -> new AlbumDTO(a.getAlbum_ID(),
                                                        a.getName(),
                                                        a.getPicture()))
                                                .collect(Collectors.toList())))
                .collect(Collectors.toList());
    }

    public void uploadImage(MultipartFile picture, UUID album_ID) {
        Optional<Album> alb = albumRepository.findById(album_ID);

        if (!alb.isPresent())
            throw new IllegalStateException("Album with ID '" + album_ID + "' does not exist!");

        Album album = alb.get();

        if (fileStorageService.storeFile(picture, "images/" + album_ID.toString())) {
            album.setPicture("images/" + album_ID.toString());
        }

        albumRepository.save(album);
    }

    public Map<String, byte[]> downloadImage(String name) {
        Map<String, byte[]> resp = new HashMap<>();
        resp.put("data", fileStorageService.downloadFile(name));
        return resp;
    }

    public void removeAccount(Album album, Account account) {
        album.getAccounts().remove(account);
        albumRepository.save(album);
    }

    public void removeAlbumLike(Album album, Account account) {
        album.getLikes().remove(account);
        albumRepository.save(album);
    }

    public AlbumDTO getAlbum(UUID album_id) {
        Optional<Album> optionalAlbum = albumRepository.findById(album_id);

        if (!optionalAlbum.isPresent()) {
            throw new IllegalStateException("Album with ID '" + album_id + "' does not exist!");
        }

        Album album = optionalAlbum.get();

        return new AlbumDTO(album.getAlbum_ID(),
                            album.getName(),
                            album.getPicture(),
                            album.getReleased(),
                            album.getLikes().size(),
                            album.getAccounts().stream()
                                    .map(acc -> new AccountDTO(acc.getAccount_ID(),
                                            acc.getPicture(),
                                            acc.getUsername(),
                                            acc.getStage_name(),
                                            acc.getDisplayName(),
                                            acc.getAlbums().stream()
                                                    .map(a -> new AlbumDTO(a.getAlbum_ID(),
                                                            a.getName(),
                                                            a.getPicture()))
                                            .collect(Collectors.toList())))
                                    .collect(Collectors.toList()));
    }

    public List<SongDTO> getSongs(UUID album_id) {
        Optional<Album> optionalAlbum = albumRepository.findById(album_id);

        if (!optionalAlbum.isPresent()) {
            throw new IllegalStateException("Album with ID '" + album_id + "' does not exist!");
        }

        Album album = optionalAlbum.get();

        return album.getSongs().stream()
                                .sorted(Comparator.comparing(Song::getAlbum_order))
                                .map(s -> new SongDTO(s.getSong_ID(),
                                        s.getName(),
                                        s.getAudio(),
                                        s.getCollaborations(),
                                        s.getPopularity(),
                                        s.getReleased(),
                                        new AlbumDTO(s.getAlbum().getAlbum_ID(),
                                                s.getAlbum().getName(),
                                                s.getAlbum().getPicture(),
                                                s.getAlbum().getSongs().size()),
                                        s.getAlbum_order(),
                                        s.getAccounts().stream()
                                                .map(a -> new AccountDTO(a.getAccount_ID(),
                                                        a.getPicture(),
                                                        a.getUsername(),
                                                        a.getStage_name(),
                                                        a.getDisplayName(),
                                                        a.getAlbums().stream()
                                                                .map(alb -> new AlbumDTO(alb.getAlbum_ID(),
                                                                        alb.getName(),
                                                                        alb.getPicture()))
                                                                .collect(Collectors.toList())))
                                                .collect(Collectors.toList()),
                                        s.getGenres().stream()
                                                .map(g -> new GenreDTO(g.getGenre_ID(), g.getName()))
                                                .collect(Collectors.toList()),
                                        s.getComments().stream()
                                                .map(c -> new CommentDTO(c.getComment_ID(),
                                                        c.getContent(),
                                                        c.getCreated(),
                                                        c.getLast_modified(),
                                                        new AccountDTO(c.getAccount().getAccount_ID(),
                                                                c.getAccount().getPicture(),
                                                                c.getAccount().getDisplayName())))
                                                .collect(Collectors.toList())))
                                .collect(Collectors.toList());
    }

    public Boolean isLiked(String username, UUID album_id) {
        Optional<Album> optionalAlbum = albumRepository.findById(album_id);

        if (!optionalAlbum.isPresent()) {
            throw new IllegalStateException("Album with ID '" + album_id + "' does not exist!");
        }

        Optional<Account> accountOptional = accountRepository.findAccountByUsername(username);

        if (!accountOptional.isPresent())
            throw new IllegalStateException("Account with username '" + username + "' doesn't exist!");

        Album album = optionalAlbum.get();

        Account account = accountOptional.get();

        return album.getLikes().contains(account);
    }

    public void like(String username, UUID album_id) {
        Optional<Album> optionalAlbum = albumRepository.findById(album_id);

        if (!optionalAlbum.isPresent()) {
            throw new IllegalStateException("Album with ID '" + album_id + "' does not exist!");
        }

        Optional<Account> accountOptional = accountRepository.findAccountByUsername(username);

        if (!accountOptional.isPresent())
            throw new IllegalStateException("Account with username '" + username + "' doesn't exist!");

        Album album = optionalAlbum.get();

        Account account = accountOptional.get();

        if (!album.getLikes().contains(account)) {
            album.getLikes().add(account);
            account.getLikesAlbums().add(album);

            albumRepository.save(album);
            accountRepository.save(account);
        }
    }

    public void dislike(String username, UUID album_id) {
        Optional<Album> optionalAlbum = albumRepository.findById(album_id);

        if (!optionalAlbum.isPresent()) {
            throw new IllegalStateException("Album with ID '" + album_id + "' does not exist!");
        }

        Optional<Account> accountOptional = accountRepository.findAccountByUsername(username);

        if (!accountOptional.isPresent())
            throw new IllegalStateException("Account with username '" + username + "' doesn't exist!");

        Album album = optionalAlbum.get();

        Account account = accountOptional.get();

        if (album.getLikes().contains(account)) {
            album.getLikes().remove(account);
            account.getLikesAlbums().remove(album);

            albumRepository.save(album);
            accountRepository.save(account);
        }
    }

    public List<AlbumDTO> getNewestAlbums() {
    	return albumRepository.findAll().stream()
                .sorted((a, b) -> -a.getReleased().compareTo(b.getReleased()))
                .limit(4)
                .map(album -> new AlbumDTO(album.getAlbum_ID(),
                        album.getName(),
                        album.getPicture(),
                        album.getReleased(),
                        album.getAccounts().stream()
                                .map(acc -> new AccountDTO(acc.getAccount_ID(),
                                        acc.getPicture(),
                                        acc.getUsername(),
                                        acc.getStage_name(),
                                        acc.getDisplayName(),
                                        acc.getAlbums().stream()
                                                .map(a -> new AlbumDTO(a.getAlbum_ID(),
                                                        a.getName(),
                                                        a.getPicture()))
                                                .collect(Collectors.toList())))
                                .collect(Collectors.toList())))
                .collect(Collectors.toList());
    }

    public List<AlbumDTO> getMostPopularAlbums() {
        return albumRepository.findAll().stream()
                .sorted((a, b) -> -Integer.compare(a.getLikes().size(), b.getLikes().size()))
                .limit(4)
                .map(album -> new AlbumDTO(album.getAlbum_ID(),
                        album.getName(),
                        album.getPicture(),
                        album.getReleased(),
                        album.getAccounts().stream()
                                .map(acc -> new AccountDTO(acc.getAccount_ID(),
                                        acc.getPicture(),
                                        acc.getUsername(),
                                        acc.getStage_name(),
                                        acc.getDisplayName(),
                                        acc.getAlbums().stream()
                                                .map(a -> new AlbumDTO(a.getAlbum_ID(),
                                                        a.getName(),
                                                        a.getPicture()))
                                                .collect(Collectors.toList())))
                                .collect(Collectors.toList())))
                .collect(Collectors.toList());
    }

    public List<AlbumDTO> getNewestFollowingAlbums(UUID account_id) {
        Optional<Account> accountOptional = accountRepository.findById(account_id);

        if (!accountOptional.isPresent())
            throw new IllegalStateException("Account with ID '" + account_id + "' doesn't exist!");

        Account account = accountOptional.get();

        return account.getFollowing().stream()
                                .flatMap(acc -> acc.getAlbums().stream())
                                .distinct()
                                .sorted(Comparator.comparing(Album::getReleased))
                                .limit(4)
                                .map(album -> new AlbumDTO(album.getAlbum_ID(),
                                        album.getName(),
                                        album.getPicture(),
                                        album.getReleased(),
                                        album.getAccounts().stream()
                                                .map(acc -> new AccountDTO(acc.getAccount_ID(),
                                                        acc.getPicture(),
                                                        acc.getUsername(),
                                                        acc.getStage_name(),
                                                        acc.getDisplayName(),
                                                        acc.getAlbums().stream()
                                                                .map(a -> new AlbumDTO(a.getAlbum_ID(),
                                                                        a.getName(),
                                                                        a.getPicture()))
                                                                .collect(Collectors.toList())))
                                                .collect(Collectors.toList())))
                                .collect(Collectors.toList());
    }
    
    public List<AlbumDTO> search(String search) {
	    return albumRepository.findAll().stream()
	    				.filter(a -> a.getName().toLowerCase().contains(search.toLowerCase()))
		                    .map(album -> new AlbumDTO(album.getAlbum_ID(),
		                                                album.getName(),
		                                                album.getPicture(),
		                                                album.getReleased(),
		                                                album.getAccounts().stream()
		                                                        .map(acc -> new AccountDTO(acc.getAccount_ID(),
		                                                                                    acc.getPicture(),
		                                                                                    acc.getUsername(),
		                                                                                    acc.getStage_name(),
		                                                                                    acc.getDisplayName(),
		                                                                                    acc.getAlbums().stream()
		                                                                                            .map(a -> new AlbumDTO(a.getAlbum_ID(),
		                                                                                                                    a.getName(),
		                                                                                                                    a.getPicture()))
		                                                                                            .collect(Collectors.toList())))
		                                                        .collect(Collectors.toList())))
		                    .collect(Collectors.toList());
    }
}
