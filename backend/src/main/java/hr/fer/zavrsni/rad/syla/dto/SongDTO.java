package hr.fer.zavrsni.rad.syla.dto;

import hr.fer.zavrsni.rad.syla.model.Genre;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public class SongDTO {

    private UUID song_ID;
    private String name;
    private String audio;
    private String collaborationsString;
    private List<String> collaborations;
    private Integer popularity;
    private LocalDate released;
    private UUID album_ID;
    private AlbumDTO album;
    private Integer album_order;
    private Integer playlist_order;
    private List<AccountDTO> accounts;
    private List<UUID> account_ids;
    private List<GenreDTO> genres;
    private List<UUID> genre_ids;
    private List<CommentDTO> comments;

    public SongDTO() {}

    public SongDTO(String name,
                   List<String> collaborations,
                   UUID album_ID,
                   Integer album_order,
                   List<UUID> account_ids,
                   List<UUID> genre_ids) {
        this.name = name;
        this.collaborations = collaborations;
        this.album_ID = album_ID;
        this.album_order = album_order;
        this.account_ids = account_ids;
        this.genre_ids = genre_ids;
    }

    public SongDTO(String name,
                   List<AccountDTO> accounts,
                   List<String> collaborations,
                   UUID album_ID,
                   Integer album_order,
                   List<GenreDTO> genres) {
        this.name = name;
        this.collaborations = collaborations;
        this.album_ID = album_ID;
        this.album_order = album_order;
        this.accounts = accounts;
        this.genres = genres;
    }

    public SongDTO(UUID song_ID,
                   String name,
                   String audio,
                   String collaborations,
                   Integer playlist_order,
                   List<AccountDTO> accounts) {
        this.song_ID = song_ID;
        this.name = name;
        this.audio = audio;
        this.collaborationsString = collaborations;
        this.playlist_order = playlist_order;
        this.accounts = accounts;
    }

    public SongDTO(UUID song_ID, String name) {
        this.song_ID = song_ID;
        this.name = name;
    }

    public SongDTO(UUID song_ID,
                   String name,
                   String audio,
                   String collaborationsString,
                   Integer popularity,
                   LocalDate released,
                   Integer album_order,
                   List<AccountDTO> accounts,
                   List<GenreDTO> genres,
                   List<CommentDTO> comments) {
        this.song_ID = song_ID;
        this.name = name;
        this.audio = audio;
        this.collaborationsString = collaborationsString;
        this.popularity = popularity;
        this.released = released;
        this.album_order = album_order;
        this.accounts = accounts;
        this.genres = genres;
        this.comments = comments;
    }

    public SongDTO(UUID song_ID,
                   String name,
                   String audio,
                   String collaborationsString,
                   Integer popularity,
                   LocalDate released,
                   AlbumDTO album,
                   Integer album_order,
                   List<AccountDTO> accounts,
                   List<GenreDTO> genres,
                   List<CommentDTO> comments) {
        this.song_ID = song_ID;
        this.name = name;
        this.audio = audio;
        this.collaborationsString = collaborationsString;
        this.popularity = popularity;
        this.released = released;
        this.album = album;
        this.album_order = album_order;
        this.accounts = accounts;
        this.genres = genres;
        this.comments = comments;
    }

    public SongDTO(UUID song_ID,
                   String name,
                   String audio,
                   List<String> collaborations,
                   Integer popularity,
                   LocalDate released,
                   AlbumDTO album,
                   Integer album_order,
                   List<AccountDTO> accounts,
                   List<GenreDTO> genres,
                   List<CommentDTO> comments) {
        this.song_ID = song_ID;
        this.name = name;
        this.audio = audio;
        this.collaborations = collaborations;
        this.popularity = popularity;
        this.released = released;
        this.album = album;
        this.album_order = album_order;
        this.accounts = accounts;
        this.genres = genres;
        this.comments = comments;
    }

    public UUID getSong_ID() {
        return song_ID;
    }

    public void setSong_ID(UUID song_ID) {
        this.song_ID = song_ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public List<String> getCollaborations() {
        return collaborations;
    }

    public void setCollaborations(List<String> collaborations) {
        this.collaborations = collaborations;
    }

    public Integer getPopularity() {
        return popularity;
    }

    public void setPopularity(Integer popularity) {
        this.popularity = popularity;
    }

    public LocalDate getReleased() {
        return released;
    }

    public void setReleased(LocalDate released) {
        this.released = released;
    }

    public AlbumDTO getAlbum() {
        return album;
    }

    public void setAlbum(AlbumDTO album) {
        this.album = album;
    }

    public Integer getAlbum_order() {
        return album_order;
    }

    public void setAlbum_order(Integer album_order) {
        this.album_order = album_order;
    }

    public List<GenreDTO> getGenres() {
        return genres;
    }

    public void setGenres(List<GenreDTO> genres) {
        this.genres = genres;
    }

    public List<CommentDTO> getComments() {
        return comments;
    }

    public void setComments(List<CommentDTO> comments) {
        this.comments = comments;
    }

    public List<AccountDTO> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<AccountDTO> accounts) {
        this.accounts = accounts;
    }

    public UUID getAlbum_ID() {
        return album_ID;
    }

    public void setAlbum_ID(UUID album_ID) {
        this.album_ID = album_ID;
    }

    public List<UUID> getAccount_ids() {
        return account_ids;
    }

    public void setAccount_ids(List<UUID> account_ids) {
        this.account_ids = account_ids;
    }

    public List<UUID> getGenre_ids() {
        return genre_ids;
    }

    public void setGenre_ids(List<UUID> genre_ids) {
        this.genre_ids = genre_ids;
    }

    public String getCollaborationsString() {
        return collaborationsString;
    }

    public void setCollaborationsString(String collaborationsString) {
        this.collaborationsString = collaborationsString;
    }

    public Integer getPlaylist_order() {
        return playlist_order;
    }

    public void setPlaylist_order(Integer playlist_order) {
        this.playlist_order = playlist_order;
    }
}
