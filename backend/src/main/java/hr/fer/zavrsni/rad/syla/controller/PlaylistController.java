package hr.fer.zavrsni.rad.syla.controller;

import hr.fer.zavrsni.rad.syla.dto.PlaylistDTO;
import hr.fer.zavrsni.rad.syla.dto.SongDTO;
import hr.fer.zavrsni.rad.syla.model.Playlist;
import hr.fer.zavrsni.rad.syla.service.PlaylistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping(path = "api/v1/playlist")
public class PlaylistController {

    private final PlaylistService playlistService;

    @Autowired
    public PlaylistController(PlaylistService playlistService) {
        this.playlistService = playlistService;
    }

    @GetMapping
    public List<PlaylistDTO> getPlaylists() {
        return playlistService.getPlaylists();
    }
    
    @GetMapping(path = "/search")
    public List<PlaylistDTO> search(@RequestParam String search) {
        return playlistService.search(search);
    }

    @GetMapping(path = "/get")
    public PlaylistDTO getPlaylist(@RequestParam("playlist_ID") UUID playlist_ID) {
        return playlistService.getPlaylist(playlist_ID);
    }

    @GetMapping(path = "/favourites")
    public PlaylistDTO getFavourites(@RequestParam("account_ID") UUID account_ID) {
        return playlistService.getFavourites(account_ID);
    }

    @RequestMapping(path = "/add")
    public void add(@RequestParam(name = "song_ID") UUID song_ID,
                    @RequestParam(name = "playlist_ID") UUID playlist_ID) {
        playlistService.add(song_ID, playlist_ID);
    }

    @RequestMapping(path = "/remove")
    public void remove(@RequestParam(name = "song_ID") UUID song_ID,
                       @RequestParam(name = "playlist_ID") UUID playlist_ID) {
        playlistService.remove(song_ID, playlist_ID);
    }

    @PostMapping
    public void registerNewPlaylist(@RequestBody PlaylistDTO playlist) {
        playlistService.addNewPlaylist(playlist);
    }

    @GetMapping(path = "/delete")
    public void deletePlaylist(@RequestParam("playlist_ID") UUID playlist_ID) {
        playlistService.deletePlaylist(playlist_ID);
    }

    @PostMapping(path = "/update")
    public void updatePlaylist(@RequestBody PlaylistDTO playlist) {
        playlistService.updatePlaylist(playlist);
    }

    @GetMapping(path = "/songs")
    public List<SongDTO> getPlaylistSongs(@RequestParam UUID id) {
        return playlistService.getPlaylistSongs(id);
    }
}
