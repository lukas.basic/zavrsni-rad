package hr.fer.zavrsni.rad.syla.service;

import hr.fer.zavrsni.rad.syla.config.JwtTokenProvider;
import hr.fer.zavrsni.rad.syla.dao.AccountRepository;
import hr.fer.zavrsni.rad.syla.dao.BelongsToRepository;
import hr.fer.zavrsni.rad.syla.dto.AccountDTO;
import hr.fer.zavrsni.rad.syla.dto.AlbumDTO;
import hr.fer.zavrsni.rad.syla.dto.PlaylistDTO;
import hr.fer.zavrsni.rad.syla.model.*;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.apache.commons.codec.digest.DigestUtils;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class AccountService {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenProvider tokenProvider;

    private final FileStorageService fileStorageService;

    private final AccountRepository accountRepository;

    private final PlaylistService playlistService;

    private final SongService songService;

    private final AlbumService albumService;

    private final CommentService commentService;

    private final BelongsToService belongsToService;

    private final BelongsToRepository belongsToRepository;

    @Autowired
    public AccountService(AccountRepository accountRepository,
                          PlaylistService playlistService,
                          SongService songService,
                          AlbumService albumService,
                          CommentService commentService,
                          BelongsToService belongsToService,
                          BelongsToRepository belongsToRepository,
                          FileStorageService fileStorageService) {
        this.accountRepository = accountRepository;
        this.playlistService = playlistService;
        this.songService = songService;
        this.albumService = albumService;
        this.commentService = commentService;
        this.belongsToService = belongsToService;
        this.belongsToRepository = belongsToRepository;
        this.fileStorageService = fileStorageService;
    }

    public List<AccountDTO> getAccounts() {
        return accountRepository.findAll().stream()
                                        .map(a -> new AccountDTO(a.getAccount_ID(),
                                                                a.getUsername(),
                                                                a.getStage_name(),
                                                                a.getEmail(),
                                                                a.getPicture(),
                                                                a.getAppearance(),
                                                                a.getCreated(),
                                                                a.getAdmin(),
                                                                (a.getSongs().size() > 0)))
                                        .collect(Collectors.toList());
    }

    public void addNewAccount(AccountDTO account, MultipartFile picture) {
        Optional<Account> accountByUsername = accountRepository
                .findAccountByUsername(account.getUsername());

        Optional<Account> accountByEmail = accountRepository
                .findAccountByEmail(account.getEmail());

        if (accountByUsername.isPresent() && accountByEmail.isPresent())
            throw new IllegalStateException("Username and email taken!");

        if (accountByUsername.isPresent())
            throw new IllegalStateException("Username taken!");

        if (accountByEmail.isPresent())
            throw new IllegalStateException("Email taken!");

        Account acc = new Account(account.getUsername(),
                                    account.getStage_name(),
                                    account.getEmail(),
                                    DigestUtils.sha256Hex(account.getPassword()),
                                    account.getAppearance());

        accountRepository.save(acc);

        if (picture != null)
            uploadImage(picture, acc.getAccount_ID());
            
        playlistService.createFavourites(acc.getAccount_ID());
    }

    public void deleteAccount(UUID account_id) {
        Optional<Account> optionalAccount = accountRepository.findById(account_id);

        if (!optionalAccount.isPresent()) {
            throw new IllegalStateException("Account with ID '" + account_id + "' does not exist!");
        }

        Account account = optionalAccount.get();

        for (Playlist playlist : account.getPlaylists())
            playlistService.deletePlaylist(playlist.getPlaylist_ID());

        for (Comment comment : account.getComments())
            commentService.deleteComment(comment.getComment_ID());

        for (Album album : account.getLikesAlbums()) {
            albumService.removeAlbumLike(album, account);
        }

        List<Album> albums = new ArrayList<>(account.getAlbums());
        for (Album album : albums) {
            if (album.getAccounts().size() == 1)
                albumService.deleteAlbum(album.getAlbum_ID());
            else
                albumService.removeAccount(album, account);
        }

        List<Song> songs = new ArrayList<>(account.getSongs());
        for (Song song : songs) {
            if (song.getAccounts().size() == 1)
                songService.deleteSong(song.getSong_ID());
            else
                songService.removeAccount(song, account);
        }

        for (Account follower : account.getFollowers()) {
            follower.getFollowing().remove(account);
            accountRepository.save(follower);
        }

        for (Account following : account.getFollowing()) {
            following.getFollowers().remove(account);
            accountRepository.save(following);
        }

        accountRepository.deleteById(account_id);
    }

    public void updateAccount(AccountDTO account) {
        Optional<Account> acc = accountRepository.findById(account.getAccount_ID());

        if (!acc.isPresent())
            throw new IllegalStateException("Account with ID '" + account.getAccount_ID() + "' does not exist!");

        Account account1 = acc.get();

        account1.setStage_name(account.getStage_name());
        //account1.setPicture(account.getPicture());
        account1.setAppearance(account.getAppearance());

        accountRepository.save(account1);
    }

    public void uploadImage(MultipartFile picture, UUID account_ID) {
        Optional<Account> acc = accountRepository.findById(account_ID);

        if (!acc.isPresent())
            throw new IllegalStateException("Account with ID '" + account_ID + "' does not exist!");

        Account account = acc.get();

        if (fileStorageService.storeFile(picture, "images/" + account_ID.toString())) {
            account.setPicture("images/" + account_ID.toString());
        }

        accountRepository.save(account);
    }

    public Map<String, byte[]> downloadImage(String name) {
        Map<String, byte[]> resp = new HashMap<>();
        resp.put("data", fileStorageService.downloadFile(name));
        return resp;
    }

    public void removeAlbum(Account account, Album album) {
        account.getAlbums().remove(album);
        accountRepository.save(account);
    }

    public void removeAlbumLike(Account account, Album album) {
        account.getLikesAlbums().remove(album);
        accountRepository.save(account);
    }

    public ResponseEntity<String> authenticate(String email, String password) {
        JSONObject jsonObject = new JSONObject();
        try {
            Account acc = accountRepository.findAccountByEmail(email).get();
            String sha256hex = DigestUtils.sha256Hex(password);
            /*Authentication authentication = authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(email, password));*/
            if (//authentication.isAuthenticated()
                acc.getPassword().equals(sha256hex)) {
                jsonObject.put("account_ID", acc.getAccount_ID());
                jsonObject.put("name", acc.getUsername());
                jsonObject.put("authorities", acc.getAdmin() ? "ADMIN" : "USER");
                jsonObject.put("token", tokenProvider.createToken(email, acc.getAdmin()));
                return new ResponseEntity<String>(jsonObject.toString(), HttpStatus.OK);
            }
        } catch (JSONException | NoSuchElementException | AuthenticationException e) {
            try {
                jsonObject.put("exception", e.getMessage());
            } catch (JSONException e1) {
                e1.printStackTrace();
            }
            return new ResponseEntity<String>(jsonObject.toString(), HttpStatus.UNAUTHORIZED);
        }
        return null;
    }

    public AccountDTO getAccount(String username) {
        Optional<Account> optionalAccount = accountRepository.findAccountByUsername(username);

        if (!optionalAccount.isPresent()) {
            throw new IllegalStateException("Account with username '" + username + "' does not exist!");
        }

        Account a = optionalAccount.get();

        return new AccountDTO(a.getAccount_ID(),
                                a.getUsername(),
                                a.getStage_name(),
                                a.getEmail(),
                                a.getPicture(),
                                a.getAppearance(),
                                a.getCreated(),
                                a.getAdmin(),
                                (a.getSongs().size() > 0),
                                a.getFollowing().size(),
                                a.getFollowers().size());
    }

    public List<AlbumDTO> getAlbums(String username) {
        Optional<Account> optionalAccount = accountRepository.findAccountByUsername(username);

        if (!optionalAccount.isPresent()) {
            throw new IllegalStateException("Account with username '" + username + "' does not exist!");
        }

        Account account = optionalAccount.get();

        return account.getAlbums().stream()
                            .map(album -> new AlbumDTO(album.getAlbum_ID(),
                                    album.getName(),
                                    album.getPicture(),
                                    album.getReleased(),
                                    album.getAccounts().stream()
                                            .map(acc -> new AccountDTO(acc.getAccount_ID(),
                                                    acc.getPicture(),
                                                    acc.getUsername(),
                                                    acc.getStage_name(),
                                                    acc.getDisplayName(),
                                                    acc.getAlbums().stream()
                                                            .map(a -> new AlbumDTO(a.getAlbum_ID(),
                                                                    a.getName(),
                                                                    a.getPicture()))
                                                            .collect(Collectors.toList())))
                                            .collect(Collectors.toList())))
                            .collect(Collectors.toList());
    }

    public List<PlaylistDTO> getPublicPlaylists(String username) {
        Optional<Account> optionalAccount = accountRepository.findAccountByUsername(username);

        if (!optionalAccount.isPresent()) {
            throw new IllegalStateException("Account with username '" + username + "' does not exist!");
        }

        Account account = optionalAccount.get();

        return account.getPlaylists().stream()
                                    .filter(p -> p.getPublic_access())
                                    .map(playlist -> new PlaylistDTO(
                                            playlist.getPlaylist_ID(),
                                            playlist.getName(),
                                            playlist.getModifiable(),
                                            playlist.getPublic_access(),
                                            playlist.getCreated(),
                                            playlist.getLast_modified(),
                                            playlist.getAccount().getAccount_ID()
                                    ))
                                    .collect(Collectors.toList());
    }

    public List<PlaylistDTO> getMyPlaylists(UUID account_id) {
        Optional<Account> optionalAccount = accountRepository.findById(account_id);

        if (!optionalAccount.isPresent()) {
            throw new IllegalStateException("Account with ID '" + account_id + "' does not exist!");
        }

        Account account = optionalAccount.get();

        return account.getPlaylists().stream()
                                    .map(playlist -> new PlaylistDTO(
                                            playlist.getPlaylist_ID(),
                                            playlist.getName(),
                                            playlist.getModifiable(),
                                            playlist.getPublic_access(),
                                            playlist.getCreated(),
                                            playlist.getLast_modified(),
                                            playlist.getAccount().getAccount_ID()
                                    ))
                                    .collect(Collectors.toList());
    }

    public Boolean isFollowing(String username1, String username2) {
        Optional<Account> optionalAccount1 = accountRepository.findAccountByUsername(username1);

        if (!optionalAccount1.isPresent()) {
            throw new IllegalStateException("Account with username '" + username1 + "' does not exist!");
        }

        Optional<Account> optionalAccount2 = accountRepository.findAccountByUsername(username2);

        if (!optionalAccount2.isPresent()) {
            throw new IllegalStateException("Account with username '" + username2 + "' does not exist!");
        }

        Account account1 = optionalAccount1.get();

        Account account2 = optionalAccount2.get();

        return account1.getFollowing().contains(account2);
    }

    public void follow(String username1, String username2) {
        Optional<Account> optionalAccount1 = accountRepository.findAccountByUsername(username1);

        if (!optionalAccount1.isPresent()) {
            throw new IllegalStateException("Account with username '" + username1 + "' does not exist!");
        }

        Optional<Account> optionalAccount2 = accountRepository.findAccountByUsername(username2);

        if (!optionalAccount2.isPresent()) {
            throw new IllegalStateException("Account with username '" + username2 + "' does not exist!");
        }

        Account account1 = optionalAccount1.get();

        Account account2 = optionalAccount2.get();

        if(!account1.getFollowing().contains(account2)) {
            account1.getFollowing().add(account2);
            account2.getFollowers().add(account1);

            accountRepository.save(account1);
            accountRepository.save(account2);
        }
    }

    public void unfollow(String username1, String username2) {
        Optional<Account> optionalAccount1 = accountRepository.findAccountByUsername(username1);

        if (!optionalAccount1.isPresent()) {
            throw new IllegalStateException("Account with username '" + username1 + "' does not exist!");
        }

        Optional<Account> optionalAccount2 = accountRepository.findAccountByUsername(username2);

        if (!optionalAccount2.isPresent()) {
            throw new IllegalStateException("Account with username '" + username2 + "' does not exist!");
        }

        Account account1 = optionalAccount1.get();

        Account account2 = optionalAccount2.get();

        if(account1.getFollowing().contains(account2)) {
            account1.getFollowing().remove(account2);
            account2.getFollowers().remove(account1);

            accountRepository.save(account1);
            accountRepository.save(account2);
        }
    }

    public List<AlbumDTO> getLikedAlbums(String username, Integer limit) {
        Optional<Account> optionalAccount = accountRepository.findAccountByUsername(username);

        if (!optionalAccount.isPresent()) {
            throw new IllegalStateException("Account with username '" + username + "' does not exist!");
        }

        Account account = optionalAccount.get();

        if (limit == null)
            return account.getLikesAlbums().stream()
                    .map(album -> new AlbumDTO(album.getAlbum_ID(),
                            album.getName(),
                            album.getPicture(),
                            album.getReleased(),
                            album.getAccounts().stream()
                                    .map(acc -> new AccountDTO(acc.getAccount_ID(),
                                            acc.getPicture(),
                                            acc.getUsername(),
                                            acc.getStage_name(),
                                            acc.getDisplayName(),
                                            acc.getAlbums().stream()
                                                    .map(a -> new AlbumDTO(a.getAlbum_ID(),
                                                            a.getName(),
                                                            a.getPicture()))
                                                    .collect(Collectors.toList())))
                                    .collect(Collectors.toList())))
                    .collect(Collectors.toList());
        else
            return account.getLikesAlbums().stream()
                    .limit(limit)
                    .map(album -> new AlbumDTO(album.getAlbum_ID(),
                            album.getName(),
                            album.getPicture(),
                            album.getReleased(),
                            album.getAccounts().stream()
                                    .map(acc -> new AccountDTO(acc.getAccount_ID(),
                                            acc.getPicture(),
                                            acc.getUsername(),
                                            acc.getStage_name(),
                                            acc.getDisplayName(),
                                            acc.getAlbums().stream()
                                                    .map(a -> new AlbumDTO(a.getAlbum_ID(),
                                                            a.getName(),
                                                            a.getPicture()))
                                                    .collect(Collectors.toList())))
                                    .collect(Collectors.toList())))
                    .collect(Collectors.toList());
    }

    public Boolean likesSong(String username, UUID song_id) {
        Optional<Account> optionalAccount = accountRepository.findAccountByUsername(username);

        if (!optionalAccount.isPresent()) {
            throw new IllegalStateException("Account with username '" + username + "' does not exist!");
        }

        Optional<Song> optionalSong = songService.findSongById(song_id);

        if (!optionalSong.isPresent())
            throw new IllegalStateException("Song with ID '" + song_id + "' does not exist!");

        Account account = optionalAccount.get();

        Song song = optionalSong.get();

        Playlist favourites = account.getPlaylists().stream()
                                                    .filter(p -> !p.getModifiable())
                                                    .findFirst()
                                                    .get();

        return favourites.getSongs_orders().stream()
                                            .map(bt -> bt.getSong())
                                            .collect(Collectors.toList())
                                            .contains(song);
    }

    public void like(String username, UUID song_id) {
        if (likesSong(username, song_id))
            return;

        Optional<Account> optionalAccount = accountRepository.findAccountByUsername(username);

        if (!optionalAccount.isPresent()) {
            throw new IllegalStateException("Account with username '" + username + "' does not exist!");
        }

        Optional<Song> optionalSong = songService.findSongById(song_id);

        if (!optionalSong.isPresent())
            throw new IllegalStateException("Song with ID '" + song_id + "' does not exist!");

        Account account = optionalAccount.get();

        Song song = optionalSong.get();

        Playlist favourites = account.getPlaylists().stream()
                                                    .filter(p -> !p.getModifiable())
                                                    .findFirst()
                                                    .get();

        BelongsToKey btk = new BelongsToKey(song_id, favourites.getPlaylist_ID());

        BelongsTo bt = new BelongsTo(btk, song, favourites, favourites.getSongs_orders().size());

        favourites.getSongs_orders().add(bt);
        song.getPlaylists_orders().add(bt);

        belongsToRepository.save(bt);
        playlistService.save(favourites);
        songService.save(song);
    }

    public void dislike(String username, UUID song_id) {
        if (!likesSong(username, song_id))
            return;

        Optional<Account> optionalAccount = accountRepository.findAccountByUsername(username);

        if (!optionalAccount.isPresent()) {
            throw new IllegalStateException("Account with username '" + username + "' does not exist!");
        }

        Optional<Song> optionalSong = songService.findSongById(song_id);

        if (!optionalSong.isPresent())
            throw new IllegalStateException("Song with ID '" + song_id + "' does not exist!");

        Account account = optionalAccount.get();

        Song song = optionalSong.get();

        Playlist favourites = account.getPlaylists().stream()
                                                    .filter(p -> !p.getModifiable())
                                                    .findFirst()
                                                    .get();

        BelongsToKey btk = new BelongsToKey(song_id, favourites.getPlaylist_ID());

        BelongsTo bt = belongsToRepository.findById(btk).get();

        song.getPlaylists_orders().remove(bt);
        favourites.getSongs_orders().remove(bt);

        songService.save(song);
        playlistService.save(favourites);
        belongsToRepository.delete(bt);

        belongsToService.makeOrder(favourites.getPlaylist_ID());
    }

    public List<AccountDTO> getMostPopularArtists() {
        return accountRepository.findAll().stream()
                .filter(a -> a.isArtist())
                .sorted((a, b) -> -Integer.compare(a.getFollowers().size(), b.getFollowers().size()))
                .limit(4)
                .map(a -> new AccountDTO(a.getAccount_ID(),
                        a.getUsername(),
                        a.getStage_name(),
                        a.getEmail(),
                        a.getPicture(),
                        a.getAppearance(),
                        a.getCreated(),
                        a.getAdmin(),
                        (a.getSongs().size() > 0)))
                .collect(Collectors.toList());
    }
    
    public List<AccountDTO> search(String search, String type) {
    	if (type.equals("a"))
            return accountRepository.findAll().stream()
            				.filter(a -> a.isArtist())
            				.filter(a -> a.getUsername().toLowerCase().contains(search.toLowerCase()) || a.getStage_name().toLowerCase().contains(search.toLowerCase()))
                                        .map(a -> new AccountDTO(a.getAccount_ID(),
                                                                a.getUsername(),
                                                                a.getStage_name(),
                                                                a.getEmail(),
                                                                a.getPicture(),
                                                                a.getAppearance(),
                                                                a.getCreated(),
                                                                a.getAdmin(),
                                                                (a.getSongs().size() > 0)))
                                        .collect(Collectors.toList());
        else if (type.equals("p"))
            return accountRepository.findAll().stream()
            				.filter(a -> !a.isArtist())
            				.filter(a -> a.getUsername().toLowerCase().contains(search.toLowerCase()) || a.getStage_name().toLowerCase().contains(search.toLowerCase()))
                                        .map(a -> new AccountDTO(a.getAccount_ID(),
                                                                a.getUsername(),
                                                                a.getStage_name(),
                                                                a.getEmail(),
                                                                a.getPicture(),
                                                                a.getAppearance(),
                                                                a.getCreated(),
                                                                a.getAdmin(),
                                                                (a.getSongs().size() > 0)))
                                        .collect(Collectors.toList());
        else
        	return new ArrayList<AccountDTO>();
    }
}
