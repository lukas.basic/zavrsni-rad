package hr.fer.zavrsni.rad.syla.service;

import com.google.api.client.http.GenericUrl;
import com.google.api.services.storage.model.StorageObject;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

@Service
public class FileStorageService {

    @Autowired
    private Storage storage;

    public Boolean storeFile(MultipartFile file, String name) {
        BlobId blobId = BlobId.of("syla-bucket", name);
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).build();

        try {
            byte[] data = file.getBytes();
            storage.create(blobInfo, data);
        } catch (IOException ex) {
            return false;
        }

        return true;
    }

    public byte[] downloadFile(String fileName) {
        return storage.get("syla-bucket", fileName).getContent();
    }
}
