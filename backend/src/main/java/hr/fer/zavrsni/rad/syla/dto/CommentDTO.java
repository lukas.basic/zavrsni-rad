package hr.fer.zavrsni.rad.syla.dto;

import hr.fer.zavrsni.rad.syla.model.Account;

import java.time.LocalDate;
import java.util.UUID;

public class CommentDTO {

    private UUID comment_ID;
    private String content;
    private LocalDate created;
    private LocalDate last_modified;
    private AccountDTO account;
    private UUID account_id;
    private SongDTO song;
    private UUID song_id;

    public CommentDTO() {}

    public CommentDTO(String content, AccountDTO account, SongDTO song) {
        this.content = content;
        this.account = account;
        this.song = song;
    }

    public CommentDTO(String content, UUID account_id, UUID song_id) {
        this.content = content;
        this.account_id = account_id;
        this.song_id = song_id;
    }

    public CommentDTO(UUID comment_ID, String content) {
        this.comment_ID = comment_ID;
        this.content = content;
    }

    public CommentDTO(UUID comment_ID,
                      String content,
                      LocalDate created,
                      LocalDate last_modified,
                      AccountDTO account,
                      SongDTO song) {
        this.comment_ID = comment_ID;
        this.content = content;
        this.created = created;
        this.last_modified = last_modified;
        this.account = account;
        this.song = song;
    }

    public CommentDTO(UUID comment_ID,
                      String content,
                      LocalDate created,
                      LocalDate last_modified,
                      AccountDTO account) {
        this.comment_ID = comment_ID;
        this.content = content;
        this.created = created;
        this.last_modified = last_modified;
        this.account = account;
    }

    public UUID getComment_ID() {
        return comment_ID;
    }

    public void setComment_ID(UUID comment_ID) {
        this.comment_ID = comment_ID;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDate getCreated() {
        return created;
    }

    public void setCreated(LocalDate created) {
        this.created = created;
    }

    public LocalDate getLast_modified() {
        return last_modified;
    }

    public void setLast_modified(LocalDate last_modified) {
        this.last_modified = last_modified;
    }

    public AccountDTO getAccount() {
        return account;
    }

    public void setAccount(AccountDTO account) {
        this.account = account;
    }

    public SongDTO getSong() {
        return song;
    }

    public void setSong(SongDTO song) {
        this.song = song;
    }

    public UUID getAccount_id() {
        return account_id;
    }

    public void setAccount_id(UUID account_id) {
        this.account_id = account_id;
    }

    public UUID getSong_id() {
        return song_id;
    }

    public void setSong_id(UUID song_id) {
        this.song_id = song_id;
    }
}
