package hr.fer.zavrsni.rad.syla.dto;

import java.util.UUID;

public class GenreDTO {
    private UUID genre_ID;
    private String name;

    public GenreDTO() {}

    public GenreDTO(UUID genre_ID, String name) {
        this.genre_ID = genre_ID;
        this.name = name;
    }

    public GenreDTO(String name) {
        this.name = name;
    }

    public UUID getGenre_ID() {
        return genre_ID;
    }

    public void setGenre_ID(UUID genre_ID) {
        this.genre_ID = genre_ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "GenreDTO{" +
                "genre_ID=" + genre_ID +
                ", name='" + name + '\'' +
                '}';
    }
}
