package hr.fer.zavrsni.rad.syla.model;

import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "account_id", updatable = false, nullable = false)
    private UUID account_ID;

    @Column(name = "username")
    private String username;

    @Column(name = "stage_name")
    private String stage_name;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "picture")
    private String picture;

    @Column(name = "appearance")
    private Boolean appearance;

    @Column(name = "admin")
    private Boolean admin;

    @CreatedDate
    @Column(name = "created", nullable = false, updatable = false)
    private LocalDate created;

    @OneToMany(targetEntity = Playlist.class, fetch = FetchType.EAGER, mappedBy = "account")
    private List<Playlist> playlists;

    @ManyToMany
    @JoinTable(
            name = "released",
            joinColumns = @JoinColumn(name = "account_ID"),
            inverseJoinColumns = @JoinColumn(name = "album_ID"))
    private List<Album> albums;

    @OneToMany(mappedBy = "account")
    private List<Comment> comments;

    @ManyToMany(mappedBy = "accounts")
    private List<Song> songs;

    @ManyToMany
    @JoinTable(
            name = "likes_a",
            joinColumns = @JoinColumn(name = "account_ID"),
            inverseJoinColumns = @JoinColumn(name = "album_ID"))
    private List<Album> likesAlbums;

    @ManyToMany(mappedBy = "following", cascade = CascadeType.ALL)
    private List<Account> followers;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name="likes_u",
            joinColumns={@JoinColumn(name="account_ID2")},
            inverseJoinColumns={@JoinColumn(name="account_ID1")})
    private List<Account> following;

    public Account() {
        this.picture = "images/default_profile_picture.png";
        this.playlists = new ArrayList<>();
        this.albums = new ArrayList<>();
        this.comments = new ArrayList<>();
        this.songs = new ArrayList<>();
        this.likesAlbums = new ArrayList<>();
        this.followers = new ArrayList<>();
        this.following = new ArrayList<>();
        this.admin = false;
    }

    public Account(String username,
                      String stage_name,
                      String email,
                      String password,
                      Boolean appearance) {
        this();

        this.username = username;
        this.stage_name = stage_name;
        this.email = email;
        this.password = password;
        this.appearance = appearance;
    }
    
    public Account(String username,
                      String stage_name,
                      String email,
                      String password,
                      Boolean appearance,
                      Playlist favourites) {
        this();

        this.username = username;
        this.stage_name = stage_name;
        this.email = email;
        this.password = password;
        this.appearance = appearance;
        this.playlists.add(favourites);
    }

    public Account(UUID account_ID,
                   String username,
                   String stage_name,
                   String email,
                   String password,
                   String picture,
                   Boolean appearance,
                   LocalDate created) {
        this();

        this.account_ID = account_ID;
        this.username = username;
        this.stage_name = stage_name;
        this.email = email;
        this.password = password;
        this.picture = picture;
        this.appearance = appearance;
        this.created = created;
    }

    public Account(String username,
                   String stage_name,
                   String email,
                   String password,
                   String picture,
                   Boolean appearance,
                   LocalDate created) {
        this();

        this.username = username;
        this.stage_name = stage_name;
        this.email = email;
        this.password = password;
        this.picture = picture;
        this.appearance = appearance;
        this.created = created;
    }

    public Account(String username,
                   String stage_name,
                   String email,
                   String password,
                   String picture,
                   Boolean appearance) {
        this();

        this.username = username;
        this.stage_name = stage_name;
        this.email = email;
        this.password = password;
        this.picture = picture;
        this.appearance = appearance;
    }

    @PrePersist
    private void onCreate() {
        created = LocalDate.now();
    }

    public UUID getAccount_ID() {
        return account_ID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getStage_name() {
        return stage_name;
    }

    public void setStage_name(String stage_name) {
        this.stage_name = stage_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Boolean getAppearance() {
        return appearance;
    }

    public void setAppearance(Boolean appearance) {
        this.appearance = appearance;
    }

    public LocalDate getCreated() {
        return created;
    }

    public List<Playlist> getPlaylists() {
        return playlists;
    }

    public List<Album> getAlbums() {
        return albums;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    public List<Song> getSongs() {
        return songs;
    }

    public List<Album> getLikesAlbums() {
        return likesAlbums;
    }

    public List<Account> getFollowers() {
        return followers;
    }

    public List<Account> getFollowing() {
        return following;
    }

    public void addAlbum(Album album) {
        this.albums.add(album);
    }

    public void addComment(Comment comment) {
        this.comments.add(comment);
    }

    public void addPlaylist(Playlist playlist) {
        this.playlists.add(playlist);
    }
    
    public void removePlaylist(Playlist playlist) {
        this.playlists.remove(playlist);
    }

    public void addSong(Song song) {
        this.songs.add(song);
    }

    public void addLikesAlbum(Album likesAlbum) {
        this.likesAlbums.add(likesAlbum);
    }

    public void addFollower(Account follower) {
        this.followers.add(follower);
    }

    public void addFollowing(Account following) {
        this.following.add(following);
    }

    public String getDisplayName() {
        if (this.appearance && this.stage_name != null)
            return this.stage_name;

        return this.username;
    }

    public Boolean isArtist() {
        return this.songs.size() != 0;
    }
}
