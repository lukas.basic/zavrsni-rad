package hr.fer.zavrsni.rad.syla.controller;

import hr.fer.zavrsni.rad.syla.dto.AccountDTO;
import hr.fer.zavrsni.rad.syla.dto.AlbumDTO;
import hr.fer.zavrsni.rad.syla.dto.SongDTO;
import hr.fer.zavrsni.rad.syla.model.Album;
import hr.fer.zavrsni.rad.syla.service.AlbumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping(path = "api/v1/album")
public class AlbumController {

    private final AlbumService albumService;

    @Autowired
    public AlbumController(AlbumService albumService) {
        this.albumService = albumService;
    }

    @GetMapping
    public List<AlbumDTO> getAlbums() {
        return albumService.getAlbums();
    }
    
    @GetMapping(path = "/search")
    public List<AlbumDTO> search(@RequestParam String search) {
        return albumService.search(search);
    }

    @GetMapping(path = "/newest")
    public List<AlbumDTO> getNewestAlbums() {
        return albumService.getNewestAlbums();
    }

    @GetMapping(path = "/most-popular")
    public List<AlbumDTO> getMostPopularAlbums() {
        return albumService.getMostPopularAlbums();
    }

    @GetMapping(path = "/newest-following")
    public List<AlbumDTO> getNewestFollowingAlbums(@RequestParam("id") UUID account_ID) {
        return albumService.getNewestFollowingAlbums(account_ID);
    }

    @GetMapping(path = "/get")
    public AlbumDTO getAlbum(@RequestParam("album_ID") UUID album_ID) {
        return albumService.getAlbum(album_ID);
    }

    @GetMapping(path = "/liked")
    public Boolean isLiked(@RequestParam("username") String username,
                            @RequestParam("album_ID") UUID album_ID) {
        return albumService.isLiked(username, album_ID);
    }

    @PostMapping(path = "/like")
    public void like(@RequestParam("username") String username,
                            @RequestParam("album_ID") UUID album_ID) {
        albumService.like(username, album_ID);
    }

    @PostMapping(path = "/dislike")
    public void dislike(@RequestParam("username") String username,
                            @RequestParam("album_ID") UUID album_ID) {
        albumService.dislike(username, album_ID);
    }

    @GetMapping(path = "/songs")
    public List<SongDTO> getSongs(@RequestParam("album_ID") UUID album_ID) {
        return albumService.getSongs(album_ID);
    }

    @RequestMapping
    public Map<String, String> addNewAlbum(@RequestParam(name = "image") MultipartFile picture,
                                   @RequestParam(name = "name") String name,
                                   @RequestParam(name = "account_id") UUID ...account_ids) {
        return albumService.addNewAlbum(new AlbumDTO(name, List.of(account_ids)), picture);
    }

    @PostMapping(path = "/default-picture")
    public Map<String, String> addNewAlbum(@RequestParam(name = "name") String name,
                            @RequestParam(name = "account_id") UUID ...account_ids) {
        return albumService.addNewAlbum(new AlbumDTO(name, List.of(account_ids)), null);
    }

    @GetMapping(path = "/delete")
    public void deleteAlbum(@RequestParam("album_ID") UUID album_ID) {
        albumService.deleteAlbum(album_ID);
    }

    @PostMapping(path = "/update")
    public void updateAlbum(@RequestBody AlbumDTO album) {
        albumService.updateAlbum(album);
    }

    @GetMapping(path = "/authors")
    public Set<AlbumDTO> getAuthorsAlbums(@RequestParam List<UUID> ids) {
        return albumService.getAuthorsAlbums(ids);
    }

    @GetMapping(path = "/not-authors")
    public List<AccountDTO> getNotAuthors(@RequestParam UUID album_ID) {
        return albumService.getNotAuthors(album_ID);
    }

    @RequestMapping(path = "/image", produces = {MediaType.IMAGE_PNG_VALUE, "application/json"})
    public void uploadImage(@RequestParam("image") MultipartFile file,
                            @RequestParam("album_ID") UUID album_ID) {
        albumService.uploadImage(file, album_ID);
    }

    @GetMapping(path = "/image")
    public Map<String, byte[]> downloadImage(@RequestParam("image_name") String name) {
        return albumService.downloadImage(name);
    }
}
