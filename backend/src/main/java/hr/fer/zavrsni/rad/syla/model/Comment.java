package hr.fer.zavrsni.rad.syla.model;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.UUID;

@Entity
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "comment_ID", updatable = false, nullable = false)
    private UUID comment_ID;

    @Column(name = "content")
    private String content;

    @CreatedDate
    @Column(name = "created", nullable = false, updatable = false)
    private LocalDate created;

    @LastModifiedDate
    @Column(name = "last_modified")
    private LocalDate last_modified;

    @ManyToOne
    @JoinColumn(name="account_id")
    private Account account;

    @ManyToOne
    @JoinColumn(name="song_id")
    private Song song;

    public Comment() {}

    public Comment(String content,
                   LocalDate created,
                   Account account,
                   Song song) {
        this.content = content;
        this.created = created;
        this.account = account;
        this.song = song;
    }

    public Comment(UUID comment_ID,
                   String content,
                   LocalDate created,
                   LocalDate last_modified,
                   Account account,
                   Song song) {
        this.comment_ID = comment_ID;
        this.content = content;
        this.created = created;
        this.last_modified = last_modified;
        this.account = account;
        this.song = song;
    }

    public Comment(String content,
                   Account account,
                   Song song) {
        this.content = content;
        this.account = account;
        this.song = song;
    }

    @PrePersist
    private void onCreate() {
        created = LocalDate.now();
    }

    public UUID getComment_ID() {
        return comment_ID;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDate getCreated() {
        return created;
    }

    public LocalDate getLast_modified() {
        return last_modified;
    }

    public void setLast_modified(LocalDate last_modified) {
        this.last_modified = last_modified;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Song getSong() {
        return song;
    }

    public void setSong(Song song) {
        this.song = song;
    }
}
