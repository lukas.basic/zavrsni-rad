package hr.fer.zavrsni.rad.syla.controller;

import hr.fer.zavrsni.rad.syla.dto.CommentDTO;
import hr.fer.zavrsni.rad.syla.model.Comment;
import hr.fer.zavrsni.rad.syla.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping(path = "api/v1/comment")
public class CommentController {

    private final CommentService commentService;

    @Autowired
    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @GetMapping
    public List<CommentDTO> getComments() {
        return commentService.getComments();
    }

    /*@PostMapping
    public void newComment(@RequestBody CommentDTO comment) {
        commentService.newComment(comment);
    }*/

    @GetMapping(path = "/delete")
    public void deleteComment(@RequestParam("comment_ID") UUID comment_ID) {
        commentService.deleteComment(comment_ID);
    }

    @PostMapping(path = "/update")
    public void updateComment(@RequestBody CommentDTO comment) {
        commentService.updateComment(comment);
    }
}
