package hr.fer.zavrsni.rad.syla.controller;

import hr.fer.zavrsni.rad.syla.dto.AccountDTO;
import hr.fer.zavrsni.rad.syla.dto.AlbumDTO;
import hr.fer.zavrsni.rad.syla.dto.PlaylistDTO;
import hr.fer.zavrsni.rad.syla.model.Account;
import hr.fer.zavrsni.rad.syla.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping(path = "api/v1/account")
public class AccountController {

    private final AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @RequestMapping(path = "/authenticate")
    public ResponseEntity<String> authenticate(@RequestParam(name = "email") String email,
                                               @RequestParam(name = "password") String password) {
        return accountService.authenticate(email, password);
    }

    @GetMapping
    public List<AccountDTO> getAccounts() {
        return accountService.getAccounts();
    }
    
    @GetMapping(path = "/search")
    public List<AccountDTO> search(@RequestParam String search,
    					@RequestParam String type) {
        return accountService.search(search, type);
    }

    @GetMapping(path = "/most-popular")
    public List<AccountDTO> getMostPopularArtists() {
        return accountService.getMostPopularArtists();
    }

    @GetMapping(path = "/get")
    public AccountDTO getAccount(@RequestParam("username") String username) {
        return accountService.getAccount(username);
    }

    @GetMapping(path = "/albums")
    public List<AlbumDTO> getAlbums(@RequestParam("username") String username) {
        return accountService.getAlbums(username);
    }

    @GetMapping(path = "/liked-albums")
    public List<AlbumDTO> getLikedAlbums(@RequestParam("username") String username,
                                        @RequestParam(required = false) Integer limit) {
        return accountService.getLikedAlbums(username, limit);
    }

    @GetMapping(path = "/public-playlists")
    public List<PlaylistDTO> getPublicPlaylists(@RequestParam("username") String username) {
        return accountService.getPublicPlaylists(username);
    }

    @GetMapping(path = "/my-playlists")
    public List<PlaylistDTO> getMyPlaylists(@RequestParam("account_ID") UUID account_ID) {
        return accountService.getMyPlaylists(account_ID);
    }

    @GetMapping(path = "/likes-song")
    public Boolean likesSong(@RequestParam("username") String username,
                               @RequestParam("song_ID") UUID song_ID) {
        return accountService.likesSong(username, song_ID);
    }

    @RequestMapping(path = "/like")
    public void like(@RequestParam(name = "username") String username,
                       @RequestParam(name = "song_ID") UUID song_ID) {
        accountService.like(username, song_ID);
    }

    @RequestMapping(path = "/dislike")
    public void dislike(@RequestParam(name = "username") String username,
                       @RequestParam(name = "song_ID") UUID song_ID) {
        accountService.dislike(username, song_ID);
    }

    @GetMapping(path = "/following")
    public Boolean isFollowing(@RequestParam("username1") String username1,
                               @RequestParam("username2") String username2) {
        return accountService.isFollowing(username1, username2);
    }

    @RequestMapping(path = "/follow")
    public void follow(@RequestParam(name = "username1") String username1,
                       @RequestParam(name = "username2") String username2) {
        accountService.follow(username1, username2);
    }

    @RequestMapping(path = "/unfollow")
    public void unfollow(@RequestParam(name = "username1") String username1,
                        @RequestParam(name = "username2") String username2) {
        accountService.unfollow(username1, username2);
    }

    @RequestMapping
    public void registerNewAccount(@RequestParam(name = "image") MultipartFile picture,
                                   @RequestParam(name = "username") String username,
                                   @RequestParam(name = "stage_name") String stage_name,
                                   @RequestParam(name = "email") String email,
                                   @RequestParam(name = "password") String password,
                                   @RequestParam(name = "appearance") Boolean appearance) {
        accountService.addNewAccount(new AccountDTO(username, stage_name, email, password, appearance), picture);
    }

    @PostMapping(path = "/default-picture")
    public void registerNewAccount(@RequestParam(name = "username") String username,
                                   @RequestParam(name = "stage_name") String stage_name,
                                   @RequestParam(name = "email") String email,
                                   @RequestParam(name = "password") String password,
                                   @RequestParam(name = "appearance") Boolean appearance) {
        accountService.addNewAccount(new AccountDTO(username, stage_name, email, password, appearance), null);
    }

    @GetMapping(path = "/delete")
    public void deleteAccount(@RequestParam("account_ID") UUID account_ID) {
        accountService.deleteAccount(account_ID);
    }

    @PostMapping(path = "/update")
    public void updateAccount(@RequestBody AccountDTO account) {
        accountService.updateAccount(account);
    }

    @RequestMapping(path = "/image", produces = {MediaType.IMAGE_PNG_VALUE, "application/json"})
    public void uploadImage(@RequestParam("image") MultipartFile file,
                            @RequestParam("account_ID") UUID account_ID) {
        accountService.uploadImage(file, account_ID);
    }

    @GetMapping(path = "/image")
    public Map<String, byte[]> downloadImage(@RequestParam("image_name") String name) {
        return accountService.downloadImage(name);
    }
}
