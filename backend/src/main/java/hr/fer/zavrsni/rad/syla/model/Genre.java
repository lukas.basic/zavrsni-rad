package hr.fer.zavrsni.rad.syla.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
public class Genre {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "genre_ID", updatable = false, nullable = false)
    private UUID genre_ID;

    @Column(name = "name")
    private String name;

    @ManyToMany(mappedBy = "genres")
    private List<Song> songs;

    public Genre() {
        this.songs = new ArrayList<>();
    }

    public Genre(String name) {
        this();

        this.name = name;
    }

    public Genre(UUID genre_ID, String name) {
        this();

        this.genre_ID = genre_ID;
        this.name = name;
    }

    public Genre(UUID genre_ID, String name, List<Song> songs) {
        this.genre_ID = genre_ID;
        this.name = name;
        this.songs = songs;
    }

    public UUID getGenre_ID() {
        return genre_ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Song> getSongs() {
        return songs;
    }

    public void addSong(Song song) {
        this.songs.add(song);
    }
}
