package hr.fer.zavrsni.rad.syla.model;

import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
public class Song {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "song_ID", updatable = false, nullable = false)
    private UUID song_ID;

    @Column(name = "name")
    private String name;

    @Column(name = "audio")
    private String audio;

    @Column(name = "collaborations")
    private String collaborations;

    @Column(name = "popularity")
    private Integer popularity;

    @CreatedDate
    @Column(name = "released", nullable = false, updatable = false)
    private LocalDate released;

    @Column(name = "album_order")
    private Integer album_order;

    @ManyToOne
    @JoinColumn(name="album_ID")
    private Album album;

    @ManyToMany
    @JoinTable(
            name = "is_genre",
            joinColumns = @JoinColumn(name = "song_ID"),
            inverseJoinColumns = @JoinColumn(name = "genre_ID"))
    private List<Genre> genres;

    @ManyToMany
    @JoinTable(
            name = "composed",
            joinColumns = @JoinColumn(name = "song_ID"),
            inverseJoinColumns = @JoinColumn(name = "account_ID"))
    private List<Account> accounts;

    @OneToMany(mappedBy = "song")
    private List<BelongsTo> playlists_orders;

    @OneToMany(mappedBy = "song")
    private List<Comment> comments;

    public Song() {
        this.genres = new ArrayList<>();
        this.accounts = new ArrayList<>();
        this.comments = new ArrayList<>();
        this.playlists_orders = new ArrayList<>();
    }

    public Song(UUID song_ID,
                String name,
                String audio,
                String collaborations,
                Integer popularity,
                LocalDate released,
                Integer album_order,
                Album album) {
        this();

        this.song_ID = song_ID;
        this.name = name;
        this.audio = audio;
        this.collaborations = collaborations;
        this.popularity = popularity;
        this.released = released;
        this.album_order = album_order;
        this.album = album;
    }

    public Song(String name,
                String audio,
                String collaborations,
                Integer popularity,
                LocalDate released,
                Integer album_order,
                Album album) {
        this();

        this.name = name;
        this.audio = audio;
        this.collaborations = collaborations;
        this.popularity = popularity;
        this.released = released;
        this.album_order = album_order;
        this.album = album;
    }

    public Song(String name,
                String collaborations,
                LocalDate released,
                Integer album_order,
                Album album,
                List<Account> accounts,
                List<Genre> genres) {
        this();

        this.name = name;
        this.collaborations = collaborations;
        this.released = released;
        this.album_order = album_order;
        this.album = album;
        this.accounts = accounts;
        this.genres = genres;
    }

    public Song(String name,
                String audio,
                String collaborations,
                LocalDate released,
                Integer album_order,
                Album album,
                List<Account> accounts,
                List<Genre> genres) {
        this();

        this.name = name;
        this.audio = audio;
        this.collaborations = collaborations;
        this.released = released;
        this.album_order = album_order;
        this.album = album;
        this.accounts = accounts;
        this.genres = genres;
    }

    @PrePersist
    private void onCreate() {
        this.released = LocalDate.now();
    }

    public UUID getSong_ID() {
        return song_ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public String getCollaborations() {
        return collaborations;
    }

    public void setCollaborations(String collaborations) {
        this.collaborations = collaborations;
    }

    public Integer getPopularity() {
        return popularity;
    }

    public void setPopularity(Integer popularity) {
        this.popularity = popularity;
    }

    public LocalDate getReleased() {
        return released;
    }

    public Integer getAlbum_order() {
        return album_order;
    }

    public void setAlbum_order(Integer album_order) {
        this.album_order = album_order;
    }

    public Album getAlbum() {
        return album;
    }

    public void setAlbum(Album album) {
        this.album = album;
    }

    public List<Genre> getGenres() {
        return genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    public void addGenre(Genre genre) {
        this.genres.add(genre);
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void addAccount(Account account) {
        this.accounts.add(account);
    }

    public List<BelongsTo> getPlaylists_orders() {
        return playlists_orders;
    }

    public void addPlaylists_order(BelongsTo playlists_order) {
        this.playlists_orders.add(playlists_order);
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void addComment(Comment comment) {
        this.comments.add(comment);
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    public void setPlaylists_orders(List<BelongsTo> playlists_orders) {
        this.playlists_orders = playlists_orders;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }
}
