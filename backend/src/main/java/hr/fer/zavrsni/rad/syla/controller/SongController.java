package hr.fer.zavrsni.rad.syla.controller;

import hr.fer.zavrsni.rad.syla.dto.AccountDTO;
import hr.fer.zavrsni.rad.syla.dto.AlbumDTO;
import hr.fer.zavrsni.rad.syla.dto.GenreDTO;
import hr.fer.zavrsni.rad.syla.dto.SongDTO;
import hr.fer.zavrsni.rad.syla.service.SongService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping(path = "api/v1/song")
public class SongController {

    private final SongService songService;

    @Autowired
    public SongController(SongService songService) {
        this.songService = songService;
    }

    @GetMapping
    public List<SongDTO> getSongs() {
        return songService.getSongs();
    }

    @GetMapping(path = "/get")
    public SongDTO getSong(@RequestParam UUID song_ID) {
        return songService.getSong(song_ID);
    }
    
    @GetMapping(path = "/search")
    public List<SongDTO> search(@RequestParam String search) {
        return songService.search(search);
    }

    @PostMapping(path = "/update")
    public void updateSong(@RequestBody SongDTO song) {
        songService.updateSong(song);
    }

    @GetMapping(path = "/not-authors")
    public List<AccountDTO> getNotAuthors(@RequestParam UUID song_ID) {
        return songService.getNotAuthors(song_ID);
    }

    @GetMapping(path = "/audio")
    public Map<String, byte[]> downloadAudio(@RequestParam("audio") String name) {
        return songService.downloadAudio(name);
        /*Map<String, byte[]> data = songService.downloadAudio(name);
        byte[] file = data.get("data");

        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Disposition", "attachment; filename=" + name);
        ResponseEntity<byte[]> response = new ResponseEntity(file, headers, HttpStatus.OK);

        return response;*/
    }

    @RequestMapping
    public void uploadNewSong(@RequestParam(name = "audio") MultipartFile audio,
                              @RequestParam(name = "name") String name,
                              @RequestParam(name = "collaborations") String[] collaborations,
                              @RequestParam(name = "album_id") UUID album_id,
                              @RequestParam(name = "album_order") Integer album_order,
                              @RequestParam(name = "account_id") UUID[] account_ids,
                              @RequestParam(name = "genres") UUID[] genres) {
        songService.uploadNewSong(new SongDTO(name,
                                                List.of(collaborations),
                                                album_id,
                                                album_order,
                                                List.of(account_ids),
                                                List.of(genres)),
                                    audio);
    }
}
