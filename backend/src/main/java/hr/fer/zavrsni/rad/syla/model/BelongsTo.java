package hr.fer.zavrsni.rad.syla.model;

import javax.persistence.*;

@Entity
public class BelongsTo {

    @EmbeddedId
    private BelongsToKey id;

    @ManyToOne
    @MapsId("song_ID")
    @JoinColumn(name = "song_ID")
    private Song song;

    @ManyToOne
    @MapsId("playlist_ID")
    @JoinColumn(name = "playlist_ID")
    private Playlist playlist;

    @Column(name = "playlist_order")
    private Integer playlist_order;

    public BelongsTo() {}

    public BelongsTo(BelongsToKey id, Song song, Playlist playlist, int playlist_order) {
        this.id = id;
        this.song = song;
        this.playlist = playlist;
        this.playlist_order = playlist_order;
    }

    public BelongsToKey getId() {
        return id;
    }

    public Song getSong() {
        return song;
    }

    public void setSong(Song song) {
        this.song = song;
    }

    public Playlist getPlaylist() {
        return playlist;
    }

    public void setPlaylist(Playlist playlist) {
        this.playlist = playlist;
    }

    public Integer getPlaylist_order() {
        return playlist_order;
    }

    public void setPlaylist_order(int playlist_order) {
        this.playlist_order = playlist_order;
    }
}
