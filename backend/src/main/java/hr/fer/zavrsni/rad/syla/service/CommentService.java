package hr.fer.zavrsni.rad.syla.service;

import hr.fer.zavrsni.rad.syla.dao.AccountRepository;
import hr.fer.zavrsni.rad.syla.dao.CommentRepository;
import hr.fer.zavrsni.rad.syla.dao.SongRepository;
import hr.fer.zavrsni.rad.syla.dto.AccountDTO;
import hr.fer.zavrsni.rad.syla.dto.CommentDTO;
import hr.fer.zavrsni.rad.syla.dto.SongDTO;
import hr.fer.zavrsni.rad.syla.model.Account;
import hr.fer.zavrsni.rad.syla.model.Comment;
import hr.fer.zavrsni.rad.syla.model.Song;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CommentService {

    private final CommentRepository commentRepository;

    private final AccountRepository accountRepository;

    private final SongRepository songRepository;

    @Autowired
    public CommentService(CommentRepository commentRepository,
                          AccountRepository accountRepository,
                          SongRepository songRepository) {
        this.commentRepository = commentRepository;
        this.accountRepository = accountRepository;
        this.songRepository = songRepository;
    }

    public List<CommentDTO> getComments() {
        return commentRepository.findAll().stream()
                                        .map(c -> new CommentDTO(c.getComment_ID(),
                                                                c.getContent(),
                                                                c.getCreated(),
                                                                c.getLast_modified(),
                                                                new AccountDTO(c.getAccount().getAccount_ID(),
                                                                                c.getAccount().getPicture(),
                                                                                c.getAccount().getUsername(),
                                                                                c.getAccount().getStage_name(),
                                                                                c.getAccount().getDisplayName()),
                                                                new SongDTO(c.getSong().getSong_ID(),
                                                                            c.getSong().getName())))
                                        .collect(Collectors.toList());
    }

    public void newComment(CommentDTO comment) {
        Optional<Account> accountOptional = accountRepository.findById(comment.getAccount_id());

        if (!accountOptional.isPresent())
            throw new IllegalStateException("Account with ID '" + comment.getAccount_id() + "' does not exist!");

        Optional<Song> songOptional = songRepository.findById(comment.getSong_id());

        if (!songOptional.isPresent())
            throw new IllegalStateException("Song with ID '" + comment.getSong_id() + "' does not exist!");

        Account account = accountOptional.get();
        Song song = songOptional.get();

        Comment comment1 = new Comment(comment.getContent(),
                                        LocalDate.now(),
                                        account,
                                        song);

        account.addComment(comment1);
        song.addComment(comment1);

        commentRepository.save(comment1);
        accountRepository.save(account);
        songRepository.save(song);
    }

    public void deleteComment(UUID comment_id) {
        Optional<Comment> optionalComment = commentRepository.findById(comment_id);

        if (!optionalComment.isPresent()) {
            throw new IllegalStateException("Comment with ID '" + comment_id + "' does not exist!");
        }

        Comment comment = optionalComment.get();

        comment.getAccount().getComments().remove(comment);
        accountRepository.save(comment.getAccount());

        comment.getSong().getComments().remove(comment);
        songRepository.save(comment.getSong());

        commentRepository.deleteById(comment_id);
    }

    public void updateComment(CommentDTO comment) {
        Optional<Comment> commentOptional = commentRepository.findById(comment.getComment_ID());

        if (!commentOptional.isPresent())
            throw new IllegalStateException("Comment with ID '" + comment.getComment_ID() + "' does not exist!");

        Comment comment1 = commentOptional.get();

        comment1.setContent(comment.getContent());
        comment1.setLast_modified(LocalDate.now());

        commentRepository.save(comment1);
    }
}
