package hr.fer.zavrsni.rad.syla.dto;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public class AccountDTO {
    private UUID account_ID;
    private String username;
    private String stage_name;
    private String email;
    private String password;
    private String picture;
    private byte[] picture_data;
    private Boolean appearance;
    private LocalDate created;
    private String displayName;
    private Boolean admin;
    private Boolean artist;
    private List<AlbumDTO> albums;
    private Integer following;
    private Integer followers;

    public AccountDTO() {}

    public AccountDTO(String username,
                      String stage_name,
                      String email,
                      String password,
                      Boolean appearance) {
        this.username = username;
        this.stage_name = stage_name;
        this.email = email;
        this.password = password;
        this.appearance = appearance;
    }

    public AccountDTO(UUID account_ID,
                      String username,
                      String stage_name) {
        this.account_ID = account_ID;
        this.username = username;
        this.stage_name = stage_name;
    }

    public AccountDTO(UUID account_id,
                      String username,
                      String stage_name,
                      String email,
                      String picture,
                      Boolean appearance,
                      LocalDate created) {
        account_ID = account_id;
        this.username = username;
        this.stage_name = stage_name;
        this.email = email;
        this.picture = picture;
        this.appearance = appearance;
        this.created = created;
    }

    public AccountDTO(UUID account_id,
                      String username,
                      String stage_name,
                      String email,
                      String picture,
                      Boolean appearance,
                      LocalDate created,
                      Boolean admin,
                      Boolean artist) {
        account_ID = account_id;
        this.username = username;
        this.stage_name = stage_name;
        this.email = email;
        this.picture = picture;
        this.appearance = appearance;
        this.created = created;
        this.admin = admin;
        this.artist = artist;
        this.displayName = appearance ? stage_name : username;
    }

    public AccountDTO(UUID account_id,
                      String username,
                      String stage_name,
                      String email,
                      String picture,
                      Boolean appearance,
                      LocalDate created,
                      Boolean admin,
                      Boolean artist,
                      Integer following,
                      Integer followers) {
        account_ID = account_id;
        this.username = username;
        this.stage_name = stage_name;
        this.email = email;
        this.picture = picture;
        this.appearance = appearance;
        this.created = created;
        this.admin = admin;
        this.artist = artist;
        this.displayName = appearance ? stage_name : username;
        this.following = following;
        this.followers = followers;
    }

    public AccountDTO(UUID account_id,
                      String username,
                      String stage_name,
                      String email,
                      byte[] picture_data,
                      Boolean appearance,
                      LocalDate created,
                      Boolean admin,
                      Boolean artist) {
        account_ID = account_id;
        this.username = username;
        this.stage_name = stage_name;
        this.email = email;
        this.picture_data = picture_data;
        this.appearance = appearance;
        this.created = created;
        this.admin = admin;
        this.artist = artist;
        this.displayName = appearance ? stage_name : username;
    }

    public AccountDTO(UUID account_ID,
                      String username,
                      String stage_name,
                      String picture,
                      Boolean appearance) {
        this.account_ID = account_ID;
        this.username = username;
        this.stage_name = stage_name;
        this.picture = picture;
        this.appearance = appearance;
        this.displayName = appearance ? stage_name : username;
    }

    public AccountDTO(UUID account_ID,
                      String username,
                      String stage_name,
                      byte[] picture_data,
                      Boolean appearance) {
        this.account_ID = account_ID;
        this.username = username;
        this.stage_name = stage_name;
        this.picture_data = picture_data;
        this.appearance = appearance;
        this.displayName = appearance ? stage_name : username;
    }

    public AccountDTO(UUID account_ID,
                      String picture,
                      String username,
                      String stage_name,
                      String displayName) {
        this.account_ID = account_ID;
        this.picture = picture;
        this.username = username;
        this.stage_name = stage_name;
        this.displayName = displayName;
    }

    public AccountDTO(UUID account_ID,
                      String picture,
                      String username,
                      String stage_name,
                      String displayName,
                      List<AlbumDTO> albums) {
        this.account_ID = account_ID;
        this.picture = picture;
        this.username = username;
        this.stage_name = stage_name;
        this.displayName = displayName;
        this.albums = albums;
    }

    public AccountDTO(UUID account_ID,
                      String username,
                      String picture,
                      String displayName) {
        this.account_ID = account_ID;
        this.username = username;
        this.picture = picture;
        this.displayName = displayName;
    }

    public UUID getAccount_ID() {
        return account_ID;
    }

    public void setAccount_ID(UUID account_ID) {
        this.account_ID = account_ID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getStage_name() {
        return stage_name;
    }

    public void setStage_name(String stage_name) {
        this.stage_name = stage_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Boolean getAppearance() {
        return appearance;
    }

    public void setAppearance(Boolean appearance) {
        this.appearance = appearance;
    }

    public LocalDate getCreated() {
        return created;
    }

    public void setCreated(LocalDate created) {
        this.created = created;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    public Boolean getArtist() {
        return artist;
    }

    public void setArtist(Boolean artist) {
        this.artist = artist;
    }

    public String getDisplayName() {
        return displayName;
    }

    public byte[] getPicture_data() {
        return picture_data;
    }

    public void setPicture_data(byte[] picture_data) {
        this.picture_data = picture_data;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<AlbumDTO> getAlbums() {
        return albums;
    }

    public void setAlbums(List<AlbumDTO> albums) {
        this.albums = albums;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Integer getFollowing() {
        return following;
    }

    public void setFollowing(Integer following) {
        this.following = following;
    }

    public Integer getFollowers() {
        return followers;
    }

    public void setFollowers(Integer followers) {
        this.followers = followers;
    }
}
