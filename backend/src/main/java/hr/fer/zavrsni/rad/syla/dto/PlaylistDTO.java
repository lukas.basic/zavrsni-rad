package hr.fer.zavrsni.rad.syla.dto;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public class PlaylistDTO {
    private UUID playlist_ID;
    private String name;
    private Boolean modifiable;
    private Boolean public_access;
    private LocalDate created;
    private LocalDate last_modified;
    private UUID account_ID;
    private AccountDTO account;
    private List<SongDTO> songs;

    public PlaylistDTO() {}

    public PlaylistDTO(UUID playlist_ID, String name, Boolean public_access) {
        this.playlist_ID = playlist_ID;
        this.name = name;
        this.public_access = public_access;
    }

    public PlaylistDTO(String name, Boolean public_access, UUID account_ID) {
        this.account_ID = account_ID;
        this.name = name;
        this.public_access = public_access;
    }

    public PlaylistDTO(UUID playlist_ID, String name) {
        this.playlist_ID = playlist_ID;
        this.name = name;
    }

    public PlaylistDTO(String name, AccountDTO account) {
        this.account = account;
        this.name = name;
    }

    public PlaylistDTO(String name, UUID account_ID) {
        this.account_ID = account_ID;
        this.name = name;
    }

    public PlaylistDTO(UUID playlist_ID,
                       String name,
                       Boolean modifiable,
                       Boolean public_access,
                       LocalDate created,
                       LocalDate last_modified,
                       AccountDTO account) {
        this.playlist_ID = playlist_ID;
        this.name = name;
        this.modifiable = modifiable;
        this.public_access = public_access;
        this.created = created;
        this.last_modified = last_modified;
        this.account = account;
    }

    public PlaylistDTO(UUID playlist_ID,
                       String name,
                       Boolean modifiable,
                       Boolean public_access,
                       LocalDate created,
                       LocalDate last_modified,
                       AccountDTO account,
                       List<SongDTO> songs) {
        this.playlist_ID = playlist_ID;
        this.name = name;
        this.modifiable = modifiable;
        this.public_access = public_access;
        this.created = created;
        this.last_modified = last_modified;
        this.account = account;
        this.songs = songs;
    }

    public PlaylistDTO(UUID playlist_ID,
                       String name,
                       Boolean modifiable,
                       Boolean public_access,
                       LocalDate created,
                       LocalDate last_modified,
                       UUID account_ID) {
        this.playlist_ID = playlist_ID;
        this.name = name;
        this.modifiable = modifiable;
        this.public_access = public_access;
        this.created = created;
        this.last_modified = last_modified;
        this.account_ID = account_ID;
    }

    public UUID getPlaylist_ID() {
        return playlist_ID;
    }

    public void setPlaylist_ID(UUID playlist_ID) {
        this.playlist_ID = playlist_ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getModifiable() {
        return modifiable;
    }

    public void setModifiable(Boolean modifiable) {
        this.modifiable = modifiable;
    }

    public LocalDate getCreated() {
        return created;
    }

    public void setCreated(LocalDate created) {
        this.created = created;
    }

    public LocalDate getLast_modified() {
        return last_modified;
    }

    public void setLast_modified(LocalDate last_modified) {
        this.last_modified = last_modified;
    }

    public UUID getAccount_ID() {
        return account_ID;
    }

    public void setAccount_ID(UUID account_ID) {
        this.account_ID = account_ID;
    }

    public AccountDTO getAccount() {
        return account;
    }

    public void setAccount(AccountDTO account) {
        this.account = account;
    }

    public Boolean getPublic_access() {
        return public_access;
    }

    public void setPublic_access(Boolean public_access) {
        this.public_access = public_access;
    }

    public List<SongDTO> getSongs() {
        return songs;
    }

    public void setSongs(List<SongDTO> songs) {
        this.songs = songs;
    }
}
