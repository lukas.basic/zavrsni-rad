const accountsHeader = [
    "Username", 
    "Stage name", 
    "E-mail", 
    "Picture", 
    "Appearance", 
    "Created", 
    "Admin", 
    "Artist"
];

const albumsHeader = [
    "Name",
    "Author/s",
    "Picture", 
    "Released"
];

const songsHeader = [
    "Name", 
    "Features", 
    "Author/s", 
    "Album",
    "Album order",
    "Audio", 
    "Released", 
    "Genre"
];

const playlistsHeader = [
    "Name", 
    "Account",
    "Default", 
    "Public",
    "Created", 
    "Last modified"
];

const genresHeader = [
    "Genre"
];

const commentsHeader = [
    "Author",
    "Song",
    "Created",
    "Last modified"
];

export { 
    accountsHeader,
    albumsHeader,
    songsHeader,
    playlistsHeader,
    genresHeader,
    commentsHeader
};