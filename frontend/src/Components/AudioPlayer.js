import React, { useState, useEffect } from "react";
import {
  Link
} from "react-router-dom";
import AccountsComma from "./AccountsComma";

import "../Styles/AudioPlayer.css";

function AudioPlayer(props) {

    const [currentIndex, setCurrentIndex] = useState(props.trackList ? props.trackList.index : null);
    const [list, setList] = useState(props.trackList ? props.trackList.list : []);
    const [source, setSource] = useState(props.trackList ? props.trackList.source : null);
    
    const [loader, setLoader] = useState(true);
    const [audioBytes, setAudioBytes] = useState("");
    const [image, setImage] = useState("");
    
    let getAudioBytes = (index) => {
        setLoader(true);    
        setImage("");    
        fetch(`http://localhost:8080/api/v1/song/audio?audio=` + props.trackList.list[index].audio)
            .then(res => res.json())
            .then(data => setAudioBytes("data:audio/mpeg;base64," + data.data))
            .then(() => {
                fetch("http://localhost:8080/api/v1/album/image?image_name=" + props.trackList.list[index].album.picture)
                    .then(response => response.json())
                    .then(data => setImage("data:image/png;base64," + data.data))
                    .then(() => setLoader(false));
            })
            .catch(e => console.log('ERROR DOWNLOADING AUDIO FILE'));

        setCurrentIndex(index);
        setList(props.trackList.list);
        setSource(props.trackList.source);
    }
    
    useEffect(() => {
        if (props.trackList) {
            getAudioBytes(props.trackList.index);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.trackList]);

    let next = () => {
        if (currentIndex === list.length - 1)
            getAudioBytes(0);
        else
            getAudioBytes(currentIndex + 1);
    }

    let prev = () => {
        if (currentIndex === 0)
            getAudioBytes(list.length - 1);
        else
            getAudioBytes(currentIndex - 1);
    }

    if (!props.trackList)
        return (
            <div className="audio-player">
                <span>pick a song to play</span>
            </div>
        );
    else if (currentIndex === null || list.size === 0)
            return (
                <div className="audio-player">
                    <span>Loading...</span>
                </div>
            );
    else
        return (
            <div className="audio-player">
                <div className="audio-image-info">
                    {image !== "" &&
                        <div className="img">
                            <img
                                className="artwork-song"
                                src={image}
                                alt={`track artwork`}
                            />
                        </div>
                    }
                    <div className="audio-info">
                        <div style={{"font-weight": "bold"}}>
                            {list[currentIndex].name.length < 20 &&
                                <span>{list[currentIndex].name}</span>
                            }
                            {list[currentIndex].name.length > 20 &&
                                <span>{list[currentIndex].name.substring(0, 17)}...</span>
                            }
                            {list[currentIndex].collaborationsString && 
                            list[currentIndex].name.length + list[currentIndex].collaborationsString.length < 20 &&
                                <span> (ft. {list[currentIndex].collaborationsString})</span>
                            }
                            <span> - </span>
                            <Link to={"/albums/" + list[currentIndex].album.album_ID} className="nav-link"> 
                                {list[currentIndex].album.name.length < 10 &&
                                    <span className="artist">{list[currentIndex].album.name}</span>
                                }
                                {list[currentIndex].album.name.length > 10 &&
                                    <span className="artist">{list[currentIndex].album.name.substring(0, 7)}...</span>
                                }
                                
                            </Link>
                        </div>
                        <div className="song-artists">
                            <AccountsComma accounts={list[currentIndex].accounts} />
                        </div>
                        <span style={{"font-size": "small"}}>Playing from {source.type} {source.name}</span>
                    </div>
                </div>
                
                {loader && 
                    <span>Loading audio...</span>
                }
                {!loader &&
                    <div className="audio">
                        <div className="audio-controls">
                            <span onClick={prev}><i className="fas fa-backward"></i></span>
                            <span onClick={next}><i className="fas fa-forward"></i></span>
                        </div>
                        <audio controls>
                            <source src={audioBytes} type="audio/mpeg"></source>
                        </audio>
                    </div>
                }
                
            </div>
        );
};

export default AudioPlayer;