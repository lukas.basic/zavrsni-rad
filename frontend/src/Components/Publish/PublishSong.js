import React, { Component } from 'react';
import Autosuggest from 'react-autosuggest';

class PublishSong extends Component {
    constructor(props) {
        super(props);

        this.state = this.initialState;
    }

    initialState = {
        name: this.props.name,
        audio: null,
        authors: [this.props.author],
        collaborations: [],
        genres: [],
        newColl: false,
        newGenre: false,
        allGenres: [],
        genreList: [],
        notAuthors: [],
        newAuthor: false,
        newAuthorObj: null,
        value: "",
        suggestions: [],
        placeholder: "Enter username or stage name",
        onChange: (event, { newValue }) => this.setState({value: newValue})
    }

    setName = data => {
        this.props.setName(data, this.props.index);
        this.setState({name: data});
    }

    setAudio = event => { 
        this.props.setAudio(event, this.props.index);
        this.setState({audio: event.target.files[0]});
    }; 

    setAuthors = data => {
        this.props.setAccounts(data, this.props.index);
        this.setState({authors: data});
    }

    setNotAuthors = data => {
        this.setState({notAuthors: data.filter(a => a.account_ID !== this.props.author.account_ID)});
    }

    setNewAuthor = data => {
        this.setState({newAuthor: data});
    }

    setNewAuthorObj = data => {
        this.setState({newAuthorObj: data});
    }

    setValue = data => {
        console.log(this.state.value)
        this.setState({value: data});
    }

    setSuggestions = data => {
        this.setState({suggestions: data});
    }

    componentDidMount = () => {
        fetch('http://localhost:8080/api/v1/account')
            .then(response => response.json())
            .then(data => this.setNotAuthors(data));

        fetch('http://localhost:8080/api/v1/genre')
            .then(response => response.json())
            .then(data => this.setAllGenres(data));
    };

    addAuthor = () => {
        let newNotAuth = this.state.notAuthors.filter(a => a.account_ID !== this.state.newAuthorObj.account_ID);
        this.state.authors.push(this.state.newAuthorObj);
        this.setNotAuthors(newNotAuth);
        this.setAuthors(this.state.authors);
        this.setNewAuthor(false);
        this.setNewAuthorObj(null);
        this.setValue('');
    }

    removeAuthor = a => {
        let newAuth = this.state.authors.filter(auth => auth.account_ID !== a.account_ID);
        this.state.notAuthors.push(a);
        this.setNotAuthors(this.state.notAuthors);
        this.setAuthors(newAuth);
    }

    addCollaboration = () => {
        let arr = this.state.collaborations;
        arr.push(this.state.newColl);
        this.setState({collaborations: arr, newColl: false});
        this.props.setCollaborations(arr, this.props.index);
    }

    removeCollaboration = c => {
        let newColl = [];
        this.state.collaborations.map(coll => {
            if (coll !== c)
                newColl.push(coll);
            return <></>;
        })
        this.setState({collaborations: newColl});
        this.props.setCollaborations(newColl, this.props.index);
    }

    setNewColl = data => {
        this.setState({newColl: data});
    }

    setAllGenres = data => {
        this.setState({allGenres: data});
    }

    updateGenreList = () => {
        let tmp = [];

        this.state.allGenres.map(g => {
            let contains = false;

            this.state.genres.map(genre => {
                if (g.name === genre.name)
                    contains = true;
                return <></>;
            });

            if (!contains)
                tmp.push(g)
            
            return <></>;
        }, this.setState({genreList: tmp}));
    }

    addGenre = () => {
        let arr = this.state.genres;
        let g = this.state.newGenre;

        if (g === undefined)
            g = this.state.genreList[0];

        arr.push(g);
        
        this.setState({genres: arr, newGenre: false});
        this.props.setGenres(arr, this.props.index);

        this.updateGenreList();
    }

    removeGenre = g => {
        let gArr = [];
        this.state.genres.map(genre => {
            if (genre !== g)
                gArr.push(genre);
            return <></>;
        })
        
        this.setState({genres: gArr});
        this.props.setGenres(gArr, this.props.index);

        this.updateGenreList();
    }

    setDefaultGenre = g => {
        if (g === undefined) {
            let init = false;
            this.state.allGenres.map(genre => {
                if (!init) {
                    this.state.genres.map(gg => {
                        if (!init && gg.name !== genre.name) {
                            g = genre;
                            init = true;
                            return <></>;
                        }
                        return <></>;
                    })
                }
                return <></>;
            })
        }
        
        this.setState({newGenre: g});
    }

    setNewGenre = data => {
        if (data === false || data === true) {
            this.setState({newGenre: data});
            return;
        }


        let genre;
        
        this.state.genreList.map(g => {
            if (g.name === data)
                genre = g;

            return null;
        })

        this.setState({newGenre: genre});
    }

    // author autosuggest helpers start
    escapeRegexCharacters = (str) => {
        return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
    }
    
    getSuggestions = (value) => {
        const escapedValue = this.escapeRegexCharacters(value.trim());
        
        if (escapedValue === '') {
            return [];
        }
        
        const regex = new RegExp('^' + escapedValue, 'i');
        
        return this.state.notAuthors.filter(author => regex.test(author.username) || regex.test(author.stage_name));
    }
    
    getSuggestionValue = (suggestion) => {
        this.setNewAuthorObj(suggestion);
        return suggestion.username + ' (' + suggestion.stage_name + ')';
    }
    
    renderSuggestion = (suggestion) => {
        return (
            <span style={{"cursor": "pointer"}}>{suggestion.username} ({suggestion.stage_name})</span>
        );
    }

    onChange = (event, { newValue, method }) => {
        this.setValue(newValue);
    };
      
    onSuggestionsFetchRequested = ({ value }) => {
        this.setSuggestions(this.getSuggestions(value));
    };

    onSuggestionsClearRequested = () => {
        this.setSuggestions([]);
    };
    // author autosuggest helpers end

    audioData = () => { 
        if (this.state.audio) { 
            
          return ( 
            <div> 
              <p>Audio Details:</p> 
              <p>File Name: {this.state.audio.name}</p> 
              <p>File Type: {this.state.audio.type}</p> 
            </div> 
          ); 
        } else { 
          return ( 
            <div> 
              <br /> 
              <span>Choose before Pressing the Publish button</span> 
            </div> 
          ); 
        } 
      }; 

    render() {
        return (
            <>
            <form autocomplete="off" className="form-modal">
                <div className="form-row">
                    <span>Name:</span>
                    <input type="text" value={this.state.name} 
                            onChange={e => this.setName(e.target.value)} />
                </div>
                <div className="form-row">
                    <span>Audio:</span>
                    <input type="file" onChange={this.setAudio} accept=".mp3"/>
                </div>
                {this.audioData()} 

                <div className="line"></div>
                <div>
                    <span>Authors</span>
                </div>
                
                {this.state.authors.map(a => {
                    return <div className="form-row">
                                {this.props.author.account_ID === a.account_ID &&
                                    <>
                                    <span>You</span>
                                    <input type="button" className="button" value="Remove" onClick={() => this.removeAuthor(a)} disabled/>
                                    </>
                                }
                                {this.props.author.account_ID !== a.account_ID &&
                                    <>
                                    <span>{a.username} ({a.stage_name})</span>
                                    <input type="button" className="button" value="Remove" onClick={() => this.removeAuthor(a)} />
                                    </>
                                }
                            </div>
                })}
                {!this.state.newAuthor &&
                    <div className="form-row">
                        <input type="button" className="button" value="Add author" onClick={() => this.setNewAuthor(true)} />
                    </div>
                }
                {this.state.newAuthor &&
                    <div className="form-row">
                        <Autosuggest 
                            suggestions={this.state.suggestions}
                            onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                            onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                            getSuggestionValue={this.getSuggestionValue}
                            renderSuggestion={this.renderSuggestion}
                            inputProps={this.state} />
                        {this.state.newAuthorObj === null &&
                            <input type="button" className="button" value="Confirm" onClick={this.addAuthor} disabled/>
                        }
                        {this.state.newAuthorObj !== null &&
                            <input type="button" className="button" value="Confirm" onClick={this.addAuthor} />
                        }
                        <input type="button" className="button" value="Cancel" onClick={() => {this.setNewAuthor(false); this.setValue('');}} /> 
                    </div>
                }

                <div className="line"></div>
                <div>
                    <span>Collaborations</span>
                </div>
                {this.state.collaborations.length === 0 && this.state.newColl === false &&
                    <div className="form-row">
                        <input type="button" className="button" value="Add" onClick={() => this.setNewColl("")} />
                    </div>
                }
                {this.state.collaborations.length === 0 && this.state.newColl !== false &&
                    <div className="form-row">
                        <input type="text" value={this.state.newColl} onChange={e => this.setNewColl(e.target.value)} />
                        <input type="button" className="button" value="Confirm" onClick={this.addCollaboration} /> 
                        <input type="button" className="button" value="Cancel" onClick={() => this.setNewColl(false)} /> 
                    </div>
                }
                {this.state.collaborations.length > 0 &&
                    <>
                        {this.state.collaborations.map(c => {
                            return <div className="form-row">
                                        <span>{c}</span>
                                        <input type="button" className="button" value="Remove" onClick={() => this.removeCollaboration(c)} />
                                    </div>
                        })}
                        {this.state.newColl === false &&
                            <div className="form-row">
                                <input type="button" className="button" value="Add collaboration" onClick={() => this.setNewColl("")} />
                            </div>
                        }
                        {this.state.newColl !== false &&
                            <div className="form-row">
                                <input type="text" value={this.state.newColl} onChange={e => this.setNewColl(e.target.value)} />
                                <input type="button" className="button" value="Confirm" onClick={this.addCollaboration} /> 
                                <input type="button" className="button" value="Cancel" onClick={() => this.setNewColl(false)} /> 
                            </div>
                        }
                    </>
                }
                <div className="line"></div>
                <div>
                    <span>Genres</span>
                </div>
                {this.state.genres.length === 0 && this.state.newGenre === false &&
                    <div className="form-row">
                        <input type="button" className="button" value="Add" onClick={() => {this.updateGenreList(); this.setDefaultGenre(this.state.genreList[0]);}} />
                    </div>
                }
                {this.state.genres.length === 0 && this.state.newGenre !== false &&
                    <div className="form-row">
                        <select defaultValue={this.state.genreList[0].name} onChange={e => this.setNewGenre(e.target.value)}>
                            {this.state.genreList.map(g => {
                                return <option value={g.name}>{g.name}</option>
                            })}
                        </select>
                        <input type="button" className="button" value="Confirm" onClick={this.addGenre} /> 
                        <input type="button" className="button" value="Cancel" onClick={() => this.setNewGenre(false)} /> 
                    </div>
                }
                {this.state.genres.length > 0 &&
                    <>
                        {this.state.genres.map(c => {
                            return <div className="form-row">
                                        <span>{c.name}</span>
                                        <input type="button" className="button" value="Remove" onClick={() => this.removeGenre(c)} />
                                    </div>
                        })}
                        {this.state.newGenre === false &&
                            <div className="form-row">
                                <input type="button" className="button" value="Add genre" onClick={() => {this.updateGenreList(); this.setDefaultGenre(this.state.genreList[0]);}} />
                            </div>
                        }
                        {this.state.newGenre !== false &&
                            <div className="form-row">
                                <select defaultValue={this.state.genreList[0].name} onChange={e => this.setNewGenre(e.target.value)}>
                                    {this.state.genreList.map(g => {
                                        return <option value={g.name}>{g.name}</option>
                                    })}
                                </select>
                                <input type="button" className="button" value="Confirm" onClick={this.addGenre} /> 
                                <input type="button" className="button" value="Cancel" onClick={() => this.setNewGenre(false)} /> 
                            </div>
                        }
                    </>
                }
            </form>
        </>
        );
    }
}

export default PublishSong;