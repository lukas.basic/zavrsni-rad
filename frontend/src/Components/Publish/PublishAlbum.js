import React, { Component } from 'react';
import ImageUploader from "react-images-upload";
import Autosuggest from 'react-autosuggest';

class PublishAlbum extends Component {
    constructor(props) {
        super(props);

        this.state = this.initialState;
    }

    initialState = {
        name: this.props.name,
        authors: [this.props.author],
        notAuthors: [],
        newAuthor: false,
        newAuthorObj: null,
        newPicture: false,
        imageData: [],
        value: "",
        suggestions: [],
        placeholder: "Enter username or stage name",
        onChange: (event, { newValue }) => this.setState({value: newValue})
    }

    setName = data => {
        this.props.setName(data);
        this.setState({name: data});
    }

    setAuthors = data => {
        this.props.setAccounts(data);
        this.setState({authors: data});
    }

    setNotAuthors = data => {
        this.setState({notAuthors: data.filter(a => a.account_ID !== this.props.author.account_ID)});
    }

    setNewAuthor = data => {
        this.setState({newAuthor: data});
    }

    setNewAuthorObj = data => {
        this.setState({newAuthorObj: data});
    }

    setNewPicture = data => {
        this.setState({newPicture: data});
    }

    setImageData = data => {
        this.props.setImageData(data);
        this.setState({imageData: data});
    }

    setValue = data => {
        console.log(this.state.value)
        this.setState({value: data});
    }

    setSuggestions = data => {
        this.setState({suggestions: data});
    }

    componentDidMount = () => {
        fetch('http://localhost:8080/api/v1/account')
            .then(response => response.json())
            .then(data => this.setNotAuthors(data));
    };

    addAuthor = () => {
        let newNotAuth = this.state.notAuthors.filter(a => a.account_ID !== this.state.newAuthorObj.account_ID);
        this.state.authors.push(this.state.newAuthorObj);
        this.setNotAuthors(newNotAuth);
        this.setAuthors(this.state.authors);
        this.setNewAuthor(false);
        this.setNewAuthorObj(null);
        this.setValue('');
    }

    removeAuthor = a => {
        let newAuth = this.state.authors.filter(auth => auth.account_ID !== a.account_ID);
        this.state.notAuthors.push(a);
        this.setNotAuthors(this.state.notAuthors);
        this.setAuthors(newAuth);
    }

    onDrop = (picture) => {
        this.setNewPicture(true);
        let file = picture[0];
        const imageData = new FormData();
        imageData.append('image', file);
        this.setImageData(imageData);
    } 

    // author autosuggest helpers start
    escapeRegexCharacters = (str) => {
        return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
    }
    
    getSuggestions = (value) => {
        const escapedValue = this.escapeRegexCharacters(value.trim());
        
        if (escapedValue === '') {
            return [];
        }
        
        const regex = new RegExp('^' + escapedValue, 'i');
        
        return this.state.notAuthors.filter(author => regex.test(author.username) || regex.test(author.stage_name));
    }
    
    getSuggestionValue = (suggestion) => {
        this.setNewAuthorObj(suggestion);
        return suggestion.username + ' (' + suggestion.stage_name + ')';
    }
    
    renderSuggestion = (suggestion) => {
        return (
            <span style={{"cursor": "pointer"}}>{suggestion.username} ({suggestion.stage_name})</span>
        );
    }

    onChange = (event, { newValue, method }) => {
        this.setValue(newValue);
    };
      
    onSuggestionsFetchRequested = ({ value }) => {
        this.setSuggestions(this.getSuggestions(value));
    };

    onSuggestionsClearRequested = () => {
        this.setSuggestions([]);
    };
    // author autosuggest helpers end

    render() {
        return (
            <div>
                <div className="line">Album</div>
                <form autocomplete="off" className="form-modal">
                    <div className="form-row">
                        <ImageUploader
                            singleImage={true}
                            withIcon={true}
                            withLabel={false}
                            withPreview={true}
                            buttonText="Choose album picture"
                            onChange={this.onDrop}
                            imgExtension={[".jpg", ".png", ".jpeg"]}
                            maxFileSize={5242880}
                        />
                    </div>

                    <div className="form-row">
                        <span>Name:</span>
                        <input type="text" value={this.state.name} 
                                onChange={e => this.setName(e.target.value)} />
                    </div>
                    <div className="line"></div>
                    <div>
                        <span>Authors</span>
                    </div>
                    {this.state.authors.map(a => {
                        return <div className="form-row">
                                    {this.props.author.account_ID === a.account_ID &&
                                        <>
                                        <span>You</span>
                                        <input type="button" className="button" value="Remove" onClick={() => this.removeAuthor(a)} disabled/>
                                        </>
                                    }
                                    {this.props.author.account_ID !== a.account_ID &&
                                        <>
                                        <span>{a.username} ({a.stage_name})</span>
                                        <input type="button" className="button" value="Remove" onClick={() => this.removeAuthor(a)} />
                                        </>
                                    }
                                </div>
                    })}
                    {!this.state.newAuthor &&
                        <div className="form-row">
                            <input type="button" className="button" value="Add author" onClick={() => this.setNewAuthor(true)} />
                        </div>
                    }
                    {this.state.newAuthor &&
                        <div className="form-row">
                            <Autosuggest 
                                suggestions={this.state.suggestions}
                                onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                                onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                                getSuggestionValue={this.getSuggestionValue}
                                renderSuggestion={this.renderSuggestion}
                                inputProps={this.state} />
                            {this.state.newAuthorObj === null &&
                                <input type="button" className="button" value="Confirm" onClick={this.addAuthor} disabled/>
                            }
                            {this.state.newAuthorObj !== null &&
                                <input type="button" className="button" value="Confirm" onClick={this.addAuthor} />
                            }
                            <input type="button" className="button" value="Cancel" onClick={() => {this.setNewAuthor(false); this.setValue('');}} /> 
                        </div>
                    }
                </form>
                <div className="line"></div>
            </div>
        );
    }
}

export default PublishAlbum;