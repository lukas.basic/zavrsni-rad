import React, { Component } from 'react';
import {connect} from 'react-redux';
import {Row, Col, Card, Form, InputGroup, FormControl, Button} from 'react-bootstrap';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faSignInAlt, faEnvelope, faLock, faUndo} from "@fortawesome/free-solid-svg-icons";
import {authenticateUser} from '../Services/index.js';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = this.initialState;
    }

    initialState = {
        email:'', password:'', error:''
    };

    credentialChange = event => {
        this.setState({
            [event.target.name] : event.target.value
        });
    };

    validateUser = () => {
        this.props.authenticateUser(this.state.email, this.state.password);
        setTimeout(() => {
            if(this.props.auth.isLoggedIn) {
                this.resetLoginForm();
            } else {
                this.resetLoginForm();
                this.setState({"error":"Invalid email and password"});
            }
        }, 1000);
    };

    resetLoginForm = () => {
        this.setState(() => this.initialState);
    };
    
    handleKeypress = e => { 
        if (this.state.email.length !== 0 && this.state.password.length !== 0 && e.charCode === 13) {      
            this.validateUser();    
        }  
    };

    render() {
        const {email, password, error} = this.state;

        return (
            <div >
            <Row className="justify-content-md-center">
                <Col xs={5}>
                    {error && 
                        <div style={{"color":"red", "margin-bottom": "10px", "font-size": "medium"}}>
                            <span>{error}</span>
                        </div>
                    }
                    <Card className={"border border-dark bg-dark text-white"}>
                        <Card.Header style={{"margin-bottom":"10px"}}>
                            <FontAwesomeIcon icon={faSignInAlt}/> Login
                        </Card.Header>
                        <Card.Body>
                            <Form.Row>
                                <Form.Group as={Col}>
                                    <InputGroup style={{"display":"flex", "flex-direction":"row", "justify-content": "space-between"}}>
                                        <InputGroup.Prepend style={{"margin-right":"10px"}}>
                                            <InputGroup.Text><FontAwesomeIcon icon={faEnvelope}/></InputGroup.Text>
                                        </InputGroup.Prepend>
                                        <FormControl required autoComplete="off" type="text" name="email" value={email} onChange={this.credentialChange}
                                            onKeyPress={this.handleKeypress} className={"bg-dark text-white"} placeholder="Enter Email Address"/>
                                    </InputGroup>
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col}>
                                    <InputGroup style={{"display":"flex", "flex-direction":"row", "justify-content": "space-between"}}>
                                        <InputGroup.Prepend style={{"margin-right":"10px"}}>
                                            <InputGroup.Text><FontAwesomeIcon icon={faLock}/></InputGroup.Text>
                                        </InputGroup.Prepend>
                                        <FormControl required autoComplete="off" type="password" name="password" value={password} onChange={this.credentialChange}
                                            onKeyPress={this.handleKeypress} className={"bg-dark text-white"} placeholder="Enter Password"/>
                                    </InputGroup>
                                </Form.Group>
                            </Form.Row>
                        </Card.Body>
                        <Card.Footer style={{"text-align":"right"}}>
                            <Button size="sm" className="button" type="button" variant="success" onClick={this.validateUser}
                                disabled={this.state.email.length === 0 || this.state.password.length === 0}>
                                <FontAwesomeIcon icon={faSignInAlt}/> Login
                            </Button>{' '}
                            <Button size="sm" className="button" type="button" variant="info" onClick={this.resetLoginForm}
                                disabled={this.state.email.length === 0 && this.state.password.length === 0 && this.state.error.length === 0}>
                                <FontAwesomeIcon icon={faUndo}/> Reset
                            </Button>
                        </Card.Footer>
                    </Card>
                </Col>
            </Row>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        auth:state.auth
    }
};

const mapDispatchToProps = dispatch => {
    return {
        authenticateUser: (email, password) => dispatch(authenticateUser(email, password))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);