import React from 'react';

import '../Styles/Modal.css';

function ImageModal(props) {
    return (
        <>
            <div className="form-row picture-row">
                <img className="picture"
                    src={props.picture}
                    alt="album_pic"
                />
            </div>
            <div className="form-buttons">
                <input type="button" className="button" value="Close" onClick={() => props.setShowModal(false)} />
            </div>
        </>
    );
}

export default ImageModal;