import React from 'react';
import AlbumCard from '../Components/AlbumCard';

function AlbumRow({ ind, albums }) {
    return (
        <div key={ind} className="album-card-row">
            {albums.map((album, index) => {
                if (index >= ind*4 && index < ind*4 + 4)
                    return <AlbumCard key={index} album={album} />;
                else
                    return <></>; 
            })}
            {Math.floor(albums.length/4) === ind && albums.length%4 === 1 && 
                <>
                    <div className="album-card-empty" />
                    <div className="album-card-empty" />
                    <div className="album-card-empty" />
                </>
            }
            {Math.floor(albums.length/4) === ind && albums.length%4 === 2 && 
                <>
                    <div className="album-card-empty" />
                    <div className="album-card-empty" />
                </>
            }
            {Math.floor(albums.length/4) === ind && albums.length%4 === 3 && 
                <>
                    <div className="album-card-empty" />
                </>
            }
        </div>
    );
}

export default AlbumRow;