import React, { useState } from 'react';

import { 
    accountsHeader,
    albumsHeader,
    songsHeader,
    playlistsHeader,
    genresHeader,
    commentsHeader
} from '../utils/TableHeaders';

import '../Styles/Navigation.css';

function NavigationAdmin(props) {
    const [isOpen, setIsOpen] = useState(false);

    return (
        <div className="nav-bar" onMouseEnter={() => setIsOpen(true)}
                                onMouseLeave={() => setIsOpen(false)}>
            <div className="nav-item nav-bar-accounts"
                    onClick={() => {
                                props.setData("Account");
                                props.setHeader(accountsHeader);
                                props.setFilter({ value: "", sort: "", sortDirection: undefined })
                            }}>
                <span><i className="fas fa-user-circle"></i></span>

                {isOpen && 
                    <span className="nav-description">Accounts</span>
                }
            </div>
            <div className="nav-item nav-bar-albums"
                    onClick={() => {
                        props.setData("Album");
                        props.setHeader(albumsHeader);
                        props.setFilter({ value: "", sort: "", sortDirection: undefined })
                    }}>
                <span><i className="fas fa-record-vinyl"></i></span>

                {isOpen && 
                    <span className="nav-description">Albums</span>
                }
            </div>
            <div className="nav-item nav-bar-songs"
                    onClick={() => {
                        props.setData("Song");
                        props.setHeader(songsHeader);
                        props.setFilter({ value: "", sort: "", sortDirection: undefined })
                    }}>
                <span><i className="fas fa-music"></i></span>

                {isOpen && 
                    <span className="nav-description">Songs</span>
                }
            </div>
            <div className="nav-item nav-bar-playlists"
                    onClick={() => {
                        props.setData("Playlist");
                        props.setHeader(playlistsHeader);
                        props.setFilter({ value: "", sort: "", sortDirection: undefined })
                    }}>
                <span><i className="fas fa-list-ul"></i></span>

                {isOpen && 
                    <span className="nav-description">Playlists</span>
                }
            </div>
            <div className="nav-item nav-bar-genres"
                    onClick={() => {
                        props.setData("Genre");
                        props.setHeader(genresHeader);
                        props.setFilter({ value: "", sort: "", sortDirection: undefined })
                    }}>
                <span><i className="fas fa-dna"></i></span>
                
                {isOpen && 
                    <span className="nav-description">Genres</span>
                }
            </div>
            <div className="nav-item nav-bar-comments"
                    onClick={() => {
                        props.setData("Comment");
                        props.setHeader(commentsHeader);
                        props.setFilter({ value: "", sort: "", sortDirection: undefined })
                    }}>
                <span><i className="fas fa-comments"></i></span>
                
                {isOpen && 
                    <span className="nav-description">Comments</span>
                }
            </div>
        </div>
    );
}

export default NavigationAdmin;