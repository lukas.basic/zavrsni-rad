import React from 'react';
import EditModal from './EditModals/EditModal';
import ImageModal from './ImageModal';
import AddToPlaylist from './AddToPlaylistModal';

import '../Styles/Modal.css';

function Modal(props) {
    return (
        <div className="Modal-screen">
            <div className="Modal-window">
                <div className="Modal-content">
                    {props.modal.type === "Picture" && 
                        <ImageModal picture={props.modal.picture} setShowModal={props.setShowModal}/>
                    }
                    {props.modal.type === "AddToPlaylist" && 
                        <AddToPlaylist song_ID={props.modal.song_ID} setShowModal={props.setShowModal}/>
                    }
                    {props.modal.type !== "Picture" && props.modal.type !== "AddToPlaylist" && 
                        <EditModal modal={props.modal} setShowModal={props.setShowModal}/>
                    }
                </div>
            </div>
        </div>
    );
}

export default Modal;