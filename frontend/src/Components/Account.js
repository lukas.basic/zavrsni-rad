import React, { useState, useEffect } from "react";
import {
  useParams,
  Redirect
} from "react-router-dom";
import AlbumRow from './AlbumRow';
import PlaylistRow from './PlaylistRow';
import Loader from './Loader';
import Modal from '../Components/Modal';

import {connect} from 'react-redux';
import axios from "axios";
import {logoutUser} from '../Services/index';

import '../Styles/Account.css';

function Account(props) {
    let { username } = useParams();

    const [update, setUpdate] = useState(1);
    const [showModal, setShowModal] = useState(false);

    const [loaderA, setLoaderA] = useState(true);
    const [image, setImage] = useState("");
    const [account, setAccount] = useState({});
    const [followers, setFollowers] = useState(0);
    useEffect(() => {
        setLoaderA(true);
        fetch('http://localhost:8080/api/v1/account/get?username=' + username)
            .then(response => response.json())
            .then(data => {
                if (!data.message) {
                    setAccount(data);
                    setFollowers(data.followers);
                    fetch('http://localhost:8080/api/v1/account/image?image_name=' + data.picture)
                        .then(response => response.json())
                        .then(data => setImage("data:image/png;base64," + data.data));
                    setLoaderA(false);
                }
            });
    }, [username, update]);

    const [loaderB, setLoaderB] = useState(true);
    const [albums, setAlbums] = useState([]);
    const [rows, setRows] = useState([]);
    useEffect(() => {
        setLoaderB(true);
        fetch('http://localhost:8080/api/v1/account/albums?username=' + username)
            .then(response => response.json())
            .then(data => {
                setAlbums(data);
                setLoaderB(false);     
                let tmp = [];
                for (let i = 0; i < data.length/4; i++) {
                    tmp.push(i);
                }
                setRows(tmp);
            });
    }, [username]);

    const [loaderC, setLoaderC] = useState(true);
    const [playlists, setPlaylists] = useState([]);
    useEffect(() => {
        setLoaderC(true);

        let playlistLink = 'http://localhost:8080/api/v1/account/public-playlists?username=' + username;

        if (username === props.auth.isLoggedIn.username)
            playlistLink = 'http://localhost:8080/api/v1/account/my-playlists?account_ID=' + props.auth.isLoggedIn.account_ID;

        fetch(playlistLink)
            .then(response => response.json())
            .then(data => {
                setPlaylists(data);
                setLoaderC(false);
            });

            // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [username]);

    const [following, setFollowing] = useState(null);
    useEffect(() => {
        setFollowing(null);
        if (props.auth.isLoggedIn.isLoggedIn && props.auth.isLoggedIn.username !== username) {
            fetch(`http://localhost:8080/api/v1/account/following?username1=${props.auth.isLoggedIn.username}&username2=${username}`)
                .then(response => response.json())
                .then(data => {
                    setFollowing(data);
                });
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [username]);

    let handleEdit = () => {
        setShowModal({
            type: "Account", 
            action: "edit",
            model: account,
            picture: image,
            update: () => {setUpdate(update + 1)}})
    }

    const [redirect, setRedirect] = useState(false);
    let handleDelete = () => {
        if (window.confirm("Are you sure you want to delete your account?")) {
            fetch('http://localhost:8080/api/v1/account/delete?account_ID=' + account.account_ID)
                .then(response => response.json())
                .then(data => {
                    if (data.message)
                        alert(data.message);
                })
                .catch(() => {
                    alert("Account successfully deleted!");
                    props.logoutUser();
                    setRedirect(true);
                });
        }
    }

    const followForm = new FormData();
    followForm.append('username1', props.auth.isLoggedIn.username);
    followForm.append('username2', username);

    let follow = () => {
        axios.post('http://localhost:8080/api/v1/account/follow', followForm)
                .then(data => {
                    if (data.message)
                        alert(data.message);
                    else {
                        setFollowing(true);
                        setFollowers(followers + 1);
                    }
                });
    }

    let unfollow = () => {
        axios.post('http://localhost:8080/api/v1/account/unfollow', followForm)
                .then(data => {
                    if (data.message)
                        alert(data.message);
                    else {
                        setFollowing(false);
                        setFollowers(followers - 1);
                    }
                });
    }

    if (loaderA || loaderB || loaderC)
        return (
            <Loader />
        );
    else
        return (
            <>
            {redirect &&
                <Redirect to="/" />
            }
            {showModal &&
                <Modal modal={showModal} setShowModal={setShowModal} />
            }
            <div className="account-header-container">
                <div className="account-header">
                    <img
                        className="artwork account-header-artwork"
                        src={image}
                        alt={`profile pic`}
                    />
                    <div className="account-info-container">
                        <div className="account-info">
                            <span className="account-desc">Username: </span>
                            <span className="account-username">{account.username}</span>
                        </div>
                        <div className="account-info">
                            <span className="account-desc">Stage name: </span>
                            <span className="account-username">{account.stage_name}</span>
                        </div>
                        <div className="account-info">
                            <span className="account-desc">Following: {account.following}</span>
                        </div>
                        <div className="account-info">
                            <span className="account-desc">Followers: {followers}</span>
                        </div>
                    </div>
                    <div className="account-info-container">
                        {props.auth.isLoggedIn.isLoggedIn && props.auth.isLoggedIn.username === username &&
                            <div className="account-info-buttons">
                                <button className="button" onClick={handleEdit}>
                                    <span className="account-desc">Edit</span>
                                </button>
                                <button className="button" onClick={handleDelete}>
                                    <span className="account-desc">Delete</span>
                                </button>
                            </div>
                        }
                        {props.auth.isLoggedIn.isLoggedIn && following === true &&
                            <button className="button" onClick={unfollow}>
                                <span className="account-desc">Unfollow</span>
                            </button>
                        }
                        {props.auth.isLoggedIn.isLoggedIn && following === false &&
                            <button className="button" onClick={follow}>
                                <span className="account-desc">Follow</span>
                            </button>
                        }
                    </div>
                </div>

                {albums.length > 0 &&
                    <div className="section-desc">
                        <div className="l b"></div>
                        <span>Albums</span>
                        <div className="l a"></div>
                    </div>
                }                
                <div className="album-card-container">
                    {rows.map(i => {
                        return <AlbumRow ind={i} albums={albums}/>
                    })}
                </div>

                {playlists.length > 0 &&
                    <div className="section-desc">
                        <div className="l b"></div>
                        <span>Playlists</span>
                        <div className="l a"></div>
                    </div>
                }                
                <div className="album-card-container">
                    {playlists.length > 0 &&
                        <span style={{"font-size":"small", "margin-right":"40%"}}>Total: {playlists.length} playlists</span>
                    }
                    {playlists.map(playlist => {
                        return <PlaylistRow key={playlist} playlist={playlist} setShowModal={setShowModal} />
                    })}
                </div>
            </div>
            </>
        );
}

const mapStateToProps = state => {
    return {
        auth: state.auth
    };
};

const mapDispatchToProps = dispatch => {
    return {
        logoutUser: () => dispatch(logoutUser())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Account);