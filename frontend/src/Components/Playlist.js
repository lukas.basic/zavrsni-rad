import React, { useState, useEffect } from "react";
import {
  useParams,
  Link,
  Redirect
} from "react-router-dom";
import Loader from '../Components/Loader';
import {connect} from 'react-redux';
import Modal from '../Components/Modal';
import axios from "axios";

import SongRow from './SongRow';

function Playlist(props) {
    let { playlistId } = useParams();

    const [showModal, setShowModal] = useState(false);
    const [update, setUpdate] = useState(0);

    const [loaderP, setLoaderP] = useState(true);
    const [image, setImage] = useState("");
    const [playlist, setPlaylist] = useState({});
    const [author, setAuthor] = useState(false);
    useEffect(() => {
        fetch('http://localhost:8080/api/v1/playlist/get?playlist_ID=' + playlistId)
            .then(response => response.json())
            .then(data => {
                if (!data.message) {
                    setPlaylist(data);
                    fetch('http://localhost:8080/api/v1/account/image?image_name=' + data.account.picture)
                        .then(response => response.json())
                        .then(data => setImage("data:image/png;base64," + data.data));
                    setLoaderP(false);

                    if (data.account.username === props.auth.isLoggedIn.username)
                        setAuthor(true);
                }
            });
    });

    let edit = () => {
        setShowModal({type: "Playlist",
                        action: "edit", 
                        model: playlist, 
                        update: props.setUpdate});
    }

    const [redirect, setRedirect] = useState(false);
    let handleDelete = () => {
        if (window.confirm("Are you sure you want to delete this playlist?"))
            fetch('http://localhost:8080/api/v1/playlist/delete?playlist_ID=' + playlist.playlist_ID)
                .then(response => response.json())
                .then(data => {
                    if (data.message)
                        alert(data.message);
                })
                .catch(() => {
                    setRedirect(true);
                });;
    }

    let removeFromPlaylist = song_ID => {
        const plForm = new FormData();
        plForm.append('song_ID', song_ID);
        plForm.append('playlist_ID', playlistId);

        axios.post('http://localhost:8080/api/v1/playlist/remove', plForm)
                .then(data => {
                    if (data.message)
                        alert(data.message);
                });
        setUpdate(update + 1);
    }

    let setTrackList = index => {
        props.setTrackList({
            index: index - 1,
            list: playlist.songs,
            source: {
                type: "playlist",
                name: playlist.name
            }
        })
    }

    if (loaderP)
        return (
            <Loader />
        );
    else
        return (
            <>
            {redirect &&
                <Redirect to="/playlists"/>
            }
            {showModal &&
                <Modal modal={showModal} setShowModal={setShowModal} />
            }
            <div className="album-header-container">
                <div className="album-header">
                    <img
                        className="artwork album-header-artwork"
                        src={image}
                        alt={`profile pic`}
                    />
                    <div className="album-header-info">
                        <span className="title">{playlist.name}</span>
                        <br/>
                        
                        <span className="playlist-author">by <Link to={"/accounts/" + playlist.account.username} className="nav-link">{playlist.account.displayName}</Link></span>
                    </div>
                    <div className="album-header-info">
                        {props.auth.isLoggedIn.isLoggedIn && author && playlist.modifiable &&
                            <div className="album-header-buttons">
                                <button className="like button" onClick={edit}>
                                    <span className="album-desc">Edit</span>
                                </button>
                                <button className="like button" onClick={handleDelete}>
                                    <span className="album-desc">Delete</span>
                                </button>
                            </div>
                        }
                    </div>
                </div>

                <div className="album-songs">
                    {playlist.songs.length === 0 &&
                        <span>There's no songs here yet...</span>
                    }
                    {playlist.songs.map((song, index) => {
                        return <SongRow key={song} song={song} order={index+1} setRemove={removeFromPlaylist} setTrackList={setTrackList} />
                    })}
                </div>

            </div>
            </>
        );
}

const mapStateToProps = state => {
    return {
        auth: state.auth
    };
  };
  
  export default connect(mapStateToProps)(Playlist);