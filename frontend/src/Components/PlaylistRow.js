import React from 'react';
import {
    Link
} from "react-router-dom";
import {connect} from 'react-redux';

function PlaylistRow(props) {
    const playlist = props.playlist;
    const owner = playlist.account ? playlist.account.account_ID === props.auth.isLoggedIn.account_ID : 
                                    playlist.account_ID === props.auth.isLoggedIn.account_ID;
    const modifiable = playlist.modifiable;

    let edit = () => {
        props.setShowModal({type: "Playlist",
                            action: "edit", 
                            model: playlist, 
                            update: props.setUpdate});
    }

    let handleDelete = () => {
        if (window.confirm("Are you sure you want to delete this playlist?"))
            fetch('http://localhost:8080/api/v1/playlist/delete?playlist_ID=' + playlist.playlist_ID)
                .then(response => response.json())
                .then(data => {
                    if (data.message)
                        alert(data.message);
                });
    }

    return (
        <>
        <div className="playlist-row">
            <Link to={'/playlists/' + playlist.playlist_ID} className="nav-link playlist-row-link">
                <span>{playlist.name} </span>
                {playlist.account &&
                    <span style={{"font-size": "x-small", "margin-left": "15px"}}> by {playlist.account.displayName}</span>
                }
                {props.auth.isLoggedIn.isLoggedIn && owner && modifiable &&
                    <div className="playlist-info">
                        <span>Created: {playlist.created}</span>
                        {playlist.last_modified &&
                            <span>Edited: {playlist.last_modified}</span>
                        }
                    </div>
                }
                {props.auth.isLoggedIn.isLoggedIn && owner && !modifiable &&
                    <div className="playlist-info">
                        <span style={{"font-size":"xsmall"}}>default playlist</span>
                    </div>
                }
            </Link>
            {props.auth.isLoggedIn.isLoggedIn && owner && modifiable &&
                <div className="playlist-buttons">
                    <div className="row-buttons clickable"
                        onClick={edit}>
                        <span><i className="fas fa-edit"></i></span>
                    </div>
                    <div className="row-buttons clickable"
                                    onClick={handleDelete}>
                        <span><i className="fas fa-trash"></i></span>
                    </div>
                </div>
            }
        </div>
        </>
    );
}

const mapStateToProps = state => {
    return {
        auth: state.auth
    };
};

export default connect(mapStateToProps)(PlaylistRow);