import React from 'react';
import ArtistCard from '../Components/ArtistCard';

function AlbumRow({ ind, artists }) {
    return (
        <div key={ind} className="album-card-row">
            {artists.map((account, index) => {
                if (index >= ind*4 && index < ind*4 + 4)
                    return <ArtistCard key={index} account={account} />;
                else
                    return <></>; 
            })}
            {Math.floor(artists.length/4) === ind && artists.length%4 === 1 && 
                <>
                    <div className="album-card-empty" />
                    <div className="album-card-empty" />
                    <div className="album-card-empty" />
                </>
            }
            {Math.floor(artists.length/4) === ind && artists.length%4 === 2 && 
                <>
                    <div className="album-card-empty" />
                    <div className="album-card-empty" />
                </>
            }
            {Math.floor(artists.length/4) === ind && artists.length%4 === 3 && 
                <>
                    <div className="album-card-empty" />
                </>
            }
        </div>
    );
}

export default AlbumRow;