import React, { useState, useEffect } from 'react';
import {
  useParams
} from "react-router-dom";
import Loader from './Loader';
import Accounts from './AccountsComma';
import Comment from './Comment.js';
import {connect} from 'react-redux';

function SongComments(props) {
    let { songId } = useParams();

    const [loader, setLoader] = useState(true);
    const [image, setImage] = useState("");
    const [song, setSong] = useState({});
    useEffect(() => {
        fetch('http://localhost:8080/api/v1/song/get?song_ID=' + songId)
            .then(response => response.json())
            .then(data => {
                if (!data.message) {
                    setSong(data);
                    fetch('http://localhost:8080/api/v1/album/image?image_name=' + data.album.picture)
                        .then(response => response.json())
                        .then(data => setImage("data:image/png;base64," + data.data));
                    setLoader(false);
                }
            });
    });

    const [comment, setComment] = useState("");

    let submit = () => {
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                content: comment,
                account_id: props.auth.isLoggedIn.account_ID,
                song_id: songId
            })
        };

        fetch('http://localhost:8080/api/v1/comment', requestOptions)
            .then(() => {
                setComment("");
            });
    }

    if (loader)
        return (
            <Loader />
        );
    else
        return (
            <div className="album-header-container">
                <div className="album-header">
                    <img
                        className="artwork album-header-artwork"
                        src={image}
                        alt={`track artwork`}
                    />
                    <div className="album-header-info">
                        <div className="song-name">
                            <span>{song.name}</span>
                            {song.collaborationsString &&
                                <span> (ft. {song.collaborationsString})</span>
                            }
                        </div>
                        
                        <Accounts accounts={song.accounts} />
                    </div>
                </div>

                {song.comments.length === 0 &&
                    <div className="album-songs">
                        No comments yet...
                    </div>
                }

                <div className="album-songs">
                    {song.comments.map(comment => {
                        return <Comment key={comment} comment={comment} />
                    })}
                </div>

                {props.auth.isLoggedIn.isLoggedIn &&
                    <div className="album-songs">
                        <form>
                            <label for="comment">Write a new comment:</label>
                            <br/>
                            <textarea id="comment" name="comment" rows="4" cols="50"
                                    value={comment} onChange={e => setComment(e.target.value)}>
                            </textarea>
                            <br />
                            <input type="button" className="button" value="Submit" onClick={submit}/>
                        </form>
                    </div>
                }
            </div>
        );
}

const mapStateToProps = state => {
    return {
        auth: state.auth
    };
};
  
  export default connect(mapStateToProps)(SongComments);