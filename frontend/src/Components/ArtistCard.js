import React, {useState, useEffect} from 'react';
import {
  Link
} from "react-router-dom";
import Loader from './Loader';

import '../Styles/AlbumCard.css';

function ArtistCard(props) {
    const [loader, setLoader] = useState(true);
    const [image, setImage] = useState("");
    useEffect(() => {
        fetch('http://localhost:8080/api/v1/account/image?image_name=' + props.account.picture)
            .then(response => response.json())
            .then(data => setImage("data:image/png;base64," + data.data))
            .then(() => setLoader(false));

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    if (loader)
        return (
            <div className="album-card">
                <div className="album-info">
                    <Loader />
                </div>
            </div>
        );
    else
        return (
            <div className="album-card">
                <Link to={"/accounts/" + props.account.username} className="nav-link">
                    <div className="album-info">
                        <img
                            className="artwork"
                            src={image}
                            alt={`profile pic`}
                        />
                        <span className="title">{props.account.displayName}</span>
                    </div>
                </Link>
            </div>
        );
}

export default ArtistCard;