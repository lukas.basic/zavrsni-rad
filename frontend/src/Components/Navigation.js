import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Link } from "react-router-dom";

import '../Styles/Navigation.css';

class Navigation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false
        }
    }
    
    setIsOpen = open => {
        this.setState({
            isOpen: open
        })
    }

    render() {
        return (
            <div className="nav-bar" onMouseEnter={() => this.setIsOpen(true)}
                                    onMouseLeave={() => this.setIsOpen(false)}>
                <div className="nav-item nav-bar-discover">
                    <Link to="/" style={{ textDecoration: 'none', color:  'white'}}>
                        <div>
                            <span><i className="fas fa-headphones"></i></span>

                            {this.state.isOpen && 
                                <span className="nav-description">Discover</span>
                            }
                        </div>
                    </Link>
                </div>
                <div className="nav-item nav-bar-favorites">
                    <Link to="/favourites" style={{ textDecoration: 'none', color:  'white'}}>
                        <div>
                            <span><i className="fas fa-heart"></i></span>

                            {this.state.isOpen && 
                                <span className="nav-description">Favourites</span>
                            }
                        </div>
                    </Link>
                </div>
                <div className="nav-item nav-bar-playlists">
                    <Link to="/playlists" style={{ textDecoration: 'none', color:  'white'}}>
                        <div>
                            <span><i className="fas fa-list-ul"></i></span>

                            {this.state.isOpen && 
                                <span className="nav-description">Playlists</span>
                            }
                        </div>
                    </Link>
                </div>
                <div className="nav-item nav-bar-publish">
                    <Link to="/publish" style={{ textDecoration: 'none', color:  'white'}}>
                        <div>
                            <span><i className="fas fa-plus"></i></span>

                            {this.state.isOpen && 
                                <span className="nav-description">Publish</span>
                            }
                        </div>
                    </Link>
                </div>
                <div className="nav-item nav-bar-info">
                    <Link to="/info" style={{ textDecoration: 'none', color:  'white'}}>
                        <div>
                            <span><i className="fas fa-info"></i></span>
                            
                            {this.state.isOpen && 
                                <span className="nav-description">Info</span>
                            }
                        </div>
                    </Link>
                </div>
                {this.props.auth.isLoggedIn.authorities === "ADMIN" &&
                    <div className="nav-item nav-bar-admin">
                        <Link to="/admin" style={{ textDecoration: 'none', color:  'white'}}>
                            <div>
                                <span><i className="fas fa-users-cog"></i></span>
                                
                                {this.state.isOpen && 
                                    <span className="nav-description">Admin</span>
                                }
                            </div>
                        </Link>
                    </div>
                }
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        auth: state.auth
    };
};

export default connect(mapStateToProps)(Navigation);
