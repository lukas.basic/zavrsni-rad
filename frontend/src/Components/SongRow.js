import React, { useEffect, useState } from 'react';
import {
  Link
} from "react-router-dom";
import {connect} from 'react-redux';
import axios from "axios";
import Modal from '../Components/Modal';

import Accounts from './AccountsComma';

import '../Styles/SongRow.css';

function SongRow(props) {
    const [showModal, setShowModal] = useState(false);

    const [loader, setLoader] = useState(props.image === undefined);
    const [image, setImage] = useState("");
    useEffect(() => {
        if (loader) {
            fetch('http://localhost:8080/api/v1/album/image?image_name=' + props.song.album.picture)
                .then(response => response.json())
                .then(data => setImage("data:image/png;base64," + data.data))
                .then(() => setLoader(false));
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const [liked, setLiked] = useState(null);
    useEffect(() => {
        if (props.auth.isLoggedIn.isLoggedIn) {
            fetch(`http://localhost:8080/api/v1/account/likes-song?username=${props.auth.isLoggedIn.username}&song_ID=${props.song.song_ID}`)
                .then(response => response.json())
                .then(data => {
                    setLiked(data);
                });
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const likeForm = new FormData();
    likeForm.append('username', props.auth.isLoggedIn.username);
    likeForm.append('song_ID', props.song.song_ID);

    let like = () => {
        axios.post('http://localhost:8080/api/v1/account/like', likeForm)
                .then(data => {
                    if (data.message)
                        alert(data.message);
                    else {
                        setLiked(true);
                    }
                });
    }

    let dislike = () => {
        axios.post('http://localhost:8080/api/v1/account/dislike', likeForm)
                .then(data => {
                    if (data.message)
                        alert(data.message);
                    else {
                        setLiked(false);
                    }
                });
    }

    let addToPlaylist = () => {
            setShowModal({
                type: "AddToPlaylist",
                song_ID: props.song.song_ID
            })
    }

    if (loader)
        return <span>Loading...</span>;
    else
        return (
            <>
            {showModal &&
                <Modal modal={showModal} setShowModal={setShowModal} />
            }
            <div className="song-row">
                <img
                    className="artwork-song"
                    src={props.image ? props.image : image}
                    alt={`track artwork`}
                />
                <div className="song-name" onClick={() => props.setTrackList(props.order)}>
                    <span>{props.order}. </span>
                    <span>{props.song.name}</span>
                    {props.song.collaborationsString &&
                        <span> (ft. {props.song.collaborationsString})</span>
                    }
                </div>
                <div className="song-artists">
                    <Accounts accounts={props.song.accounts} />
                </div>
                <div className="song-buttons">
                    {props.auth.isLoggedIn.isLoggedIn && liked === true &&
                        <div className="like tooltip" onClick={dislike}>
                            <span className="album-desc"><i className="fas fa-heart"></i></span>
                            <span class="tooltiptext">Dislike</span>
                        </div>
                    }
                    {props.auth.isLoggedIn.isLoggedIn && liked === false &&
                        <div className="like tooltip" onClick={like}>
                            <span className="album-desc"><i className="far fa-heart"></i></span>
                            <span class="tooltiptext">Like</span>
                        </div>
                    }
                    <Link to={"/comments/" + props.song.song_ID} className="nav-link tooltip">
                        <span><i className="fas fa-comments"></i></span>
                        <span class="tooltiptext">Comments</span>
                    </Link>
                    {props.auth.isLoggedIn.isLoggedIn && !props.setRemove &&
                        <div className="like tooltip" onClick={addToPlaylist}>
                            <span className="album-desc"><i className="fas fa-plus"></i></span>
                            <span class="tooltiptext">Add to playlist</span>
                        </div>
                    }
                    {props.auth.isLoggedIn.isLoggedIn && props.setRemove &&
                        <div className="like tooltip" onClick={() => props.setRemove(props.song.song_ID)}>
                            <span className="album-desc"><i className="fas fa-minus"></i></span>
                            <span class="tooltiptext">Remove from playlist</span>
                        </div>
                    }
                </div>
            </div>
            </>
        );
}

const mapStateToProps = state => {
    return {
        auth: state.auth
    };
};

export default connect(mapStateToProps)(SongRow);