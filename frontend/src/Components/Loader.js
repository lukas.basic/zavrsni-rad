import React from 'react';
import logo from '../syla.svg';

function Loader(props) {
    if (props.update !== undefined) {
        setTimeout(() => { props.update(false); }, 3000);
    }

    return (
        <div className="App-loader">
            <img src={logo} className="App-logo" alt="logo" />
            <span className="loader-caption">{props.caption}</span>
        </div>
    );
}

export default Loader;