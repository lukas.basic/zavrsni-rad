import React, {useState, useEffect} from 'react';
import {
  Link
} from "react-router-dom";
import Loader from './Loader';
import Accounts from './AccountsComma';

import '../Styles/AlbumCard.css';

function AlbumCard(props) {
    const [loader, setLoader] = useState(true);
    const [image, setImage] = useState("");
    useEffect(() => {
        fetch('http://localhost:8080/api/v1/album/image?image_name=' + props.album.picture)
            .then(response => response.json())
            .then(data => setImage("data:image/png;base64," + data.data))
            .then(() => setLoader(false));

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    if (loader)
        return (
            <div className="album-card">
                <div className="album-info">
                    <Loader />
                </div>
            </div>
        );
    else
        return (
            <div className="album-card">
                <Link to={"/albums/" + props.album.album_ID} className="nav-link">
                    <div className="album-info">
                        <img
                            className="artwork"
                            src={image}
                            alt={`track artwork for ${props.album.name} by ${props.album.accounts[0].displayName}`}
                        />
                        <span className="title">{props.album.name}</span>
                        <br/>
                        
                        <Accounts accounts={props.album.accounts} />
                    </div>
                </Link>
            </div>
        );
}

export default AlbumCard;