import React, { useState, useEffect } from 'react';
import {
  Link
} from "react-router-dom";
import {connect} from 'react-redux';
import Modal from './Modal';

import '../Styles/Comment.css';

function Comment(props) {
    const owner = props.comment.account.account_ID === props.auth.isLoggedIn.account_ID;

    const [image, setImage] = useState(null);
    useEffect(() => {
        fetch('http://localhost:8080/api/v1/account/image?image_name=' + props.comment.account.picture)
            .then(response => response.json())
            .then(data => {
                if (!data.message) {
                    setImage("data:image/png;base64," + data.data);
                }
            });

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const [showModal, setShowModal] = useState(false);
    let handleEdit = () => {
        setShowModal({type: "Comment", 
                        action: "edit",
                        model: props.comment, 
                        update: () => {}})
    }

    let handleDelete = () => {
        if (window.confirm("Are you sure you want to delete this comment?"))
            fetch('http://localhost:8080/api/v1/comment/delete?comment_ID=' + props.comment.comment_ID)
                .then(response => response.json())
                .then(data => {
                    if (data.message)
                        alert(data.message);
                });
    }

    if (image)
        return (
            <>
            {showModal &&
                <Modal modal={showModal} setShowModal={setShowModal} />
            }
            <div className="comment-row">
                <div className="comment-author">
                    <Link to={"/accounts/" + props.comment.account.username} className="nav-link comment-author-image">
                        <img
                            className="comment-author-image"
                            src={image}
                            alt={`profile pic`}
                        />
                    </Link>
                    <Link to={"/accounts/" + props.comment.account.username} className="nav-link comment-author-name">
                        <div className="comment-author-name">
                            <span>{props.comment.account.displayName}</span>
                        </div>
                    </Link>

                    {props.auth.isLoggedIn.isLoggedIn && owner &&
                        <div className="comment-buttons">
                            <div className="row-buttons clickable"
                                onClick={handleEdit}>
                                <span><i className="fas fa-edit"></i></span>
                            </div>
                            <div className="row-buttons clickable"
                                            onClick={handleDelete}>
                                <span><i className="fas fa-trash"></i></span>
                            </div>
                        </div>
                    }
                </div>
                <div className="comment-content">
                    <span>{props.comment.content}</span>
                </div>
                <div className="comment-info">
                    <span>Created: {props.comment.created}</span>
                    {props.comment.last_modified &&
                        <span>Edited: {props.comment.last_modified}</span>
                    }
                </div>
            </div>
            </>
        );
    else 
        return <span>Loading...</span>;
}

const mapStateToProps = state => {
    return {
        auth: state.auth
    };
};
  
  export default connect(mapStateToProps)(Comment);