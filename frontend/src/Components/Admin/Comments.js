import React, { useState, useEffect } from 'react';
import Loader from '../Loader';

import '../../Styles/Table.css';

function Comments(props) {
    const [showContent, setShowContent] = useState(false);
    const [comments, setComments] = useState(false);

    useEffect(() => {
        fetch('http://localhost:8080/api/v1/comment')
            .then(response => response.json())
            .then(data => setComments(data));
    }, []);

    let handleDelete = comment => {
        if (window.confirm("Are you sure you want to delete this comment?"))
            fetch('http://localhost:8080/api/v1/comment/delete?comment_ID=' + comment.comment_ID)
                .then(response => response.json())
                .then(data => {
                    if (data.message)
                        alert(data.message);
                    else 
                        props.update(true);
                });
    }

    let filtered = false;

    let filter = () => {
        filtered = false;
        let tmp = [];
        let f = props.filter.value.toLowerCase();

        if (f === "")
            filtered = comments;
        else {
            comments.map((a) => {
                if (a.account.displayName.toLowerCase().includes(f) ||
                    a.song.name.toLowerCase().includes(f) ||
                    a.content.toLowerCase().includes(f)
                ) {
                    tmp.push(a);
                }
                return <></>;
            }, filtered = tmp);
        }
    }

    filter();

    return (
    <>
        {!filtered &&
            <Loader caption={"Loading..."} />
        }
        {filtered && filtered.length === 0 &&
            <Loader caption={"No results!"} />
        }
        {filtered && filtered.length !== 0 && filtered.map((comment) =>
            <div className="table-comment"
                key={comment.comment_id}
                onMouseEnter={() => setShowContent(comment.comment_ID)}>
                <div className="table-row">
                    <div>
                        <span>{comment.account.displayName}</span>
                    </div>
                    <div>
                        <span>{comment.song.name}</span>
                    </div>
                    <div>
                        <span>{comment.created}</span>
                    </div>
                    <div>
                        <span>{comment.last_modified}</span>
                    </div>
                    <div className="row-buttons clickable"
                    onClick={() => props.setShowModal({type: "Comment", 
                                                        action: "edit",
                                                        model: comment, 
                                                        update: props.setUpdate})}>
                        <span><i className="fas fa-edit"></i></span>
                    </div>
                    <div className="row-buttons clickable"
                                    onClick={() => handleDelete(comment)}>
                        <span><i className="fas fa-trash"></i></span>
                    </div>
                </div>
                {!showContent &&
                    <div className="content-hidden">
                        <div>
                            <span>{comment.content}</span>
                        </div>
                    </div>
                }
                {showContent === comment.comment_ID &&
                    <div className="content-show">
                        <div>
                            <span>{comment.content}</span>
                        </div>
                    </div>
                }
            </div>
        )}
    </>
    );
}

export default Comments;