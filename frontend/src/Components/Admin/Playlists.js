import React, { useState, useEffect } from 'react';
import Loader from '../Loader';

import '../../Styles/Table.css';

function Playlists(props) {
    const [playlists, setPlaylists] = useState(false);

    useEffect(() => {
        fetch('http://localhost:8080/api/v1/playlist')
            .then(response => response.json())
            .then(data => setPlaylists(data));
    }, []);

    let handleDelete = playlist => {
        if (window.confirm("Are you sure you want to delete this playlist?"))
            fetch('http://localhost:8080/api/v1/playlist/delete?playlist_ID=' + playlist.playlist_ID)
                .then(response => response.json())
                .then(data => {
                    if (data.message)
                        alert(data.message);
                    else 
                        props.update(true);
                });
    }
    
    let filtered = false;
    let direction = props.filter.sortDirection;
    let sort = props.filter.sort;

    let filter = () => {
        filtered = false;
        let tmpS = [];
        let tmpF = [];
        let f = props.filter.value.toLowerCase();

        if (direction === undefined) 
            tmpS = playlists; 
        else {
            playlists.map((a) => {
                if (sort === "Default") {
                    if (direction && !a.modifiable)
                        tmpS.push(a);
                    else if (!direction && a.modifiable)
                        tmpS.push(a);
                } else if (sort === "Public") {
                    if (direction && a.public_access)
                        tmpS.push(a);
                    else if (!direction && !a.public_access)
                        tmpS.push(a);
                }
                return <></>;
            });
        }

        if (f === "")
            filtered = tmpS;
        else {
            tmpS.map((a) => {
                if (
                    a.name.toLowerCase().includes(f) ||
                    a.account.displayName.toLowerCase().includes(f)
                ) {
                    tmpF.push(a);
                }
                return <></>;
            }, filtered = tmpF);
        }
    }

    filter();

    return (
    <>
        {!filtered &&
            <Loader caption={"Loading..."} />
        }
        {filtered && filtered.length === 0 &&
            <Loader caption={"No results!"} />
        }
        {filtered && filtered.length !== 0 && filtered.map((playlist) =>
            <div className="table-row">
                <div>
                    <span>{playlist.name}</span>
                </div>
                <div>
                    <span>{playlist.account.displayName}</span>
                </div>
                <div>
                    {!playlist.modifiable && 
                        <span><i className="fas fa-check-circle"></i></span>
                    }
                    {playlist.modifiable && 
                        <span><i className="fas fa-times-circle"></i></span>
                    }
                </div>
                <div>
                    {playlist.public_access && 
                        <span><i className="fas fa-check-circle"></i></span>
                    }
                    {!playlist.public_access && 
                        <span><i className="fas fa-times-circle"></i></span>
                    }
                </div>
                <div>
                    {playlist.created}
                </div>
                <div>
                    {playlist.last_modified}
                </div>
                <div className="row-buttons clickable"
                    onClick={() => props.setShowModal({type: "Playlist",
                                                        action: "edit", 
                                                        model: playlist, 
                                                        update: props.setUpdate})}>
                    <span><i className="fas fa-edit"></i></span>
                </div>
                <div className="row-buttons clickable"
                                onClick={() => handleDelete(playlist)}>
                    <span><i className="fas fa-trash"></i></span>
                </div>
            </div>
        )}
    </>
    );
}

export default Playlists;