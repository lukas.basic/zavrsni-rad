import React, { useState, useEffect } from 'react';
import Loader from '../Loader';

import '../../Styles/Table.css';

function Songs(props) {
    const [songs, setSongs] = useState(false);

    let setSongsC = data => {
        let s = [];

        data.map(song => {
            if (song.collaborations[0] === "")
                song.collaborations = [];

            s.push(song);

            return <></>;
        })

        setSongs(s);
    }

    useEffect(() => {
        fetch('http://localhost:8080/api/v1/song')
            .then(response => response.json())
            .then(data => {setSongsC(data)});
    }, []);

    let handleDelete = song => {
        if (window.confirm("Are you sure you want to delete this song?"))
            fetch('http://localhost:8080/api/v1/song/delete?song_ID=' + song.song_ID)
                .then(response => response.json())
                .then(data => {
                    if (data.message)
                        alert(data.message);
                    else 
                        props.update(true);
                });
    }
    
    let filtered = false;

    let filter = () => {
        filtered = false;
        let tmp = [];
        let f = props.filter.value.toLowerCase();

        if (f === "")
            filtered = songs;
        else {
            songs.map((a) => {
                let pushed = false;
                a.collaborations.map(c => {
                    if (c.toLowerCase().includes(f)) {
                        tmp.push(a);
                        pushed = true;
                        return <></>;
                    }
                    return <></>;
                })

                if (!pushed &&
                    (a.name.toLowerCase().includes(f) ||
                    a.album.name.toLowerCase().includes(f))
                ) {
                    tmp.push(a);
                } else {
                    a.accounts.map((aa) => {
                        if (!pushed && aa.displayName.toLowerCase().includes(f)) {
                            tmp.push(a);
                            pushed = true;
                        }
                        return <></>;
                    });
                }
                return <></>;
            }, filtered = tmp);
        }
    }

    filter();

    return (
    <>
        {!filtered &&
            <Loader caption={"Loading..."} />
        }
        {filtered && filtered.length === 0 &&
            <Loader caption={"No results!"} />
        }
        {filtered && filtered.length !== 0 && filtered.map((song) =>
            <div className="table-row">
                <div>
                    <span>{song.name}</span>
                </div>
                <div>
                    <span>
                        {song.collaborations.map((c) =>
                            <span> |{c}| </span>
                        )}
                    </span>
                </div>
                <div>
                    <span>
                        {song.accounts.map((author) =>
                            <span> |{author.displayName}| </span>
                        )}
                    </span>
                </div>
                <div>
                    {song.album.name}
                </div>
                <div>
                    {song.album_order}
                </div>
                <div>
                    <span className="clickable" onClick={() => {}}>
                        Open
                    </span>
                </div>
                <div>
                    {song.released}
                </div>
                <div>
                    <span>
                        {song.genres.map((genre) =>
                            <span> |{genre.name}| </span>
                        )}
                    </span>
                </div>
                <div className="row-buttons clickable"
                    onClick={() => props.setShowModal({type: "Song", 
                                                        action: "edit",
                                                        model: song, 
                                                        update: props.setUpdate})}>
                    <span><i className="fas fa-edit"></i></span>
                </div>
                <div className="row-buttons clickable"
                                onClick={() => handleDelete(song)}>
                    <span><i className="fas fa-trash"></i></span>
                </div>
            </div>
        )}
    </>
    );
}

export default Songs;