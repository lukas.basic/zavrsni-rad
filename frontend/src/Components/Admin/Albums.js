import React, { useState, useEffect } from 'react';
import Loader from '../Loader';

import '../../Styles/Table.css';

function Albums(props) {
    const [albums, setAlbums] = useState(false);

    const [loader, setLoader] = useState(true);

    const [update, setUpdate] = useState(0);

    useEffect(() => {
        setLoader(true);
        fetch('http://localhost:8080/api/v1/album')
            .then(response => response.json())
            .then(data => {
                if (data !== albums)
                    setAlbums(data)
                setLoader(false);
            });

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [update]);

    let openEditModal = album => {
        fetch('http://localhost:8080/api/v1/album/image?image_name=' + album.picture)
            .then(response => response.json())
            .then(data => props.setShowModal({
                            type: "Album", 
                            action: "edit",
                            model: album, 
                            picture: "data:image/png;base64," + data.data,
                            update: setUpdate}))
    }

    let openImageModal = album => {
        fetch('http://localhost:8080/api/v1/album/image?image_name=' + album.picture)
            .then(response => response.json())
            .then(data => props.setShowModal({
                            type: "Picture",
                            picture: "data:image/png;base64," + data.data}))
        
    }

    let handleDelete = album => {
        if (window.confirm("Are you sure you want to delete this album?"))
            fetch('http://localhost:8080/api/v1/album/delete?album_ID=' + album.album_ID)
                .then(response => response.json())
                .then(data => {
                    if (data.message)
                        alert(data.message);
                    else 
                        setUpdate(update + 1);
                });
    }

    let filtered = false;

    let filter = () => {
        filtered = false;
        let tmp = [];
        let f = props.filter.value.toLowerCase();

        if (f === "")
            filtered = albums;
        else {
            albums.map((a) => {
                if (a.name.toLowerCase().includes(f)) {
                    tmp.push(a);
                } else {
                    let pushed = false;
                    a.accounts.map((aa) => {
                        if (!pushed && aa.displayName.toLowerCase().includes(f)) {
                            tmp.push(a);
                            pushed = true;
                        }
                        return <></>;
                    });
                }
                return <></>;
            }, filtered = tmp);
        }
    }

    filter();

    if (loader)
        return <Loader />
    else 
        return (
        <>
            {!filtered &&
                <Loader caption={"Loading..."} />
            }
            {filtered && filtered.length === 0 &&
                <Loader caption={"No results!"} />
            }
            {filtered && filtered.length !== 0 && filtered.map((album) =>
                <div className="table-row">
                    <div>
                        <span>{album.name}</span>
                    </div>
                    <div>
                        <span>
                            {album.accounts.map((author) =>
                                <span> |{author.displayName}| </span>
                            )}
                        </span>
                    </div>
                    <div>
                        <span className="clickable" 
                                onClick={() => openImageModal(album)}
                        >
                            View
                        </span>
                    </div>
                    <div>
                        {album.released}
                    </div>
                    <div className="row-buttons clickable">
                        <span><i className="fas fa-edit"
                                onClick={() => openEditModal(album)}
                        ></i></span>
                    </div>
                    <div className="row-buttons clickable"
                                    onClick={() => handleDelete(album)}>
                        <span><i className="fas fa-trash"></i></span>
                    </div>
                </div>
            )}
        </>
        );
}

export default Albums;