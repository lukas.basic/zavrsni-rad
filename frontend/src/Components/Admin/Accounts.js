import React, { useState, useEffect } from 'react';
import Loader from '../Loader';

import '../../Styles/Table.css';

function Accounts(props) {
    const [accounts, setAccounts] = useState(false);
    
    useEffect(() => {
        fetch('http://localhost:8080/api/v1/account')
            .then(response => response.json())
            .then(data => setAccounts(data));
    }, []);

    let openEditModal = account => {
        fetch('http://localhost:8080/api/v1/account/image?image_name=' + account.picture)
            .then(response => response.json())
            .then(data => props.setShowModal({
                            type: "Account", 
                            action: "edit",
                            model: account,
                            picture: "data:image/png;base64," + data.data,
                            update: props.setUpdate}))
    }

    let openImageModal = account => {
        fetch('http://localhost:8080/api/v1/account/image?image_name=' + account.picture)
            .then(response => response.json())
            .then(data => props.setShowModal({
                            type: "Picture",
                            picture: "data:image/png;base64," + data.data}))
        
    }

    let handleDelete = account => {
        if (window.confirm("Are you sure you want to delete this account?"))
            fetch('http://localhost:8080/api/v1/account/delete?account_ID=' + account.account_ID)
                .then(response => response.json())
                .then(data => {
                    if (data.message)
                        alert(data.message);
                    else 
                        props.update(true);
                });
    }

    let filtered = false;
    let direction = props.filter.sortDirection;
    let sort = props.filter.sort;

    let filter = () => {
        filtered = false;
        let tmpS = [];
        let tmpF = [];
        let f = props.filter.value.toLowerCase();

        if (direction === undefined) 
            tmpS = accounts; 
        else {
            accounts.map((a) => {
                if (sort === "Admin") {
                    if (direction && a.admin)
                        tmpS.push(a);
                    else if (!direction && !a.admin)
                        tmpS.push(a);
                } else if (sort === "Artist") {
                    if (direction && a.artist)
                        tmpS.push(a);
                    else if (!direction && !a.artist)
                        tmpS.push(a);
                }
                return <></>;
            });
        }

        if (f === "")
            filtered = tmpS;
        else {
            tmpS.map((a) => {
                if (
                    a.username.toLowerCase().includes(f) ||
                    a.stage_name.toLowerCase().includes(f) ||
                    a.email.toLowerCase().includes(f)
                ) {
                    tmpF.push(a);
                }
                return <></>;
            }, filtered = tmpF);
        }
    }

    filter();

    return (
    <>
        {!filtered &&
            <Loader caption={"Loading..."} />
        }
        {filtered && filtered.length === 0 &&
            <Loader caption={"No results!"} />
        }
        {filtered && filtered.length !== 0 && filtered.map((account) =>
            <div className="table-row">
                <div>
                    <span>{account.username}</span>
                </div>
                <div>
                    <span>{account.stage_name}</span>
                </div>
                <div>
                    <span>{account.email}</span>
                </div>
                <div>
                    <span className="clickable" 
                            onClick={() => openImageModal(account)}
                    >
                        View
                    </span>
                </div>
                <div>
                    {account.appearance && 
                        <span>{account.stage_name}</span>
                    }
                    {!account.appearance && 
                        <span>{account.username}</span>
                    }
                </div>
                <div>
                    {account.created}
                </div>
                <div>
                    {account.admin && 
                        <span><i className="fas fa-check-circle"></i></span>
                    }
                    {!account.admin && 
                        <span><i className="fas fa-times-circle"></i></span>
                    }
                </div>
                <div>
                    {account.artist && 
                        <span><i className="fas fa-check-circle"></i></span>
                    }
                    {!account.artist && 
                        <span><i className="fas fa-times-circle"></i></span>
                    }
                </div>
                <div className="row-buttons clickable"
                                onClick={() => openEditModal(account)}>
                    <span><i className="fas fa-edit"></i></span>
                </div>
                <div className="row-buttons clickable"
                                onClick={() => handleDelete(account)}>
                    <span><i className="fas fa-trash"></i></span>
                </div>
            </div>
        )}
    </>
    );
}

export default Accounts;