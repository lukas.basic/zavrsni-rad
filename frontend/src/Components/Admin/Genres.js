import React, { useState, useEffect } from 'react';
import Loader from '../Loader';

import '../../Styles/Table.css';

function Genres(props) {
    const [genres, setGenres] = useState(false);

    useEffect(() => {
        fetch('http://localhost:8080/api/v1/genre')
            .then(response => response.json())
            .then(data => setGenres(data));
    }, []);

    let handleDelete = genre => {
        if (window.confirm("Are you sure you want to delete this genre?"))
            fetch('http://localhost:8080/api/v1/genre/delete?genre_ID=' + genre.genre_ID)
                .then(response => response.json())
                .then(data => {
                    if (data.message)
                        alert(data.message);
                    else 
                        props.update(true);
                });
    }

    let filtered = false;

    let filter = () => {
        filtered = false;
        let tmp = [];
        let f = props.filter.value.toLowerCase();

        console.log(f);

        if (f === "")
            filtered = genres;
        else {
            genres.map((a) => {
                if (a.name.toLowerCase().includes(f)) {
                    tmp.push(a);
                    console.log(a.name);
                }
                return <></>;
            }, filtered = tmp);
        }

        return <></>;
    }

    filter();

    return (
    <>
        {!filtered &&
            <>
                <Loader caption={"Loading..."} />
            </>
        }
        {filtered && filtered.length === 0 &&
            <Loader caption={"No results!"} />
        }
        {filtered && filtered.length !== 0 && filtered.map((genre) =>
            <div className="table-row">
                <div>
                    <span>{genre.name}</span>
                </div>
                <div className="row-buttons clickable"
                    onClick={() => props.setShowModal({type: "Genre", 
                                                        action: "edit",
                                                        model: genre, 
                                                        update: props.setUpdate})}>
                    <span><i className="fas fa-edit"></i></span>
                </div>
                <div className="row-buttons clickable"
                                onClick={() => handleDelete(genre)}>
                    <span><i className="fas fa-trash"></i></span>
                </div>
            </div>
        )}
    </>
    );
}

export default Genres;