import React, { useState, useEffect } from "react";
import {
  useParams,
  Redirect
} from "react-router-dom";
import Loader from '../Components/Loader';

import {logoutUser} from '../Services/index';
import {connect} from 'react-redux';
import axios from "axios";

import Accounts from './AccountsComma';
import SongRow from './SongRow';
import Modal from './Modal';

import '../Styles/Album.css';

function Album(props) {
    let { albumId } = useParams();

    const [loaderA, setLoaderA] = useState(true);
    const [image, setImage] = useState("");
    const [album, setAlbum] = useState({});
    const [likes, setLikes] = useState(0);
    const [author, setAuthor] = useState(false);
    useEffect(() => {
        fetch('http://localhost:8080/api/v1/album/get?album_ID=' + albumId)
            .then(response => response.json())
            .then(data => {
                if (!data.message) {
                    setAlbum(data);
                    setLikes(data.likes);
                    fetch('http://localhost:8080/api/v1/album/image?image_name=' + data.picture)
                        .then(response => response.json())
                        .then(data => setImage("data:image/png;base64," + data.data));
                    setLoaderA(false);

                    data.accounts.map(a => {
                        if (a.username === props.auth.isLoggedIn.username) 
                            setAuthor(true);

                        return null;
                    });
                }
            });

        // eslint-disable-next-line react-hooks/exhaustive-deps
    });

    const [songs, setSongs] = useState([]);
    const [loaderS, setLoaderS] = useState(true);
    useEffect(() => {
        fetch('http://localhost:8080/api/v1/album/songs?album_ID=' + albumId)
            .then(response => response.json())
            .then(data => {
                if (!data.message) {
                    setSongs(data);
                    setLoaderS(false);
                }
            });
        
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const [liked, setLiked] = useState(null);
    useEffect(() => {
        if (props.auth.isLoggedIn.isLoggedIn) {
            fetch(`http://localhost:8080/api/v1/album/liked?username=${props.auth.isLoggedIn.username}&album_ID=${albumId}`)
                .then(response => response.json())
                .then(data => {
                    setLiked(data);
                });
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const [showModal, setShowModal] = useState(false);
    let handleEdit = () => {
        setShowModal({type: "Album", 
                    action: "edit",
                    model: album, 
                    picture: image,
                    update: () => {}})
    }

    const [redirect, setRedirect] = useState(false);
    let handleDelete = () => {
        if (window.confirm("Are you sure you want to delete this album?"))
            fetch('http://localhost:8080/api/v1/album/delete?album_ID=' + album.album_ID)
                .then(response => response.json())
                .then(data => {
                    if (data.message)
                        alert(data.message);
                })
                .catch(() => {
                    setRedirect(true);
                });
    }

    const likeForm = new FormData();
    likeForm.append('username', props.auth.isLoggedIn.username);
    likeForm.append('album_ID', albumId);

    let like = () => {
        axios.post('http://localhost:8080/api/v1/album/like', likeForm)
                .then(data => {
                    if (data.message)
                        alert(data.message);
                    else {
                        setLiked(true);
                        setLikes(likes + 1);
                    }
                });
    }

    let dislike = () => {
        axios.post('http://localhost:8080/api/v1/album/dislike', likeForm)
                .then(data => {
                    if (data.message)
                        alert(data.message);
                    else {
                        setLiked(false);
                        setLikes(likes - 1);
                    }
                });
    }

    let setTrackList = index => {
        props.setTrackList({
            index: index - 1,
            list: songs,
            source: {
                type: "album",
                name: album.name
            }
        })
    }

    if (loaderA || loaderS)
        return (
            <Loader />
        );
    else
        return (
            <>
            {redirect &&
                <Redirect to={"/accounts/" + props.auth.isLoggedIn.username}/>
            }
            {showModal &&
                <Modal modal={showModal} setShowModal={setShowModal} />
            }
            <div className="album-header-container">
                <div className="album-header">
                    <img
                        className="artwork album-header-artwork"
                        src={image}
                        alt={`track artwork`}
                    />
                    <div className="album-header-info">
                        <span className="title">{album.name}</span>
                        <br/>
                        
                        <Accounts accounts={album.accounts} />
                        <br />
                        
                        <span className="album-desc">Likes: {likes}</span>
                    </div>
                    <div className="album-header-info">
                        {props.auth.isLoggedIn.isLoggedIn && liked === true &&
                            <div className="like" onClick={dislike}>
                                <span className="album-desc"><i className="fas fa-heart"></i></span>
                            </div>
                        }
                        {props.auth.isLoggedIn.isLoggedIn && liked === false &&
                            <div className="like" onClick={like}>
                                <span className="album-desc"><i className="far fa-heart"></i></span>
                            </div>
                        }
                        {props.auth.isLoggedIn.isLoggedIn && author &&
                            <div className="album-header-buttons">
                                <button className="like button" onClick={handleEdit}>
                                    <span className="album-desc">Edit</span>
                                </button>
                                <button className="like button" onClick={handleDelete}>
                                    <span className="album-desc">Delete</span>
                                </button>
                            </div>
                        }
                    </div>
                </div>

                <div className="album-songs">
                    {songs.length === 0 &&
                        <span>This album is empty...</span>
                    }
                    {songs.map(song => {
                        return <SongRow key={song} song={song} image={image} order={song.album_order} setTrackList={setTrackList} />
                    })}
                </div>

            </div>
            </>
        );
}


const mapStateToProps = state => {
    return {
        auth: state.auth
    };
  };
  
  const mapDispatchToProps = dispatch => {
    return {
        logoutUser: () => dispatch(logoutUser())
    };
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(Album);