import React, { useState, useEffect } from 'react';
import Loader from './Loader';
import {connect} from 'react-redux';
import axios from "axios";

import '../Styles/Modal.css';

function AddToPlaylist(props) {

    const [loader, setLoader] = useState(true);
    const [loaderCaption, setLoaderCaption] = useState("");
    const [playlists, setPlaylists] = useState([]);
    useEffect(() => {
        fetch('http://localhost:8080/api/v1/account/my-playlists?account_ID=' + props.auth.isLoggedIn.account_ID)
            .then(response => response.json())
            .then(data => {
                setPlaylists(data);
                setLoader(false);
            });

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    let add = pid => {
        setLoader(true);
        setLoaderCaption("Just a moment...");

        const plForm = new FormData();
        plForm.append('song_ID', props.song_ID);
        plForm.append('playlist_ID', pid);

        axios.post('http://localhost:8080/api/v1/playlist/add', plForm)
                .then(data => {
                    if (data.message)
                        alert(data.message);
                    props.setShowModal(false);
                });
    }

    if (loader)
        return (<Loader caption={loaderCaption} />);
    else
        return (
            <>
                <span>Choose playlist</span>
                <div className="album-card-container">
                    {playlists.length === 0 &&
                        <div>
                            <span>You have to create playlist first!</span>
                        </div>
                    }
                    {playlists.map(playlist => {
                        if (playlist.modifiable)
                            return <>
                            <div className="playlist-row full-width" onClick={() => add(playlist.playlist_ID)}>
                                <span>{playlist.name}</span>
                                <div className="playlist-info">
                                    <span>Created: {playlist.created}</span>
                                    {playlist.last_modified &&
                                        <span>Edited: {playlist.last_modified}</span>
                                    }
                                </div>
                            </div>
                            </>

                        return <></>;
                    })}
                </div>
                <button className="button" onClick={() => props.setShowModal(false)}>Close</button>
            </>
        );
}

const mapStateToProps = state => {
    return {
        auth: state.auth
    };
};

export default connect(mapStateToProps)(AddToPlaylist);