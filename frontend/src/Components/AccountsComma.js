import React from 'react';
import {
  Link
} from "react-router-dom";

function AccountsComma(props) {
    return (
        <span>
            {props.accounts.map((acc, index) => {
                if (props.accounts.length - 1 === index)
                    return <span className="artist">
                                <Link to={"/accounts/" + acc.username} className="nav-link">{acc.displayName}</Link>
                            </span>
                else
                    return <span>
                                <span className="artist">
                                    <Link to={"/accounts/" + acc.username} className="nav-link">{acc.displayName}</Link>
                                </span><span>, </span>
                            </span>
            })}
        </span>
    );
}

export default AccountsComma;