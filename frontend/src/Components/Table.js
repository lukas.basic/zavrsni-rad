import React from 'react';
import Loader from './Loader';

import Accounts from "./Admin/Accounts";
import Albums from "./Admin/Albums";
import Songs from "./Admin/Songs";
import Playlists from "./Admin/Playlists";
import Genres from "./Admin/Genres";
import Comments from "./Admin/Comments";

import '../Styles/Table.css';

function Table(props) {
    let filter = props.filter;
    let setFilter = props.setFilter;

    let myChangeHandler = (event) => {
        let sort = filter.sort;
        let sortDirection = filter.sortDirection;
        setFilter({ 
            value: event.target.value,
            sort: sort,
            sortDirection: sortDirection
        });
    }

    let headerClickHandler = (item) => {
        let value = filter.value;
        let sort = filter.sort;
        let sortDirection = filter.sortDirection;
        setFilter({
            value: value,
            sort: item,
            sortDirection: (sort !== item) ? true : (sortDirection === undefined) ? true : ((sortDirection === true) ? false : undefined)
        });
    }

    let boolCol = item => {
        return (item === "Admin" && filter.sort === "Admin") ||
                (item === "Artist" && filter.sort === "Artist") ||
                (item === "Public" && filter.sort === "Public") ||
                (item === "Default" && filter.sort === "Default");
    }

    let empty = item => {
        return boolCol(item) && filter.sortDirection === undefined;
    }

    let check = item => {
        return boolCol(item) && filter.sortDirection;
    }

    let cross = item => {
        return boolCol(item) &&
                !filter.sortDirection &&
                filter.sortDirection !== undefined
    }

    let models = new Map();
    models.set("Account", {type: "Account", 
                            action: "add",
                            model: {username: "",
                                    stage_name: "",
                                    email: "",
                                    password: "",
                                    picture: "",
                                    appearance: false},
                            update: props.setUpdate});
    models.set("Album", {type: "Album", 
                            action: "add",
                            model: {name: "",
                                    picture: "",
                                    accounts: []},
                            update: props.setUpdate});
    models.set("Comment", {type: "Comment", 
                            action: "add",
                            model: {content: "",
                                    account_id: "",
                                    song_id: ""},
                            update: props.setUpdate});
    models.set("Genre", {type: "Genre", 
                            action: "add",
                            model: {name: ""},
                            update: props.setUpdate});
    models.set("Playlist", {type: "Playlist", 
                            action: "add",
                            model: {name: "",
                                    public_access: false,
                                    account_ID: ""},
                            update: props.setUpdate});
    models.set("Song", {type: "Song", 
                            action: "add",
                            model: {name: "",
                                    collaborations: [],
                                    album_ID: "",
                                    album_order: 0,
                                    accounts: [],
                                    genres: []},
                            update: props.setUpdate});
    return (
    <>
        {props.update &&
            <>
                <Loader />
                {props.setUpdate(false)}
            </>
        }
        {!props.update &&
            <>
                <div className="table-name">
                    <span>{props.data + "s"}</span>

                    <button className="button" onClick={() => props.setShowModal(models.get(props.data))}>{"Add new " + props.data}</button>
                </div>

                <div className="table-filter">
                    <input
                        type="text"
                        onChange={myChangeHandler}
                        placeholder="filter..."
                    />
                </div>

                <div className="table">
                    <div className="table-row table-header">
                        {props.headerItems.map((item) =>  
                            <div onClick={() => headerClickHandler(item)}>
                                {boolCol(item) && 
                                    <span style={{'cursor': 'pointer'}}>{item}</span>
                                }
                                {!boolCol(item) && 
                                    <span>{item}</span>
                                }
                                {empty(item) &&
                                    <span></span>
                                }
                                {check(item) &&
                                    <span><i className="fas fa-check-circle"></i></span>
                                }
                                {cross(item) &&
                                    <span><i className="fas fa-times-circle"></i></span>
                                }
                            </div> 
                        )}
                        <div className="row-buttons"></div>
                        <div className="row-buttons"></div>
                    </div>
                    <div className="header-line line"></div>
                    <div className="table-rows">
                        {props.data === "Account" &&
                            <Accounts filter={filter} setShowModal={props.setShowModal} setUpdate={props.setUpdate} />
                        }
                        {props.data === "Album" &&
                            <Albums filter={filter} setShowModal={props.setShowModal} setUpdate={props.setUpdate} />
                        }
                        {props.data === "Song" &&
                            <Songs filter={filter} setShowModal={props.setShowModal} setUpdate={props.setUpdate} />
                        }
                        {props.data === "Playlist" &&
                            <Playlists filter={filter} setShowModal={props.setShowModal} setUpdate={props.setUpdate} />
                        }
                        {props.data === "Genre" &&
                            <Genres filter={filter} setShowModal={props.setShowModal} setUpdate={props.setUpdate} />
                        }
                        {props.data === "Comment" &&
                            <Comments filter={filter} setShowModal={props.setShowModal} setUpdate={props.setUpdate} />
                        }
                    </div>
                </div>
            </>
        }
    </>
    );
}

export default Table;