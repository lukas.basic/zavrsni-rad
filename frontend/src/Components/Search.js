import React, { useState, useEffect } from "react";
import {
  useParams
} from "react-router-dom";
import Loader from '../Components/Loader';
import Modal from '../Components/Modal';

import SongRow from './SongRow';
import AlbumRow from './AlbumRow';
import ArtistRow from './ArtistRow';
import PlaylistRow from './PlaylistRow';

function Search(props) {
    let { search } = useParams();

    const [showModal, setShowModal] = useState(false);

    const [loader1, setLoader1] = useState(true);
    const [songs, setSongs] = useState([]);
    const [songsPage, setSongsPage] = useState(1);
    const [renderSongs, setRenderSongs] = useState([]);
    useEffect(() => {
        setLoader1(true);
        fetch('http://localhost:8080/api/v1/song/search?search=' + search)
            .then(response => response.json())
            .then(data => {
                if (!data.message)
                    setSongs(data);

                let tmp = [];
                for (let i = 0; i < 5 && i < data.length; i++)
                    tmp.push(data[i]);

                setRenderSongs(tmp);
                setLoader1(false);
            });
    }, [search]);

    const [loader2, setLoader2] = useState(true);
    const [albums, setAlbums] = useState([]);
    const [albumsPage, setAlbumsPage] = useState(1);
    useEffect(() => {
        setLoader2(true);
        fetch('http://localhost:8080/api/v1/album/search?search=' + search)
            .then(response => response.json())
            .then(data => {
                if (!data.message) {
                    setAlbums(data);
                }

                setLoader2(false);
            });
    }, [search]);

    const [loader3, setLoader3] = useState(true);
    const [artists, setArtists] = useState([]);
    const [artistsPage, setArtistsPage] = useState(1);
    useEffect(() => {
        setLoader3(true);
        fetch(`http://localhost:8080/api/v1/account/search?search=${search}&type=a`)
            .then(response => response.json())
            .then(data => {
                if (!data.message) {
                    setArtists(data);
                }

                setLoader3(false);
            });
    }, [search]);

    const [loader4, setLoader4] = useState(true);
    const [profiles, setProfiles] = useState([]);
    const [profilesPage, setProfilesPage] = useState(1);
    useEffect(() => {
        setLoader4(true);
        fetch(`http://localhost:8080/api/v1/account/search?search=${search}&type=p`)
            .then(response => response.json())
            .then(data => {
                if (!data.message) {
                    setProfiles(data);
                }

                setLoader4(false);
            });
    }, [search]);

    const [loader5, setLoader5] = useState(true);
    const [playlists, setPlaylists] = useState([]);
    const [playlistsPage, setPlaylistsPage] = useState(1);
    const [renderPlaylists, setRenderPlaylists] = useState([]);
    useEffect(() => {
        setLoader5(true);
        fetch('http://localhost:8080/api/v1/playlist/search?search=' + search)
            .then(response => response.json())
            .then(data => {
                if (!data.message) {
                    setPlaylists(data);

                    let tmp = [];
                    for (let i = 0; i < 5 && i < data.length; i++)
                        tmp.push(data[i]);

                    setRenderPlaylists(tmp);
                }
                setLoader5(false);
            });
    }, [search]);

    let results = () => {
        return songs.length !== 0 || 
                albums.length !== 0 ||
                artists.length !== 0 ||
                profiles.length !== 0 ||
                playlists.length !== 0;
    }

    let setTrackList = index => {
        props.setTrackList({
            index: index - 1,
            list: songs,
            source: {
                type: "searching",
                name: search
            }
        })
    }

    let previousSongsPage = () => {
        if (songsPage === 1)
            return;

        renderSongsSetting(songsPage - 1);
    }

    let nextSongsPage = () => {
        if (songs.length%5 === 0 ? songsPage === Math.floor(songs.length/5) : songsPage === Math.floor(songs.length/5) + 1)
            return;

        renderSongsSetting(songsPage + 1);
    }

    let renderSongsSetting = page => {
        let tmp = [];
        for (let i = (page - 1)*5; i < (page - 1)*5 + 5 && i < songs.length; i++)
            tmp.push(songs[i]);

        setRenderSongs(tmp);
        setSongsPage(page);
    }

    let previousAlbumsPage = () => {
        if (albumsPage === 1)
            return;

        setAlbumsPage(albumsPage - 1);
    }

    let nextAlbumsPage = () => {
        if (albums.length%4 === 0 ? albumsPage === Math.floor(albums.length/4) : albumsPage === Math.floor(albums.length/4) + 1)
            return;

        setAlbumsPage(albumsPage + 1);
    }

    let previousArtistsPage = () => {
        if (artistsPage === 1)
            return;

        setArtistsPage(artistsPage - 1);
    }

    let nextArtistsPage = () => {
        if (artists.length%4 === 0 ? artistsPage === Math.floor(artists.length/4) : artistsPage === Math.floor(artists.length/4) + 1)
            return;

        setArtistsPage(artistsPage + 1);
    }

    let previousProfilesPage = () => {
        if (profilesPage === 1)
            return;

        setProfilesPage(profilesPage - 1);
    }

    let nextProfilesPage = () => {
        if (profiles.length%4 === 0 ? profilesPage === Math.floor(profiles.length/4) : profilesPage === Math.floor(profiles.length/4) + 1)
            return;

        setProfilesPage(profilesPage + 1);
    }

    let previousPlaylistsPage = () => {
        if (playlistsPage === 1)
            return;

        renderPlaylistsSetting(playlistsPage - 1);
    }

    let nextPlaylistsPage = () => {
        if (playlists.length%5 === 0 ? playlistsPage === Math.floor(playlists.length/5) : playlistsPage === Math.floor(playlists.length/5) + 1)
            return;

        renderPlaylistsSetting(playlistsPage + 1);
    }

    let renderPlaylistsSetting = page => {
        let tmp = [];
        for (let i = (page - 1)*5; i < (page - 1)*5 + 5 && i < playlists.length; i++)
            tmp.push(playlists[i]);

        setRenderPlaylists(tmp);
        setPlaylistsPage(page);
    }

    if (loader1 || loader2 || loader3 || loader4 || loader5)
        return (
            <Loader caption={`Searching for ${search}`} />
        );
    else
        return (
            <>
            {showModal &&
                <Modal modal={showModal} setShowModal={setShowModal} />
            }
            <div className="album-card-container">


                    {results() &&
                        <div>
                            <span>Search results for: {search}</span>
                        </div>
                    }

                    {!results() &&
                        <div>
                            <span>No search results for: {search}</span>
                        </div>
                    }
                    

                    {songs.length > 0 &&
                        <>
                        <div className="section-desc">
                            <div className="l b"></div>
                            <span>Songs</span>
                            <div className="l a"></div>
                        </div>
                                    
                        <div className="search-songs">
                            {renderSongs.map((song, index) => {
                                return <SongRow key={song} song={song} order={index + 1} setTrackList={setTrackList} />
                            })}
                        </div>

                        <div style={{"font-size":"medium", "margin-top":"10px"}}>
                            <span>results: {songs.length}</span>

                            <div>
                                <button className="button" onClick={previousSongsPage}>
                                    <span><i className="fas fa-caret-left"></i></span>
                                </button>
                                <span style={{"margin-left":"5px"}}>
                                    page {songsPage} of {songs.length%5 === 0 ? Math.floor(songs.length/5) : Math.floor(songs.length/5) + 1}
                                </span>
                                <button className="button" onClick={nextSongsPage}>
                                    <span><i className="fas fa-caret-right"></i></span>
                                </button>
                            </div>
                        </div>
                        </>
                    }

                    {albums.length > 0 &&
                        <>
                        <div className="section-desc">
                            <div className="l b"></div>
                            <span>Albums</span>
                            <div className="l a"></div>
                        </div>
                                  
                        <div className="album-card-container">
                            <AlbumRow ind={albumsPage - 1} albums={albums}/>
                        </div>

                        <div style={{"font-size":"medium", "margin-top":"10px"}}>
                            <span>results: {albums.length}</span>

                            <div>
                                <button className="button" onClick={previousAlbumsPage}>
                                    <span><i className="fas fa-caret-left"></i></span>
                                </button>
                                <span style={{"margin-left":"5px"}}>
                                    page {albumsPage} of {albums.length%4 === 0 ? Math.floor(albums.length/4) : Math.floor(albums.length/4) + 1}
                                </span>
                                <button className="button" onClick={nextAlbumsPage}>
                                    <span><i className="fas fa-caret-right"></i></span>
                                </button>
                            </div>
                        </div>
                        </>
                    }

                    {artists.length > 0 &&
                        <>
                        <div className="section-desc">
                            <div className="l b"></div>
                            <span>Artists</span>
                            <div className="l a"></div>
                        </div>
                            
                        <div className="album-card-container">
                            <ArtistRow ind={artistsPage - 1} artists={artists}/>
                        </div>

                        <div style={{"font-size":"medium", "margin-top":"10px"}}>
                            <span>results: {artists.length}</span>

                            <div>
                                <button className="button" onClick={previousArtistsPage}>
                                    <span><i className="fas fa-caret-left"></i></span>
                                </button>
                                <span style={{"margin-left":"5px"}}>
                                    page {artistsPage} of {artists.length%4 === 0 ? Math.floor(artists.length/4) : Math.floor(artists.length/4) + 1}
                                </span>
                                <button className="button" onClick={nextArtistsPage}>
                                    <span><i className="fas fa-caret-right"></i></span>
                                </button>
                            </div>
                        </div>
                        </>
                    }

                    {profiles.length > 0 &&
                        <>
                        <div className="section-desc">
                            <div className="l b"></div>
                            <span>Profiles</span>
                            <div className="l a"></div>
                        </div>
                                 
                        <div className="album-card-container">
                            <ArtistRow ind={profilesPage - 1} artists={profiles}/>
                        </div>

                        <div style={{"font-size":"medium", "margin-top":"10px"}}>
                            <span>results: {profiles.length}</span>

                            <div>
                                <button className="button" onClick={previousProfilesPage}>
                                    <span><i className="fas fa-caret-left"></i></span>
                                </button>
                                <span style={{"margin-left":"5px"}}>
                                    page {profilesPage} of {profiles.length%4 === 0 ? Math.floor(profiles.length/4) : Math.floor(profiles.length/4) + 1}
                                </span>
                                <button className="button" onClick={nextProfilesPage}>
                                    <span><i className="fas fa-caret-right"></i></span>
                                </button>
                            </div>
                        </div>
                        </>
                    }

                    {playlists.length > 0 &&
                        <>
                        <div className="section-desc">
                            <div className="l b"></div>
                            <span>Playlists</span>
                            <div className="l a"></div>
                        </div>
                                 
                        <div className="album-card-container">
                            {renderPlaylists.map(playlist => {
                                return <PlaylistRow key={playlist} playlist={playlist} setShowModal={setShowModal} />
                            })}
                        </div>

                        <div style={{"font-size":"medium", "margin-top":"10px"}}>
                            <span>results: {playlists.length}</span>

                            <div>
                                <button className="button" onClick={previousPlaylistsPage}>
                                    <span><i className="fas fa-caret-left"></i></span>
                                </button>
                                <span style={{"margin-left":"5px"}}>
                                    page {playlistsPage} of {playlists.length%5 === 0 ? Math.floor(playlists.length/5) : Math.floor(playlists.length/5) + 1}
                                </span>
                                <button className="button" onClick={nextPlaylistsPage}>
                                    <span><i className="fas fa-caret-right"></i></span>
                                </button>
                            </div>
                        </div>
                        </>
                    }
            </div>
            </>
        );
}
  
export default Search;