import React, { useState } from 'react';

import '../../Styles/Modal.css';

function EditComment(props) {
    const [content, setContent] = useState(props.model.content);

    const setShow = props.setShowModal;

    let handleSubmit = e => {
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                comment_ID: props.model.comment_ID,
                content: content
            })
        };

        fetch('http://localhost:8080/api/v1/comment/update', requestOptions)
            .then(response => response.json())
            .then(data => {
                if (data.message)
                    alert(data.message);
            });

        setShow(false);
        props.update(true);
    }

    return (
        <>
            <div className="modal-title">
                Edit comment
            </div>
            <form className="form-modal">
                <div className="form-row">
                    <span>Content:</span>
                    <input type="text" value={content} 
                            onChange={e => setContent(e.target.value)} />
                </div>
                <div className="form-buttons">
                    <input type="button" className="button" value="Submit" onClick={handleSubmit} />
                    <input type="button" className="button" value="Cancel" onClick={() => setShow(false)} />
                </div>
            </form>
        </>
    );
}

export default EditComment;