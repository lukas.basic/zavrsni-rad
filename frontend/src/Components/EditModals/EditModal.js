import React from 'react';
import EditAccount from './EditAccount';
import EditAlbum from './EditAlbum';
import EditComment from './EditComment';
import EditGenre from './EditGenre';
import EditPlaylist from './EditPlaylist';
import EditSong from './EditSong';

import '../../Styles/Modal.css';

function EditModal(props) {
    return (
        <>
            {props.modal.type === "Account" && 
                <EditAccount model={props.modal.model} 
                            action={props.modal.action}
                            picture={props.modal.picture}
                            setShowModal={props.setShowModal} 
                            update={props.modal.update}/>
            }
            {props.modal.type === "Album" && 
                <EditAlbum model={props.modal.model}  
                            action={props.modal.action}
                            picture={props.modal.picture}
                            setShowModal={props.setShowModal} 
                            update={props.modal.update}/>
            }
            {props.modal.type === "Comment" && 
                <EditComment model={props.modal.model}  
                            action={props.modal.action}
                            setShowModal={props.setShowModal} 
                            update={props.modal.update}/>
            }
            {props.modal.type === "Genre" && 
                <EditGenre model={props.modal.model}  
                            action={props.modal.action}
                            setShowModal={props.setShowModal} 
                            update={props.modal.update}/>
            }
            {props.modal.type === "Playlist" && 
                <EditPlaylist model={props.modal.model} 
                            action={props.modal.action} 
                            setShowModal={props.setShowModal} 
                            update={props.modal.update}/>
            }
            {props.modal.type === "Song" && 
                <EditSong model={props.modal.model}  
                            action={props.modal.action}
                            setShowModal={props.setShowModal} 
                            update={props.modal.update}/>
            }
        </>
    );
}

export default EditModal;