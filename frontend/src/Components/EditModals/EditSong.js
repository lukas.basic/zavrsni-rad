import React, { useState, useEffect } from 'react';
import Autosuggest from 'react-autosuggest';

import '../../Styles/Modal.css';

function EditSong(props) {
    let identity = (x) => x;

    const [name, setName] = useState(props.model.name);
    const [audio, setAudio] = useState(props.model.audio);
    const [collaborations, setCollaborations] = useState(props.model.collaborations.map(identity));
    const [genres, setGenres] = useState(props.model.genres.map(identity));
    const [genreList, setGenreList] = useState([]);
    const [allGenres, setAllGenres] = useState([]);
    useEffect(() => {
        fetch('http://localhost:8080/api/v1/genre')
            .then(response => response.json())
            .then(data => setAllGenres(data));
    }, []);

    const album = props.model.album;

    const [authors, setAuthors] = useState(props.model.accounts.map(identity));
    const [notAuthors, setNotAuthors] = useState([]);
    useEffect(() => {
        let s;
        if (authors.length > 0)
            s = 'http://localhost:8080/api/v1/song/not-authors?song_ID=' + props.model.song_ID;
        else 
            s = 'http://localhost:8080/api/v1/account';

        fetch(s)
            .then(response => response.json())
            .then(data => setNotAuthors(data));

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const [newAuthor, setNewAuthor] = useState(false);
    const [newAuthorObj, setNewAuthorObj] = useState(null);
    const [newColl, setNewColl] = useState(false);
    const [newGenre, setNewGenre] = useState(false);

    const setShow = props.setShowModal;

    let handleSubmit = e => {
        if (props.action === "edit") {
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    song_ID: props.model.song_ID,
                    name: name,
                    audio: audio,
                    collaborations: collaborations,
                    genres: genres,
                    album: album,
                    accounts: authors,
                    album_order: props.model.album_order
                })
            };

            fetch('http://localhost:8080/api/v1/song/update', requestOptions)
                .then(response => response.json())
                .then(data => {
                    if (data.message)
                        alert(data.message);
                });
        } else {
            
        }

        setShow(false);
        props.update(true);
    }

    let addAuthor = () => {
        let newNotAuth = notAuthors.filter(a => a.account_ID !== newAuthorObj.account_ID);
        authors.push(newAuthorObj);
        setNotAuthors(newNotAuth);
        setAuthors(authors);
        setNewAuthor(false);
        setNewAuthorObj(null);
        setValue('');
    }

    let removeAuthor = a => {
        let newAuth = authors.filter(auth => auth.account_ID !== a.account_ID);
        notAuthors.push(a);
        setNotAuthors(notAuthors);
        setAuthors(newAuth);
    }

    let addCollaboration = () => {
        collaborations.push(newColl);
        setCollaborations(collaborations);
        setNewColl(false);
    }

    let removeCollaboration = c => {
        let newColl = [];
        collaborations.map(coll => {
            if (coll !== c)
                newColl.push(coll);
            return <></>;
        })
        setCollaborations(newColl);
    }

    let updateGenreList = () => {
        let tmp = [];

        allGenres.map(g => {
            let contains = false;

            genres.map(genre => {
                if (g.name === genre.name)
                    contains = true;
                return <></>;
            });

            if (!contains)
                tmp.push(g)
            
            return <></>;
        }, setGenreList(tmp));
    }

    let addGenre = () => {
        genres.push(newGenre);
        setGenres(genres);
        updateGenreList();
        setNewGenre(false);
    }

    let removeGenre = g => {
        let gArr = [];
        genres.map(genre => {
            if (genre !== g)
                gArr.push(genre);
            return <></>;
        })
        setGenres(gArr);
        updateGenreList();
    }

    let setDefaultGenre = g => {
        if (g === undefined) {
            let init = false;
            allGenres.map(genre => {
                if (!init) {
                    genres.map(gg => {
                        if (!init && gg.name !== genre.name) {
                            g = genre;
                            init = true;
                            return <></>;
                        }
                        return <></>;
                    })
                }
                return <></>;
            })
        }
        setNewGenre(g);
    }

    // author autosuggest helpers start
    let escapeRegexCharacters = (str) => {
        return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
    }
    
    let getSuggestions = (value) => {
        const escapedValue = escapeRegexCharacters(value.trim());
        
        if (escapedValue === '') {
            return [];
        }
        
        const regex = new RegExp('^' + escapedValue, 'i');
        
        return notAuthors.filter(author => regex.test(author.username) || regex.test(author.stage_name));
    }
    
    let getSuggestionValue = (suggestion) => {
        setNewAuthorObj(suggestion);
        return suggestion.username + ' (' + suggestion.stage_name + ')';
    }
    
    let renderSuggestion = (suggestion) => {
        return (
            <span style={{"cursor": "pointer"}}>{suggestion.username} ({suggestion.stage_name})</span>
        );
    }

    const [value, setValue] = useState('');
    const [suggestions, setSuggestions] = useState([]);

    let onChange = (event, { newValue, method }) => {
        setValue(newValue);
    };
      
    let onSuggestionsFetchRequested = ({ value }) => {
        setSuggestions(getSuggestions(value));
    };

    let onSuggestionsClearRequested = () => {
        setSuggestions([]);
    };

    const inputProps = {
        placeholder: "Enter username or stage name",
        value,
        onChange: onChange
    };
    // author autosuggest helpers end

    return (
        <>
            {props.action === "add" &&
                <div className="modal-title">
                    Upload song
                </div>
            }
            {props.action === "edit" &&
                <div className="modal-title">
                    Edit song {props.model.name}
                </div>
            }
            <form autocomplete="off" className="form-modal">
                <div className="form-row" onClick={() => console.log(props.action)}>
                    <span>Name:</span>
                    <input type="text" value={name} 
                            onChange={e => setName(e.target.value)} />
                </div>
                <div className="form-row">
                    <span>Audio:</span>
                    <input type="text" value={audio} 
                            onChange={e => setAudio(e.target.value)} />
                </div>
                <div className="line"></div>
                <div>
                    <span>Authors</span>
                </div>
                {authors.length === 0 && !newAuthor &&
                    <div className="form-row">
                        <input type="button" className="button" value="Add" onClick={() => setNewAuthor(true)} />
                    </div>
                }
                {authors.length === 0 && newAuthor &&
                    <div className="form-row">
                        <Autosuggest 
                            suggestions={suggestions}
                            onSuggestionsFetchRequested={onSuggestionsFetchRequested}
                            onSuggestionsClearRequested={onSuggestionsClearRequested}
                            getSuggestionValue={getSuggestionValue}
                            renderSuggestion={renderSuggestion}
                            inputProps={inputProps} />
                        {newAuthorObj === null &&
                            <input type="button" className="button" value="Confirm" onClick={addAuthor} disabled/>
                        }
                        {newAuthorObj &&
                            <input type="button" className="button" value="Confirm" onClick={addAuthor} />
                        }
                        <input type="button" className="button" value="Cancel" onClick={() => {setNewAuthor(false); setValue('');}} /> 
                    </div>
                }
                {authors.length > 0 &&
                    <>
                        {authors.map(a => {
                            return <div className="form-row">
                                        <span>{a.username} ({a.stage_name})</span>
                                        {authors.length === 1 &&
                                            <input type="button" className="button" value="Remove" onClick={() => removeAuthor(a)} disabled/>
                                        }
                                        {authors.length > 1 &&
                                            <input type="button" className="button" value="Remove" onClick={() => removeAuthor(a)} />
                                        }
                                    </div>
                        })}
                        {!newAuthor &&
                            <div className="form-row">
                                <input type="button" className="button" value="Add author" onClick={() => setNewAuthor(true)} />
                            </div>
                        }
                        {newAuthor &&
                            <div className="form-row">
                                <Autosuggest 
                                    suggestions={suggestions}
                                    onSuggestionsFetchRequested={onSuggestionsFetchRequested}
                                    onSuggestionsClearRequested={onSuggestionsClearRequested}
                                    getSuggestionValue={getSuggestionValue}
                                    renderSuggestion={renderSuggestion}
                                    inputProps={inputProps} />
                                {newAuthorObj === null &&
                                    <input type="button" className="button" value="Confirm" onClick={addAuthor} disabled/>
                                }
                                {newAuthorObj !== null &&
                                    <input type="button" className="button" value="Confirm" onClick={addAuthor} />
                                }
                                <input type="button" className="button" value="Cancel" onClick={() => {setNewAuthor(false); setValue('');}} /> 
                            </div>
                        }
                    </>
                }
                <div className="line"></div>
                <div>
                    <span>Collaborations</span>
                </div>
                {collaborations.length === 0 && newColl === false &&
                    <div className="form-row">
                        <input type="button" className="button" value="Add" onClick={() => setNewColl("")} />
                    </div>
                }
                {collaborations.length === 0 && newColl !== false &&
                    <div className="form-row">
                        <input type="text" value={newColl} onChange={e => setNewColl(e.target.value)} />
                        <input type="button" className="button" value="Confirm" onClick={addCollaboration} /> 
                        <input type="button" className="button" value="Cancel" onClick={() => setNewColl(false)} /> 
                    </div>
                }
                {collaborations.length > 0 &&
                    <>
                        {collaborations.map(c => {
                            return <div className="form-row">
                                        <span>{c}</span>
                                        <input type="button" className="button" value="Remove" onClick={() => removeCollaboration(c)} />
                                    </div>
                        })}
                        {newColl === false &&
                            <div className="form-row">
                                <input type="button" className="button" value="Add collaboration" onClick={() => setNewColl("")} />
                            </div>
                        }
                        {newColl !== false &&
                            <div className="form-row">
                                <input type="text" value={newColl} onChange={e => setNewColl(e.target.value)} />
                                <input type="button" className="button" value="Confirm" onClick={addCollaboration} /> 
                                <input type="button" className="button" value="Cancel" onClick={() => setNewColl(false)} /> 
                            </div>
                        }
                    </>
                }
                <div className="line"></div>
                <div>
                    <span>Genres</span>
                </div>
                {genres.length === 0 && newGenre === false &&
                    <div className="form-row">
                        <input type="button" className="button" value="Add" onClick={() => {updateGenreList(); setDefaultGenre(genreList[0]);}} />
                    </div>
                }
                {genres.length === 0 && newGenre !== false &&
                    <div className="form-row">
                        <select defaultValue={genreList[0]} onChange={e => setNewGenre(e.target.value)}>
                            {genreList.map(g => {
                                return <option value={g}>{g.name}</option>
                            })}
                        </select>
                        <input type="button" className="button" value="Confirm" onClick={addGenre} /> 
                        <input type="button" className="button" value="Cancel" onClick={() => setNewGenre(false)} /> 
                    </div>
                }
                {genres.length > 0 &&
                    <>
                        {genres.map(c => {
                            return <div className="form-row">
                                        <span>{c.name}</span>
                                        <input type="button" className="button" value="Remove" onClick={() => removeGenre(c)} />
                                    </div>
                        })}
                        {newGenre === false &&
                            <div className="form-row">
                                <input type="button" className="button" value="Add genre" onClick={() => {updateGenreList(); setDefaultGenre(genreList[0]);}} />
                            </div>
                        }
                        {newGenre !== false &&
                            <div className="form-row">
                                <select defaultValue={genreList[0]} onChange={e => setNewGenre(e.target.value)}>
                                    {genreList.map(g => {
                                        return <option value={g}>{g.name}</option>
                                    })}
                                </select>
                                <input type="button" className="button" value="Confirm" onClick={addGenre} /> 
                                <input type="button" className="button" value="Cancel" onClick={() => setNewGenre(false)} /> 
                            </div>
                        }
                    </>
                }
                <div className="form-buttons">
                    <input type="button" className="button" value="Submit" onClick={handleSubmit} />
                    <input type="button" className="button" value="Cancel" onClick={() => setShow(false)} />
                </div>
            </form>
        </>
    );
}

export default EditSong;