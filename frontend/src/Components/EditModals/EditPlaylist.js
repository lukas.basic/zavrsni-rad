import React, { useState } from 'react';

import '../../Styles/Modal.css';

function EditPlaylist(props) {
    const [name, setName] = useState(props.model.name);
    const [publicAccess, setPublicAccess] = useState(props.model.public_access);

    const setShow = props.setShowModal;

    let handleSubmit = e => {
        if (props.action === "edit") {
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    playlist_ID: props.model.playlist_ID,
                    name: name,
                    public_access: publicAccess
                })
            };

            fetch('http://localhost:8080/api/v1/playlist/update', requestOptions)
                .then(response => response.json())
                .then(data => {
                    if (data.message)
                        alert(data.message);
                });
        } else if (props.action === "add") {
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    account_ID: props.model.account_ID,
                    name: name,
                    public_access: publicAccess
                })
            };

            fetch('http://localhost:8080/api/v1/playlist', requestOptions)
                .then(response => response.json())
                .then(data => {
                    if (data.message)
                        alert(data.message);
                });
        }

        setShow(false);
        //props.update();
    }

    return (
        <>
            {props.action === "add" &&
                <div className="modal-title">
                    Create new playlist
                </div>
            }
            {props.action === "edit" &&
                <div className="modal-title">
                    Edit playlist {props.model.name}
                </div>
            }
            <form className="form-modal">
                <div className="form-row">
                    <span>Name:</span>
                    <input type="text" value={name} 
                            onChange={e => setName(e.target.value)} />
                </div>
                <div className="form-row">
                    <span>Public playlist:</span>
                    <input type="checkbox" checked={publicAccess} 
                            onClick={e => setPublicAccess(!publicAccess)} />
                </div>
                <div className="form-buttons">
                    {name !== "" &&
                        <input type="button" className="button" value="Submit" onClick={handleSubmit} />
                    }
                    {name === "" &&
                        <input type="button" className="button" value="Submit" disabled/>
                    }
                    <input type="button" className="button" value="Cancel" onClick={() => setShow(false)} />
                </div>
            </form>
        </>
    );
}

export default EditPlaylist;