import React, { useState, useEffect } from 'react';
import ImageUploader from "react-images-upload";
import Autosuggest from 'react-autosuggest';

import '../../Styles/Modal.css';

function EditAlbum(props) {
    let identity = (x) => x;

    const [name, setName] = useState(props.model.name);
    const currentPicture = props.picture;

    const [authors, setAuthors] = useState(props.model.accounts.map(identity));
    const [notAuthors, setNotAuthors] = useState([]);
    useEffect(() => {
        let s;
        if (authors.length > 0)
            s = 'http://localhost:8080/api/v1/album/not-authors?album_ID=' + props.model.album_ID;
        else 
            s = 'http://localhost:8080/api/v1/account';

        fetch(s)
            .then(response => response.json())
            .then(data => setNotAuthors(data));

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const [newAuthor, setNewAuthor] = useState(false);
    const [newAuthorObj, setNewAuthorObj] = useState(null);

    let addAuthor = () => {
        let newNotAuth = notAuthors.filter(a => a.account_ID !== newAuthorObj.account_ID);
        authors.push(newAuthorObj);
        setNotAuthors(newNotAuth);
        setAuthors(authors);
        setNewAuthor(false);
        setNewAuthorObj(null);
        setValue('');
    }

    let removeAuthor = a => {
        let newAuth = authors.filter(auth => auth.account_ID !== a.account_ID);
        notAuthors.push(a);
        setNotAuthors(notAuthors);
        setAuthors(newAuth);
    }

    const [newPicture, setNewPicture] = useState(false);
    const [imageData, setImageData] = useState([]);
    const [changePic, setChangePic] = useState(props.action === "add");

    const setShow = props.setShowModal;

    let handleSubmit = e => {
        if (props.action === "edit") {
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    album_ID: props.model.album_ID,
                    name: name,
                    accounts: authors
                })
            };

            fetch('http://localhost:8080/api/v1/album/update', requestOptions)
                .then(response => response.json())
                .then(data => {
                    if (data.message)
                        alert(data.message);
                });

                if (newPicture && changePic) {
                    imageData.append('album_ID', props.model.album_ID);
                    const requestOptionsImage = {
                        method: 'POST',
                        body: imageData
                    };
        
                    if (imageData.entries().next().value[1] !== null) {
                        fetch('http://localhost:8080/api/v1/album/image', requestOptionsImage)
                            .then(response => response.json())
                            .then(data => {
                                if (data.message)
                                    alert(data.message);
                            });
                    }
                }
        } else if (props.action === "add") {
            let albumData = new FormData();
            let address = 'http://localhost:8080/api/v1/album/default-picture';
            if (newPicture) {
                albumData = imageData;
                address = 'http://localhost:8080/api/v1/album';
            }

            albumData.append('name', name);
            albumData.append('account_id', authors.map(a => a.account_ID));

            const requestOptions = {
                method: 'POST',
                body: albumData
            };

            fetch(address, requestOptions)
                .then(response => response.json())
                .then(data => {
                    if (data.message)
                        alert(data.message);
                })
                .catch(() => {
                    props.update(true);
                });
        }

        
        setShow(false);
        props.update(true);
    }

    let onDrop = (picture) => {
        setNewPicture(true);
        let file = picture[0];
        const imageData = new FormData();
        imageData.append('image', file);
        setImageData(imageData);
    } 

    // author autosuggest helpers start
    let escapeRegexCharacters = (str) => {
        return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
    }
    
    let getSuggestions = (value) => {
        const escapedValue = escapeRegexCharacters(value.trim());
        
        if (escapedValue === '') {
            return [];
        }
        
        const regex = new RegExp('^' + escapedValue, 'i');
        
        return notAuthors.filter(author => regex.test(author.username) || regex.test(author.stage_name));
    }
    
    let getSuggestionValue = (suggestion) => {
        setNewAuthorObj(suggestion);
        return suggestion.username + ' (' + suggestion.stage_name + ')';
    }
    
    let renderSuggestion = (suggestion) => {
        return (
            <span style={{"cursor": "pointer"}}>{suggestion.username} ({suggestion.stage_name})</span>
        );
    }

    const [value, setValue] = useState('');
    const [suggestions, setSuggestions] = useState([]);

    let onChange = (event, { newValue, method }) => {
        setValue(newValue);
    };
      
    let onSuggestionsFetchRequested = ({ value }) => {
        setSuggestions(getSuggestions(value));
    };

    let onSuggestionsClearRequested = () => {
        setSuggestions([]);
    };

    const inputProps = {
        placeholder: "Enter username or stage name",
        value,
        onChange: onChange
    };
    // author autosuggest helpers end

    return (
        <>
            {props.action === "add" &&
                <div className="modal-title">
                    Create new album
                </div>
            }
            {props.action === "edit" &&
                <div className="modal-title">
                    Edit album {props.model.name}
                </div>
            }
            <form autocomplete="off" className="form-modal">
                {!changePic &&
                    <>
                        <div className="form-row picture-row">
                            <img className="picture"
                                src={currentPicture}
                                alt="album_pic"
                            />
                        </div>
                        <div className="form-row">
                            <input type="button"
                                    className="button"
                                    value="Change picture" 
                                    onClick={() => setChangePic(true)} 
                            />
                        </div>
                    </>
                }
                {changePic &&
                    <>
                        <div className="form-row">
                            <ImageUploader
                                singleImage={true}
                                withIcon={true}
                                withLabel={false}
                                withPreview={true}
                                buttonText="Choose album picture"
                                onChange={onDrop}
                                imgExtension={[".jpg", ".png", ".jpeg"]}
                                maxFileSize={5242880}
                            />
                        </div>
                        {props.action === "edit" &&
                            <div className="form-row">
                                <input type="button" 
                                    className="button"
                                    value="Cancel" 
                                    onClick={() => setChangePic(false)} 
                                />
                            </div>
                        }
                    </>
                }

                <div className="form-row">
                    <span>Name:</span>
                    <input type="text" value={name} 
                            onChange={e => setName(e.target.value)} />
                </div>
                <div className="line"></div>
                <div>
                    <span>Authors</span>
                </div>
                {authors.length === 0 && !newAuthor &&
                    <div className="form-row">
                        <input type="button" className="button" value="Add" onClick={() => setNewAuthor(true)} />
                    </div>
                }
                {authors.length === 0 && newAuthor &&
                    <div className="form-row">
                        <Autosuggest 
                            suggestions={suggestions}
                            onSuggestionsFetchRequested={onSuggestionsFetchRequested}
                            onSuggestionsClearRequested={onSuggestionsClearRequested}
                            getSuggestionValue={getSuggestionValue}
                            renderSuggestion={renderSuggestion}
                            inputProps={inputProps} />
                        {newAuthorObj === null &&
                            <input type="button" className="button" value="Confirm" onClick={addAuthor} disabled/>
                        }
                        {newAuthorObj &&
                            <input type="button" className="button" value="Confirm" onClick={addAuthor} />
                        }
                        <input type="button" className="button" value="Cancel" onClick={() => {setNewAuthor(false); setValue('');}} /> 
                    </div>
                }
                {authors.length > 0 &&
                    <>
                        {authors.map(a => {
                            return <div className="form-row">
                                        <span>{a.username} ({a.stage_name})</span>
                                        {authors.length === 1 &&
                                            <input type="button" className="button" value="Remove" onClick={() => removeAuthor(a)} disabled/>
                                        }
                                        {authors.length > 1 &&
                                            <input type="button" className="button" value="Remove" onClick={() => removeAuthor(a)} />
                                        }
                                    </div>
                        })}
                        {!newAuthor &&
                            <div className="form-row">
                                <input type="button" className="button" value="Add author" onClick={() => setNewAuthor(true)} />
                            </div>
                        }
                        {newAuthor &&
                            <div className="form-row">
                                <Autosuggest 
                                    suggestions={suggestions}
                                    onSuggestionsFetchRequested={onSuggestionsFetchRequested}
                                    onSuggestionsClearRequested={onSuggestionsClearRequested}
                                    getSuggestionValue={getSuggestionValue}
                                    renderSuggestion={renderSuggestion}
                                    inputProps={inputProps} />
                                {newAuthorObj === null &&
                                    <input type="button" className="button" value="Confirm" onClick={addAuthor} disabled/>
                                }
                                {newAuthorObj !== null &&
                                    <input type="button" className="button" value="Confirm" onClick={addAuthor} />
                                }
                                <input type="button" className="button" value="Cancel" onClick={() => {setNewAuthor(false); setValue('');}} /> 
                            </div>
                        }
                    </>
                }
                <div className="form-buttons">
                    {name !== "" && authors.length !== 0 &&
                        <input type="button" className="button" value="Submit" onClick={handleSubmit} />
                    }
                    {(name === "" || authors.length === 0) &&
                        <input type="button" className="button" value="Submit" disabled/>
                    }
                    <input type="button" className="button" value="Cancel" onClick={() => setShow(false)} />
                </div>
            </form>
        </>
    );
}

export default EditAlbum;