import React, { useState } from 'react';

import '../../Styles/Modal.css';

function EditGenre(props) {
    const [name, setName] = useState(props.model.name);

    const setShow = props.setShowModal;

    let handleSubmit = e => {
        if (props.action === "edit") {
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    genre_ID: props.model.genre_ID,
                    name: name
                })
            };

            fetch('http://localhost:8080/api/v1/genre/update', requestOptions)
                .then(response => response.json())
                .then(data => {
                    if (data.message)
                        alert(data.message);
                });
        } else if (props.action === "add") {
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    name: name
                })
            };

            fetch('http://localhost:8080/api/v1/genre', requestOptions)
                .then(response => response.json())
                .then(data => {
                    if (data.message)
                        alert(data.message);
                });
        }

        setShow(false);
        props.update(true);
    }

    return (
        <>
            {props.action === "add" &&
                <div className="modal-title">
                    Create new genre
                </div>
            }
            {props.action === "edit" &&
                <div className="modal-title">
                    Edit genre {props.model.name}
                </div>
            }
            <form className="form-modal">
                <div className="form-row">
                    <span>Name:</span>
                    <input type="text" value={name} 
                            onChange={e => setName(e.target.value)} />
                </div>
                <div className="form-buttons">
                    {name !== "" &&
                        <input type="button" className="button" value="Submit" onClick={handleSubmit} />
                    }
                    {name === "" &&
                        <input type="button" className="button" value="Submit" disabled/>
                    }
                    <input type="button" className="button" value="Cancel" onClick={() => setShow(false)} />
                </div>
            </form>
        </>
    );
}

export default EditGenre;