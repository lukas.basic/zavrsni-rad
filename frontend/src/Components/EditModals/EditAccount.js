import React, { useState } from 'react';
import ImageUploader from "react-images-upload";

import '../../Styles/Modal.css';

function EditAccount(props) {
    const [username, setUsername] = useState(props.model.username);
    const [email, setEmail] = useState(props.model.email);
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");


    const [stageName, setStageName] = useState(props.model.stage_name);
    const currentPicture = props.picture;

    const [appearance, setAppearance] = useState(props.model.appearance);

    const [newPicture, setNewPicture] = useState(false);
    const [imageData, setImageData] = useState([]);
    const [changePic, setChangePic] = useState(props.action === "add");

    const setShow = props.setShowModal;

    let handleSubmit = e => {
        if (props.action === "edit") {
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    account_ID: props.model.account_ID,
                    username: props.model.username,
                    stage_name: stageName,
                    appearance: appearance
                })
            };

            if (newPicture && changePic) {
                imageData.append('account_ID', props.model.account_ID);
                const requestOptionsImage = {
                    method: 'POST',
                    body: imageData
                };

                if (imageData.entries().next().value[1] !== null) {
                    fetch('http://localhost:8080/api/v1/account/image', requestOptionsImage)
                        .then(response => response.json())
                        .then(data => {
                            if (data.message)
                                alert(data.message);
                        });
                }
            }

            fetch('http://localhost:8080/api/v1/account/update', requestOptions)
                .then(response => response.json())
                .then(data => {
                    if (data.message)
                        alert(data.message);
                    else 
                        props.update(true);
                });
        } else if (props.action === "add") {
            let accData = new FormData();
            let address = 'http://localhost:8080/api/v1/account/default-picture';
            if (newPicture) {
                accData = imageData;
                address = 'http://localhost:8080/api/v1/account';
            }

            accData.append('username', username);
            accData.append('stage_name', stageName);
            accData.append('email', email);
            accData.append('password', password);
            accData.append('appearance', appearance);
            const requestOptions = {
                method: 'POST',
                body: accData
            };

            fetch(address, requestOptions)
                .then(response => response.json())
                .then(data => {
                    if (data.message)
                        alert(data.message);
                    else 
                        props.update(true);
                });
        }

        setShow(false);
        props.update(true);
    }

    let onDrop = (picture) => {
        setNewPicture(true);
        let file = picture[0];
        const imageData = new FormData();
        imageData.append('image', file);
        setImageData(imageData);
    }    

    return (
        <>
            {props.action === "add" &&
                <div className="modal-title">
                    Registration
                </div>
            }
            {props.action === "edit" &&
                <div className="modal-title">
                    Edit account {props.model.username}
                </div>
            }
            <form className="form-modal">
                {!changePic &&
                    <>
                        <div className="form-row picture-row">
                            <img className="picture"
                                src={currentPicture}
                                alt="profile_pic"
                            />
                        </div>
                        <div className="form-row">
                            <input type="button" 
                                    className="button"
                                    value="Change picture" 
                                    onClick={() => setChangePic(true)} 
                            />
                        </div>
                    </>
                }
                {changePic &&
                    <>
                        <div className="form-row">
                            <ImageUploader
                                singleImage={true}
                                withIcon={true}
                                withLabel={false}
                                withPreview={true}
                                buttonText="Choose profile picture"
                                onChange={onDrop}
                                imgExtension={[".jpg", ".png", ".jpeg"]}
                                maxFileSize={5242880}
                            />
                        </div>
                        {props.action === "edit" &&
                            <div className="form-row">
                                <input type="button" 
                                    className="button"
                                    value="Cancel" 
                                    onClick={() => setChangePic(false)} 
                                />
                            </div>
                        }
                    </>
                }

                {props.action === "add" &&
                <>
                    <div className="form-row">
                        <span>Username:</span>
                        <input type="text" value={username} 
                                onChange={e => setUsername(e.target.value)} />
                    </div>
                    <div className="form-row">
                        <span>E-mail:</span>
                        <input type="text" value={email} 
                                onChange={e => setEmail(e.target.value)} />
                    </div>
                    <div className="form-row">
                        <span>Password:</span>
                        <input type="password" value={password} 
                                onChange={e => setPassword(e.target.value)} />
                    </div>
                    <div className="form-row">
                        <span>Confirm password:</span>
                        <input type="password" value={confirmPassword} 
                                onChange={e => setConfirmPassword(e.target.value)} />
                    </div>
                </>
                }

                <div className="form-row">
                    <span>Stage name:</span>
                    <input type="text" value={stageName} 
                            onChange={e => setStageName(e.target.value)} />
                </div>
                <div className="form-row">
                    <span>Use your stage name as display name?</span>
                    <input type="checkbox" defaultChecked={appearance} 
                            onChange={e => setAppearance(!appearance)} />
                </div>
                
                <div className="form-buttons">
                    {password === confirmPassword &&
                        <input type="button" className="button" value="Submit" onClick={handleSubmit} />
                    }
                    {password !== confirmPassword &&
                        <input type="button" className="button" value="Submit" disabled/>
                    }
                    <input type="button" className="button" value="Cancel" onClick={() => setShow(false)} />
                </div>
            </form>
        </>
    );
}

export default EditAccount;