import React, { useState, useEffect } from 'react';
import PlaylistRow from './PlaylistRow';
import Loader from './Loader';
import Modal from '../Components/Modal';

import {logoutUser} from '../Services/index';
import {connect} from 'react-redux';

function MyPlaylists(props) {
    const [showModal, setShowModal] = useState(false);
    const [update, setUpdate] = useState(0);

    let updateMe = () => {
        setUpdate(update + 1);
    }

    let newPlaylist = () => {
        setShowModal({type: "Playlist", 
                    action: "add",
                    model: {name: "",
                            public_access: false,
                            account_ID: props.auth.isLoggedIn.account_ID},
                    update: updateMe})
    }

    const [loader, setLoader] = useState(true);
    const [playlists, setPlaylists] = useState([]);
    useEffect(() => {
        fetch('http://localhost:8080/api/v1/account/my-playlists?account_ID=' + props.auth.isLoggedIn.account_ID)
            .then(response => response.json())
            .then(data => {
                setPlaylists(data);
                setLoader(false);
            });
    });

    if (loader)
        return <Loader />;
    else
        return (
            <>
            {showModal &&
                <Modal modal={showModal} setShowModal={setShowModal} setUpdate={updateMe} />
            }
            <div className="account-header-container">
                {playlists.length > 0 &&
                    <div className="section-desc">
                        <div className="l b"></div>
                        <span>My playlists</span>
                        <div className="l a"></div>
                    </div>
                }                
                <div className="album-card-container">
                    <div className="new-playlist">
                        <button className="button" onClick={newPlaylist}>
                            New playlist
                        </button>
                    </div>
                    {playlists.map(playlist => {
                        return <PlaylistRow key={playlist} playlist={playlist} setShowModal={setShowModal} />
                    })}
                </div>
            </div>
            </>
        );
}

const mapStateToProps = state => {
    return {
        auth: state.auth
    };
  };
  
  const mapDispatchToProps = dispatch => {
    return {
        logoutUser: () => dispatch(logoutUser())
    };
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(MyPlaylists);