import React, { useState } from 'react';
import ImageUploader from "react-images-upload";
import Loader from './Loader';

function Register() {
    const [loaderCaption, setLoaderCaption] = useState("");

    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [stageName, setStageName] = useState("");
    const [appearance, setAppearance] = useState(false);
    const [newPicture, setNewPicture] = useState(false);
    const [imageData, setImageData] = useState([]);
    
    let reset = () => {
        setLoaderCaption("");
        setUsername("");
        setEmail("");
        setPassword("");
        setConfirmPassword("");
        setStageName("");
        setAppearance(false);
        setNewPicture(false);
        setImageData([]);
    }

    let handleSubmit = e => {
        setLoaderCaption("Registering...");

        let accData = new FormData();
        let address = 'http://localhost:8080/api/v1/account/default-picture';
        if (newPicture) {
            accData = imageData;
            address = 'http://localhost:8080/api/v1/account';
        }

        accData.append('username', username);
        accData.append('stage_name', stageName);
        accData.append('email', email);
        accData.append('password', password);
        accData.append('appearance', appearance);
        const requestOptions = {
            method: 'POST',
            body: accData
        };

        fetch(address, requestOptions)
            .then(response => response.json())
            .then(data => {
                if (data.message)
                    setLoaderCaption("Failure! " + data.message + " Try again!");
            })
            .catch(() => {
                setLoaderCaption("Success! You can now login!");
            });
    }

    let onDrop = (picture) => {
        setNewPicture(true);
        let file = picture[0];
        const imageData = new FormData();
        imageData.append('image', file);
        setImageData(imageData);
    }    

    let validate = () => {
        return username !== "" &&
                email !== "" &&
                password.length >= 8 &&
                password === confirmPassword;
    }

    if (loaderCaption !== "")
        return <div>
            <Loader caption={loaderCaption} />
        </div>
    else
        return (
            <div className="register">
                <div className="modal-title">
                    Registration
                </div>
                
                <form className="form-modal">
                    
                    <div className="form-row">
                        <ImageUploader
                            singleImage={true}
                            withIcon={true}
                            withLabel={false}
                            withPreview={true}
                            buttonText="Choose profile picture"
                            onChange={onDrop}
                            imgExtension={[".jpg", ".png", ".jpeg"]}
                            maxFileSize={5242880}
                        />
                    </div>

                    <div className="form-row">
                        <span>Username:</span>
                        <input type="text" value={username} 
                                onChange={e => setUsername(e.target.value)} />
                    </div>
                    <div className="form-row">
                        <span>E-mail:</span>
                        <input type="text" value={email} 
                                onChange={e => setEmail(e.target.value)} />
                    </div>
                    <div className="form-row">
                        <span>Password:</span>
                        <input type="password" value={password} 
                                onChange={e => setPassword(e.target.value)} />
                    </div>
                    <div className="form-row">
                        <span>Confirm password:</span>
                        <input type="password" value={confirmPassword} 
                                onChange={e => setConfirmPassword(e.target.value)} />
                    </div>

                    <div className="form-row">
                        <span>Stage name:</span>
                        <input type="text" value={stageName} 
                                onChange={e => setStageName(e.target.value)} />
                    </div>
                    <div className="form-row">
                        <span>Use your stage name as display name?</span>
                        <input type="checkbox" defaultChecked={appearance} 
                                onChange={e => setAppearance(!appearance)} />
                    </div>
                    
                    <div className="form-buttons">
                        {validate() &&
                            <input type="button" className="button" value="Submit" onClick={handleSubmit} />
                        }
                        {!validate() &&
                            <input type="button" className="button" value="Submit" disabled/>
                        }
                        <input type="button" className="button" value="Reset" onClick={reset} />
                    </div>
                </form>
            </div>
        );
}

export default Register;