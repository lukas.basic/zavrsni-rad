import React, { Component } from 'react';
import {connect} from 'react-redux';
import {Link, Redirect} from 'react-router-dom';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faUserPlus, faSignInAlt} from '@fortawesome/free-solid-svg-icons';
import {logoutUser} from '../Services/index';
import logo from '../syla.svg';

import '../Styles/Header.css'

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            search: "",
            showSearch: false
        }
    }

    setSearch = v => {
        this.setState({search: v});
    }

    setIsOpen = () => {
        this.setState({
            isOpen: !this.state.isOpen
        })
    }

    logout = () => {
        this.props.logoutUser();
    };

    handleSubmit = e => {
        e.preventDefault();
        this.setState({redirect: true})
    };
    
    handleKeypress = e => { 
        if (this.state.search.length !== 0 && e.charCode === 13) {      
            this.handleSubmit(e);    
        }  
    };

    render() {
        const guestLinks = (
            <>
                <Link to={"/register"} className="nav-link"><FontAwesomeIcon icon={faUserPlus} /> Register</Link>
                <div className="link-space"></div>
                <Link to={"/login"} className="nav-link"><FontAwesomeIcon icon={faSignInAlt} /> Login</Link>
                <div className="link-space"></div>
            </>
        );

        const userLinks = (
            <>
                <div className="dropdown" onClick={this.setIsOpen}>
                    <div className="header-profile">
                        <div className="header-caret">
                            {this.state.isOpen &&
                                <span><i className="fas fa-caret-up"></i></span>
                            }
                            {!this.state.isOpen &&
                                <span><i className="fas fa-caret-down"></i></span>
                            }
                        </div>
                        <div className="header-username">
                            <span>{this.props.auth.isLoggedIn.username}</span>
                        </div>
                        <div className="header-picture">
                            <span><i className="fas fa-user-circle"></i></span>
                        </div>
                    </div>

                    {this.state.isOpen &&
                        <div className="dropdown-content">
                            <Link to={"/accounts/" + this.props.auth.isLoggedIn.username} className="nav-link"><div>Profile</div></Link>
                            {/* <Link to={"/statistics"} className="nav-link"><div>Statistics</div></Link> */}
                            <div onClick={this.logout}>Log out</div>
                        </div>
                    }
                </div>
            </>
        );

        return (
            <div className="header" onClick={() => this.setState({redirect: false})}>
                <Link to="/">
                    <div className="header-logo">
                        <img src={logo} className="App-logo-header" alt="logo" />
                    </div>
                </Link>
                <div className="header-search text-box">
                    <span onClick={() => this.setState({showSearch: !this.state.showSearch})}>
                        <i className="fas fa-search"></i>
                    </span>

                    {this.state.showSearch &&
                        <form onSubmit={this.handleSubmit}>
                            <input type="text" onKeyPress={this.handleKeypress}
                                    value={this.state.search} onChange={e => this.setSearch(e.target.value)}>
                            </input>
                            <input type="button" className="button" onClick={this.handleSubmit} value="Search" />
                        </form>
                    }
                </div>
                
                {this.props.auth.isLoggedIn.isLoggedIn ? userLinks : guestLinks}

                {this.state.redirect &&
                    <Redirect to={"/search/" + this.state.search} />
                }
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        auth: state.auth
    };
};

const mapDispatchToProps = dispatch => {
    return {
        logoutUser: () => dispatch(logoutUser())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
