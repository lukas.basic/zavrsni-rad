import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import {logoutUser} from './Services/index';
import {connect} from 'react-redux';
import Header from './Components/Header';
import AudioPlayer from './Components/AudioPlayer';
import Home from './Pages/Home';
import Admin from './Pages/Admin';
import RegisterPage from './Pages/RegisterPage';
import LoginPage from './Pages/LoginPage';
import Albums from './Pages/Albums';
import Accounts from './Pages/Accounts';
import Comments from './Pages/Comments';
import Playlists from './Pages/Playlists';
import FavouritesPage from './Pages/FavouritesPage';
import PublishPage from './Pages/PublishPage';
import StatsPage from './Pages/StatsPage';
import InfoPage from './Pages/InfoPage';
import SearchPage from './Pages/SearchPage';

import './App.css';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      trackList: null
    }
  }

  setTrackList = list => {
    this.setState({trackList: list});
  }

  AdminRoute = ({ component: Component, ...rest }) => {
    if (!(this.props.auth.isLoggedIn.isLoggedIn && this.props.auth.isLoggedIn.authorities === "ADMIN")) {
      return (<Route {...rest} render={(props) => ( <Redirect to='/' /> )} />);
    } else {
      return (<Route {...rest} render={(props) => ( <Component {...rest} /> )} />);
    }
  } 

  PrivateRoute = ({ component: Component, ...rest }) => {
    if (!this.props.auth.isLoggedIn) {
      return (<Route {...rest} render={(props) => ( <Redirect to='/' /> )} />);
    } else {
      return (<Route {...rest} render={(props) => ( <Component {...rest} /> )} />);
    }
  }
  
  LoggedInRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={(props) => (
      this.props.auth.isLoggedIn.isLoggedIn
        ? <Redirect to='/' />
        : <Component {...props} />
    )} />
  )

  LoggedOutRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={(props) => (
      this.props.auth.isLoggedIn.isLoggedIn
        ? <Component {...props} />
        : <Redirect to='/login' />
    )} />
  )
  
  render() {
    return (
      <div className="App">
        <Router>
          <Header />

          <Switch>
            <Route exact path="/" component={Home} />
            <this.AdminRoute path="/admin" component={Admin} />
            <this.PrivateRoute path="/statistics" component={StatsPage} />
            <this.LoggedInRoute path="/register" component={RegisterPage} />
            <this.LoggedInRoute path="/login" component={LoginPage} />
            <this.LoggedOutRoute path="/favourites" component={FavouritesPage} />
            <this.LoggedOutRoute path="/publish" component={PublishPage} />
            <Route path="/info" component={InfoPage} />
            <Route path="/albums"><Albums setTrackList={this.setTrackList} /></Route> 
            <Route path="/accounts" component={Accounts} />
            <Route path="/comments" component={Comments} />
            <Route path="/playlists"><Playlists setTrackList={this.setTrackList} /></Route>
            <Route path="/search"><SearchPage setTrackList={this.setTrackList} /></Route>
          </Switch>

          <AudioPlayer trackList={this.state.trackList} />
        </Router>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
      auth: state.auth
  };
};

const mapDispatchToProps = dispatch => {
  return {
      logoutUser: () => dispatch(logoutUser())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
