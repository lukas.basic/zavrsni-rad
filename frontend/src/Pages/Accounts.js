import React from "react";
import {
  Switch,
  Route,
  Redirect,
  useRouteMatch
} from "react-router-dom";
import Navigation from '../Components/Navigation';

import Account from '../Components/Account';

function Accounts() {
    let match = useRouteMatch();
  
    return (
        <>
            <Navigation />        
            <div className="App-header">
                <Switch>
                    <Route exact path={`${match.path}/:username`}>
                        <Account />
                    </Route>
                    <Route path={match.path}>
                        <Redirect to='/' />
                    </Route>
                </Switch>
            </div>
        </>
    );
}

export default Accounts;