import React from "react";
import {
  Switch,
  Route,
  Redirect,
  useRouteMatch
} from "react-router-dom";
import Navigation from '../Components/Navigation';
import Search from '../Components/Search';

function SearchPage(props) {
    let match = useRouteMatch();
  
    return (
        <>
            <Navigation />        
            <div className="App-header">
                <Switch>
                    <Route path={`${match.path}/:search`}>
                        <Search setTrackList={props.setTrackList} />
                    </Route>
                    <Route path={match.path}>
                        <Redirect to='/' />
                    </Route>
                </Switch>
            </div>
        </>
    );
}

export default SearchPage;