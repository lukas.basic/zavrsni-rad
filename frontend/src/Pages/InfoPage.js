import React, { Component } from 'react';
import Navigation from '../Components/Navigation';
import Loader from '../Components/Loader';

class InfoPage extends Component {
    render() {
        return (
        <>
            <Navigation />
            <div className="App-header">
                <Loader caption="Support Your Local Artists" />
            </div>
        </>
        );
    }
}

export default InfoPage;