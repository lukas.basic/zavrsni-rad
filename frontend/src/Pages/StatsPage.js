import React, { Component } from 'react';
import Navigation from '../Components/Navigation';

class StatsPage extends Component {
    render() {
        return (
        <>
            <Navigation />
            <div className="App-header">
                Your statistics
            </div>
        </>
        );
    }
}

export default StatsPage;