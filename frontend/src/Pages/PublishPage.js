import React, { Component } from 'react';
import {logoutUser} from '../Services/index';
import {connect} from 'react-redux';
import Navigation from '../Components/Navigation';
import PublishAlbum from '../Components/Publish/PublishAlbum';
import PublishSong from '../Components/Publish/PublishSong';
import Loader from '../Components/Loader';

const album = {
    name: '',
    imageData: null,
    accounts: []
}

const song = {
    name: '',
    audio: null,
    accounts: [],
    collaborations: [],
    genres: []
}

let elements = [album];
for (let i = 0; i < 10; i++) {
    elements.push(song);
}

class PublishPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            elements: elements.map(x => x),
            active: 0
        }
    }

    initialState = {
        elements: elements,
        active: 0,
        loader: false,
        loaderCaption: ""
    }

    song = {
        name: "song"
    }

    setAlbum = album => {
        let elements = this.state.elements;
        elements[0] = album;
        this.setState({elements: elements});
    }

    setAlbumName = data => {
        this.album = {
            name: data,
            imageData: this.state.elements[0].imageData,
            accounts: this.state.elements[0].accounts
        }
        this.setAlbum(this.album);
        if (this.state.elements[0].accounts.length === 0)
            this.setAlbumAccounts([this.props.auth.isLoggedIn]);
    }

    setImageData = data => {
        this.album = {
            name: this.state.elements[0].name,
            imageData: data,
            accounts: this.state.elements[0].accounts
        }
        this.setAlbum(this.album);
    }

    setAlbumAccounts = data => {
        this.album = {
            name: this.state.elements[0].name,
            imageData: this.state.elements[0].imageData,
            accounts: data
        }
        this.setAlbum(this.album);
    }

    validateAlbum = () => {
        return this.state.elements[0].name !== '';
    }

    validateSong = (index) => {
        return this.state.elements[index].name !== '' && 
                this.state.elements[index].audio !== null &&
                this.state.elements[index].genres.length !== 0;
    }

    validateAll = () => {
        if (this.state.active === 0)
            return false;

        let res = this.validateAlbum();

        if (!res)
            return res;

        for (let i = 1; i <= this.state.active; i++) {
            res = res && this.validateSong(i);
        }

        return res;
    }

    setSong = (song, index) => {
        let elements = this.state.elements;
        elements[index] = song;
        this.setState({elements: elements});
    }

    setSongName = (data, index) => {
        this.song = {
            name: data,
            audio: this.state.elements[index].audio,
            accounts: this.state.elements[index].accounts,
            collaborations: this.state.elements[index].collaborations,
            genres: this.state.elements[index].genres
        }
        this.setSong(this.song, index);
        if (this.state.elements[index].accounts.length === 0)
            this.setSongAccounts([this.props.auth.isLoggedIn], index);
    }

    setSongAudio = (data, index) => {
        this.song = {
            name: this.state.elements[index].name,
            audio: data.target.files[0],
            accounts: this.state.elements[index].accounts,
            collaborations: this.state.elements[index].collaborations,
            genres: this.state.elements[index].genres
        }
        this.setSong(this.song, index);
    }

    setSongAccounts = (data, index) => {
        this.song = {
            name: this.state.elements[index].name,
            audio: this.state.elements[index].audio,
            accounts: data,
            collaborations: this.state.elements[index].collaborations,
            genres: this.state.elements[index].genres
        }
        this.setSong(this.song, index);
    }

    setSongCollaborations = (data, index) => {
        this.song = {
            name: this.state.elements[index].name,
            audio: this.state.elements[index].audio,
            accounts: this.state.elements[index].accounts,
            collaborations: data,
            genres: this.state.elements[index].genres
        }
        this.setSong(this.song, index);
    }

    setSongGenres = (data, index) => {
        this.song = {
            name: this.state.elements[index].name,
            audio: this.state.elements[index].audio,
            accounts: this.state.elements[index].accounts,
            collaborations: this.state.elements[index].collaborations,
            genres: data
        }
        this.setSong(this.song, index);
    }

    addSong = (index) => {
        let arr = this.state.elements;
        arr[index] = this.song;
        this.setState({elements: arr});
    }

    setActive = () => {
        this.setState({active: this.state.active + 1})
    }

    publish = () => {
        if (!window.confirm(`Are you sure you want to publish this ${this.state.active} song(s)?`))
            return;

        this.setState({loader: true, loaderCaption: "publishing.."});
        let albumData = new FormData();
        let address = 'http://localhost:8080/api/v1/album/default-picture';

        if (this.state.elements[0].imageData) {
            albumData = this.state.elements[0].imageData;
            address = 'http://localhost:8080/api/v1/album';
        }

        albumData.append('name', this.state.elements[0].name);


        albumData.append('account_id', this.state.elements[0].accounts.map(a => a.account_ID));

        const requestOptions = {
            method: 'POST',
            body: albumData
        };

        fetch(address, requestOptions)
            .then(response => response.json())
            .then(data => {
                if (data.message){
                    alert(data.message);
                    this.setState({loaderCaption: "Error!"});
                }
                else if (data.id) {
                    this.setState({album_id: data.id, loaderCaption: "publishing..."});
                }
            })
            .then(() => {
                for (let i = 1; i <= this.state.active && this.state.loaderCaption !== "Error!"; i++) {
                    let songData = new FormData();

                    songData.append('audio', this.state.elements[i].audio);
                    songData.append('name', this.state.elements[i].name);
                    songData.append('collaborations', this.state.elements[i].collaborations);
                    songData.append('album_id', this.state.album_id);
                    songData.append('album_order', i);

                    songData.append('account_id', this.state.elements[i].accounts.map(a => a.account_ID));

                    songData.append('genres', this.state.elements[i].genres.map(x => x.genre_ID));

                    const requestOptionsSong = {
                        method: 'POST',
                        body: songData
                    };
                    
                    fetch('http://localhost:8080/api/v1/song', requestOptionsSong)
                        .then(response => response.json())
                        .then(data => {
                            if (data.message)
                                this.setState({loaderCaption: "Error!"});
                        })
                        .catch(() => {
                            this.setState({loaderCaption: this.state.loaderCaption + "."});
                        })
                }
            })
            .then(() => {
                this.setState({loaderCaption: "Success!"});
            });
    }

    render() {
        if (this.state.loader) {
            return (
                <>
                    <Navigation />
                    <div className="App-header">
                        <Loader caption={this.state.loaderCaption} />
                        {this.state.loaderCaption === "Success!" &&
                            <button className="button" onClick={() => this.setState(this.initialState)}>Reset</button>
                        }
                    </div>
                </>
            );
        }
        else
        return (
        <>
            <Navigation />
            <div className="App-header">
                <div>
                    <span>Enter album data and then at least one and at most ten songs</span>
                    <hr />
                </div>

                    {this.state.elements.map((element, index) => {
                        const Buttons = () => {
                            return <form autocomplete="off" className="form-modal">
                                <div className="form-buttons">
                                    {this.validateAlbum() && 
                                        <input type="button" className="button" value="Add song" onClick={this.setActive} />
                                    }
                                    {!this.validateAlbum() &&
                                        <input type="button" className="button" value="Add song" disabled/>
                                    }
                                    {this.state.active > 1 &&
                                        <input type="button" className="button" value="Remove last song" onClick={() => this.setState({active: this.state.active - 1})}/>
                                    }
                                    {this.validateAll() && 
                                        <input type="button" className="button" value="Publish" onClick={this.publish} />
                                    }
                                    {!this.validateAll() &&
                                        <input type="button" className="button" value="Publish" disabled/>
                                    }
                                </div>
                            </form>
                        }

                        if (index === 0) {
                            return (
                            <div className="step">
                                <PublishAlbum 
                                    name={album.name}
                                    author={this.props.auth.isLoggedIn}
                                    setName={this.setAlbumName}
                                    setImageData={this.setImageData}
                                    setAccounts={this.setAlbumAccounts}
                                />
                                {this.state.active === 0 && <Buttons />}
                            </div>
                            );
                        }

                        else if (index <= this.state.active)
                            return (
                                <>
                                <div className="step">
                                    <div>
                                        <span>Song {index}</span>
                                        <hr />
                                    </div>
                                    <PublishSong 
                                        name={this.state.elements[index].name}
                                        author={this.props.auth.isLoggedIn}
                                        setName={this.setSongName}
                                        setAudio={this.setSongAudio}
                                        setAccounts={this.setSongAccounts}
                                        setCollaborations={this.setSongCollaborations}
                                        setGenres={this.setSongGenres}
                                        index={index}
                                    />

                                    {this.state.active === index && <Buttons />}
                                </div>
                                </>
                            );

                        else 
                            return <></>;
                    })}
            </div>
        </>
        );
    }
}

const mapStateToProps = state => {
    return {
        auth: state.auth
    };
  };
  
  const mapDispatchToProps = dispatch => {
    return {
        logoutUser: () => dispatch(logoutUser())
    };
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(PublishPage);