import React from 'react';
import Navigation from '../Components/Navigation';
import Register from '../Components/Register';
import authToken from '../utils/authToken';

function RegisterPage() {
    if(localStorage.jwtToken) {
        authToken(localStorage.jwtToken);
    }

    return (
    <>
        <Navigation />
        <div className="App-header">
            <Register />
        </div>
    </>
    );
}

export default RegisterPage;