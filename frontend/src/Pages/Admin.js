import React, { useState } from 'react';
import Loader from '../Components/Loader';
import NavigationAdmin from '../Components/NavigationAdmin';
import Modal from '../Components/Modal';

import Table from '../Components/Table';

function Admin() {
    const [data, setData] = useState(false);
    const [dataHeader, setDataHeader] = useState(false);
    const [filter, setFilter] = useState({ value: "", sort: "", sortDirection: undefined });
    const [showModal, setShowModal] = useState(false);
    const [update, setUpdate] = useState(false);

    return (
    <>
        {showModal &&
            <Modal modal={showModal} setShowModal={setShowModal} update={update} setUpdate={setUpdate} />
        }
        <NavigationAdmin setData={setData} setHeader={setDataHeader} setFilter={setFilter} />
        <div className="App-header">
            {(!data || !dataHeader) && 
                <Loader caption={"Choose a table for display..."}/>
            }
            {data && dataHeader &&
                <Table data={data} 
                        headerItems={dataHeader} 
                        filter={filter}
                        setFilter={setFilter}
                        setShowModal={setShowModal}
                        setUpdate={setUpdate}
                />
            }
        </div>
    </>
    );
}

export default Admin;