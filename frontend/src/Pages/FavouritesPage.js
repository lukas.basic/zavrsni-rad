import React, { useState, useEffect } from 'react';
import Navigation from '../Components/Navigation';
import Loader from '../Components/Loader';
import PlaylistRow from '../Components/PlaylistRow';
import AlbumRow from '../Components/AlbumRow';
import {connect} from 'react-redux';

function FavouritesPage(props) {    
    const [loaderP, setLoaderP] = useState(true);
    const [favPlaylist, setFavPlaylist] = useState(null);
    useEffect(() => {
        fetch('http://localhost:8080/api/v1/playlist/favourites?account_ID=' + props.auth.isLoggedIn.account_ID)
            .then(response => response.json())
            .then(data => {
                if (!data.message) {
                    setFavPlaylist(data);
                    setLoaderP(false);
                }
            });

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const [loaderA, setLoaderA] = useState(true);
    const [albums, setAlbums] = useState([]);
    const [rows, setRows] = useState([]);
    useEffect(() => {
        fetch('http://localhost:8080/api/v1/account/liked-albums?username=' + props.auth.isLoggedIn.username)
            .then(response => response.json())
            .then(data => {
                setAlbums(data);
                setLoaderA(false);     
                let tmp = [];
                for (let i = 0; i < data.length/4; i++) {
                    tmp.push(i);
                }
                setRows(tmp);
            });

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    if (loaderP || loaderA)
        return (
            <>
                <Navigation />
                <div className="App-header">
                    <Loader />
                </div>
            </>
        );
    else
        return (
            <>
                <Navigation />
                <div className="App-header">
                    <div className="section-desc">
                        <div className="l b"></div>
                        <span>Songs</span>
                        <div className="l a"></div>
                    </div>
                    <div className="album-card-container">
                        <PlaylistRow playlist={favPlaylist} />
                    </div>

                    <div className="section-desc">
                        <div className="l b"></div>
                        <span>Albums</span>
                        <div className="l a"></div>
                    </div>             
                    <div className="album-card-container">
                        {albums.length === 0 &&
                            <span>You like 0 albums for now...</span>
                        }
                        {rows.map(i => {
                            return <AlbumRow ind={i} albums={albums}/>
                        })}
                    </div>
                </div>
            </>
        );
}

const mapStateToProps = state => {
    return {
        auth: state.auth
    };
};

export default connect(mapStateToProps)(FavouritesPage);