import React from 'react';
import { Link } from "react-router-dom";
import Navigation from '../Components/Navigation';
import Login from '../Components/Login';
import authToken from '../utils/authToken';

function LoginPage() {
    if(localStorage.jwtToken) {
        authToken(localStorage.jwtToken);
    }

    return (
    <>
        <Navigation />
        <div className="App-header">
            <Login />
            <div style={{"font-size": "small", "margin-top": "10px"}}>
                <Link to="/register" style={{ textDecoration: 'underline', color:  'white'}}>
                        <span>Register</span>
                </Link>
                <span>, if you don't have account already!</span>
            </div>
        </div>
    </>
    );
}

export default LoginPage;