import React, { useState, useEffect } from 'react';
import Loader from '../Components/Loader';
import Navigation from '../Components/Navigation';
import AlbumRow from '../Components/AlbumRow';
import ArtistRow from '../Components/ArtistRow';
import authToken from '../utils/authToken';
import {connect} from 'react-redux';

import '../Styles/AlbumCard.css';

function Home(props) {
    if(localStorage.jwtToken) {
        authToken(localStorage.jwtToken);
    }

    const [loader1, setLoader1] = useState(true);
    const [newest, setNewest] = useState([]);
    useEffect(() => {
        fetch('http://localhost:8080/api/v1/album/newest')
            .then(response => response.json())
            .then(data => {
                if (!data.message)
                    setNewest(data);
                setLoader1(false);
            });
    }, []);

    const [loader2, setLoader2] = useState(true);
    const [mostPop, setMostPop] = useState([]);
    useEffect(() => {
        fetch('http://localhost:8080/api/v1/album/most-popular')
            .then(response => response.json())
            .then(data => {
                if (!data.message)
                    setMostPop(data);
                setLoader2(false);
            });
    }, []);

    const [loader3, setLoader3] = useState(true);
    const [mostPopAr, setMostPopAr] = useState([]);
    useEffect(() => {
        fetch('http://localhost:8080/api/v1/account/most-popular')
            .then(response => response.json())
            .then(data => {
                if (!data.message)
                    setMostPopAr(data);
                setLoader3(false);
            });
    }, []);

    const [loader4, setLoader4] = useState(props.auth.isLoggedIn.isLoggedIn);
    const [followingNewest, setFollowingNewest] = useState([]);
    useEffect(() => {
        if (props.auth.isLoggedIn.isLoggedIn)
            fetch('http://localhost:8080/api/v1/album/newest-following?id=' + props.auth.isLoggedIn.account_ID)
                .then(response => response.json())
                .then(data => {
                    if (!data.message)
                        setFollowingNewest(data);
                    setLoader4(false);
                });

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const [loader5, setLoader5] = useState(props.auth.isLoggedIn.isLoggedIn);
    const [liked, setLiked] = useState([]);
    useEffect(() => {
        if (props.auth.isLoggedIn.isLoggedIn)
            fetch(`http://localhost:8080/api/v1/account/liked-albums?username=${props.auth.isLoggedIn.username}&limit=4`)
                .then(response => response.json())
                .then(data => {
                    if (!data.message)
                        setLiked(data);
                    setLoader5(false);
                });

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    if (loader1 || loader2 || loader3 || loader4 || loader5)
        return (
        <>
            <Navigation />        
            <div className="App-header">
                <Loader />
            </div>
        </>
        );
    else
        return (
        <>
            <Navigation />        
            <div className="App-header">
                <div className="album-card-container">

                    {newest.length > 0 &&
                        <>
                        <div className="section-desc">
                            <div className="l b"></div>
                            <span>New</span>
                            <div className="l a"></div>
                        </div>
                                    
                        <div className="album-card-container">
                            <AlbumRow ind={0} albums={newest}/>
                        </div>
                        </>
                    }

                    {mostPop.length > 0 &&
                        <>
                        <div className="section-desc">
                            <div className="l b"></div>
                            <span>Most popular</span>
                            <div className="l a"></div>
                        </div>
                                  
                        <div className="album-card-container">
                            <AlbumRow ind={0} albums={mostPop}/>
                        </div>
                        </>
                    }
                    {mostPopAr.length > 0 &&
                        <>
                        <div className="section-desc">
                            <div className="l b"></div>
                            <span>Most popular artists</span>
                            <div className="l a"></div>
                        </div>
                            
                        <div className="album-card-container">
                            <ArtistRow ind={0} artists={mostPopAr}/>
                        </div>
                        </>
                    }

                    {followingNewest.length > 0 &&
                        <>
                        <div className="section-desc">
                            <div className="l b"></div>
                            <span>New by artists you follow</span>
                            <div className="l a"></div>
                        </div>
                                 
                        <div className="album-card-container">
                            <AlbumRow ind={0} albums={followingNewest}/>
                        </div>
                        </>
                    }

                    {liked.length > 0 &&
                        <>
                        <div className="section-desc">
                            <div className="l b"></div>
                            <span>Liked</span>
                            <div className="l a"></div>
                        </div>
                                 
                        <div className="album-card-container">
                            <AlbumRow ind={0} albums={liked}/>
                        </div>
                        </>
                    }
                </div>
            </div>
        </>
        );
}

const mapStateToProps = state => {
    return {
        auth: state.auth
    };
};

export default connect(mapStateToProps)(Home);