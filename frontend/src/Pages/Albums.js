import React from "react";
import {
  Switch,
  Route,
  Redirect,
  useRouteMatch
} from "react-router-dom";
import Navigation from '../Components/Navigation';

import Album from '../Components/Album';

function Albums(props) {
    let match = useRouteMatch();
  
    return (
        <>
            <Navigation />        
            <div className="App-header">
                <Switch>
                    <Route path={`${match.path}/:albumId`}>
                        <Album setTrackList={props.setTrackList} />
                    </Route>
                    <Route path={match.path}>
                        <Redirect to='/' />
                    </Route>
                </Switch>
            </div>
        </>
    );
}

export default Albums;