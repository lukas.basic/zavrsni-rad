import React from "react";
import {
  Switch,
  Route,
  Redirect,
  useRouteMatch
} from "react-router-dom";
import Navigation from '../Components/Navigation';

import {logoutUser} from '../Services/index';
import {connect} from 'react-redux';

import Playlist from '../Components/Playlist';
import MyPlaylists from '../Components/MyPlaylists';

function Playlists(props) {
    let match = useRouteMatch();
  
    return (
        <>
            <Navigation />        
            <div className="App-header">
                <Switch>
                    <Route path={`${match.path}/:playlistId`}>
                        <Playlist setTrackList={props.setTrackList} />
                    </Route>
                    <Route path={match.path}>
                        {!props.auth.isLoggedIn.isLoggedIn &&
                            <Redirect to='/login' />
                        }
                        {props.auth.isLoggedIn.isLoggedIn &&
                            <MyPlaylists />
                        }
                    </Route>
                </Switch>
            </div>
        </>
    );
}

const mapStateToProps = state => {
    return {
        auth: state.auth
    };
  };
  
  const mapDispatchToProps = dispatch => {
    return {
        logoutUser: () => dispatch(logoutUser())
    };
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(Playlists);