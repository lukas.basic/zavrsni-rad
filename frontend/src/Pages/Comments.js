import React from "react";
import {
  Switch,
  Route,
  Redirect,
  useRouteMatch
} from "react-router-dom";
import Navigation from '../Components/Navigation';

import SongComments from '../Components/SongComments';

function Comments() {
    let match = useRouteMatch();
  
    return (
        <>
            <Navigation />        
            <div className="App-header">
                <Switch>
                    <Route exact path={`${match.path}/:songId`}>
                        <SongComments />
                    </Route>
                    <Route path={match.path}>
                        <Redirect to='/' />
                    </Route>
                </Switch>
            </div>
        </>
    );
}

export default Comments;