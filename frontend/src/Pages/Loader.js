import React from 'react';
import logo from '../logo.svg';

function Loader() {
    return (
        <div className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
        </div>
    );
}

export default Loader;