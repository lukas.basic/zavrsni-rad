
import * as AT from './authTypes';
import axios from 'axios';

export const authenticateUser = (email, password) => {
    const credentials = new FormData();
    credentials.append('email', email);
    credentials.append('password', password);
    return dispatch => {
        dispatch({
            type: AT.LOGIN_REQUEST
        });
        logoutUser();
        axios.post("http://localhost:8080/api/v1/account/authenticate", credentials)
            .then(response => {
                let token = response.data.token;
                localStorage.setItem('jwtToken', token);
                dispatch(success(true, response.data.account_ID, response.data.name, response.data.authorities));
            })
            .catch(error => {
                dispatch(failure());
            });
    };
};

export const logoutUser = () => {
    return dispatch => {
        dispatch({
            type: AT.LOGOUT_REQUEST
        });
        localStorage.removeItem('jwtToken');
        dispatch(success(false));
    };
};

const success = (isLoggedIn, account_ID, username, authorities) => {
    return {
        type: AT.SUCCESS,
        payload: {
            isLoggedIn: isLoggedIn,
            account_ID: account_ID,
            username: username,
            authorities: authorities
        }
    };
};

const failure = () => {
    return {
        type: AT.FAILURE,
        payload: false
    };
};